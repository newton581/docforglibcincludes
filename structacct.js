var structacct =
[
    [ "ac_flag", "structacct.html#ae7f92a7b7b405e34355df7d7b3ea277f", null ],
    [ "ac_uid", "structacct.html#af99ad07e98083165e5a4d2944d81ef23", null ],
    [ "ac_gid", "structacct.html#a97f14344beca3ef7d7987698828dead6", null ],
    [ "ac_tty", "structacct.html#a4bb93f014a41388562e6e816130e03b6", null ],
    [ "ac_btime", "structacct.html#a82b3e9e684fa71f27e7f1854ffc5152c", null ],
    [ "ac_utime", "structacct.html#a7a8f34a80a37d30aaa603bbee9f326ec", null ],
    [ "ac_stime", "structacct.html#aa4fd57c4b8f5f347856334b82c878446", null ],
    [ "ac_etime", "structacct.html#a06f3f037e37b17cb6af3b85ffa249bfe", null ],
    [ "ac_mem", "structacct.html#a4e41da9d5a49b8080a7df3d7d87f31c9", null ],
    [ "ac_io", "structacct.html#adca0496ee3c7a3d949d34f65c2c96758", null ],
    [ "ac_rw", "structacct.html#aca4094341aadb1954749f299a59c310b", null ],
    [ "ac_minflt", "structacct.html#ac116d8d9ab4fe2f52d5d3efc454294df", null ],
    [ "ac_majflt", "structacct.html#a0366aa4738572a1571f2d0f2897272b2", null ],
    [ "ac_swaps", "structacct.html#a6761fd5305b4154220efb3d6dcca97a9", null ],
    [ "ac_exitcode", "structacct.html#aed8e80c4c40af2464e471d166c760d3e", null ],
    [ "ac_comm", "structacct.html#a61ce0c3a7649b68a175edfe37a8e99c3", null ],
    [ "ac_pad", "structacct.html#a949556c48264f8d331b78a45ed8d774b", null ]
];