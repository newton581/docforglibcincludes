var sys_2ipc_8h =
[
    [ "__uid_t_defined", "sys_2ipc_8h.html#a02b3cc2cd87d7b2248e23d109869d203", null ],
    [ "__gid_t_defined", "sys_2ipc_8h.html#a8a2a8903c49d6d48183d89ae2c9bc44b", null ],
    [ "__mode_t_defined", "sys_2ipc_8h.html#a9984a49b304b37ae37c2f5e24fb1678f", null ],
    [ "__key_t_defined", "sys_2ipc_8h.html#a92004323d2936740eab4232b6db8b43e", null ],
    [ "uid_t", "sys_2ipc_8h.html#a1844226d778badcda0a21b28310830ea", null ],
    [ "gid_t", "sys_2ipc_8h.html#a9520fe38856d436aa8c5850ff21839ec", null ],
    [ "mode_t", "sys_2ipc_8h.html#ae9f148ba55d84268ecb6f8031ab45076", null ],
    [ "key_t", "sys_2ipc_8h.html#ac2fa30bf00f667b1efa66370fa7b93f8", null ],
    [ "ftok", "sys_2ipc_8h.html#abde6f4548f741e8f85a9c18a50ec85d0", null ]
];