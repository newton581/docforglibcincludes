var structrose__facilities__struct =
[
    [ "source_addr", "structrose__facilities__struct.html#a98d2a329d898f0bbe1e2a09d1bfa0b72", null ],
    [ "dest_addr", "structrose__facilities__struct.html#a77a5594d371cf2e00bd72ad6eb32095f", null ],
    [ "source_call", "structrose__facilities__struct.html#a6fdd72044435863a6dfa8bc9397f19b9", null ],
    [ "dest_call", "structrose__facilities__struct.html#a6140378fec05a089257e58f5295ad45e", null ],
    [ "source_ndigis", "structrose__facilities__struct.html#a0f47b657985a208cbd0503420ab89063", null ],
    [ "dest_ndigis", "structrose__facilities__struct.html#a0420a35e67b4ae0f255465e5b3a20f82", null ],
    [ "source_digis", "structrose__facilities__struct.html#aea1a80ed5d7583a72c5b47ddd3f12442", null ],
    [ "dest_digis", "structrose__facilities__struct.html#a5772320a54a80640e041a94ed6cb6d00", null ],
    [ "rand", "structrose__facilities__struct.html#a928619d39612c850370d203cc982af86", null ],
    [ "fail_addr", "structrose__facilities__struct.html#a39844101b3949c78ad0f2e5656eb3579", null ],
    [ "fail_call", "structrose__facilities__struct.html#a2f420f00ee431978317b13fbd58b18ab", null ]
];