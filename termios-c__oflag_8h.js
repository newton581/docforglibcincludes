var termios_c__oflag_8h =
[
    [ "OPOST", "termios-c__oflag_8h.html#a0aad4afe9e202fff8ced3485bc835bb1", null ],
    [ "OLCUC", "termios-c__oflag_8h.html#a2ed0011eaf721d107946bf06f1824b36", null ],
    [ "ONLCR", "termios-c__oflag_8h.html#a957436bc6db0a0d49b081dc9b4c98ef2", null ],
    [ "OCRNL", "termios-c__oflag_8h.html#adc345d36b46c6ccdd079c9ecc3fc1296", null ],
    [ "ONOCR", "termios-c__oflag_8h.html#aab452deaf5d1b993c3b0ff2d3e408577", null ],
    [ "ONLRET", "termios-c__oflag_8h.html#a8fd17e0aa25ab2abd07477442ba1928e", null ],
    [ "OFILL", "termios-c__oflag_8h.html#ae6ebfbba412b33b84de07c35f12b2d7f", null ],
    [ "OFDEL", "termios-c__oflag_8h.html#a671d46b6d4ea404575eb6316dcd48fd6", null ],
    [ "VTDLY", "termios-c__oflag_8h.html#a0dca1471f7fd540d5e65c54121daeae4", null ],
    [ "VT0", "termios-c__oflag_8h.html#aa0b5f0009e2f90d8585e81b480299757", null ],
    [ "VT1", "termios-c__oflag_8h.html#adb98231c20f9f38c99220501e107bd04", null ]
];