var sys_2mman_8h =
[
    [ "__need_size_t", "sys_2mman_8h.html#a2898830827f947f003d187388f7e6c72", null ],
    [ "__off_t_defined", "sys_2mman_8h.html#af81a0b2e26ce4bbc9f41968578337f00", null ],
    [ "__mode_t_defined", "sys_2mman_8h.html#a9984a49b304b37ae37c2f5e24fb1678f", null ],
    [ "MAP_FAILED", "sys_2mman_8h.html#a8523dcf952f6ff059a3bed717e4f1296", null ],
    [ "off_t", "sys_2mman_8h.html#a042c92b5eb383998e689acdea01d12de", null ],
    [ "mode_t", "sys_2mman_8h.html#ae9f148ba55d84268ecb6f8031ab45076", null ],
    [ "mmap", "sys_2mman_8h.html#a2712d73509d6b2d1d695efadd60caa3e", null ],
    [ "munmap", "sys_2mman_8h.html#aa3d9805ee4a226316b39f91b8fe1c7d5", null ],
    [ "mprotect", "sys_2mman_8h.html#ab1d8112c5eb0f203f4f8a586575d7612", null ],
    [ "msync", "sys_2mman_8h.html#a5152e6e9275d85410f14eb3a40dc43c8", null ],
    [ "mlock", "sys_2mman_8h.html#a21596cc51da9893ff9cbada8cd48036b", null ],
    [ "munlock", "sys_2mman_8h.html#a7203ee6f0d928a22e9f770652a283fa5", null ],
    [ "mlockall", "sys_2mman_8h.html#ab175cf2e455993acf449b741b34fcd87", null ],
    [ "munlockall", "sys_2mman_8h.html#a655f31b85ea63bbbef0d8ebf26a1802e", null ],
    [ "shm_open", "sys_2mman_8h.html#ab023868ce196b0f94ee0db0e49a5a31a", null ],
    [ "shm_unlink", "sys_2mman_8h.html#abcc0c9cebf418416fa73c5ddd08481c8", null ]
];