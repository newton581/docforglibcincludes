var structip6__rthdr0 =
[
    [ "ip6r0_nxt", "structip6__rthdr0.html#af1caf3a4338125e95361aaeb8e02b8d5", null ],
    [ "ip6r0_len", "structip6__rthdr0.html#a12da0d0272596ff932c88554d54d504c", null ],
    [ "ip6r0_type", "structip6__rthdr0.html#a23af21c1148e91733ea8bcf876432e54", null ],
    [ "ip6r0_segleft", "structip6__rthdr0.html#a966258726a95193b95cd1ee8bc0b120d", null ],
    [ "ip6r0_reserved", "structip6__rthdr0.html#a88f329fa46cecef64e26cba44f8ef5d2", null ],
    [ "ip6r0_slmap", "structip6__rthdr0.html#a35a1839c6c56cdd5474e2733203c02b2", null ],
    [ "ip6r0_addr", "structip6__rthdr0.html#ab22583b2cd1888f06bcf80de57be4b1e", null ]
];