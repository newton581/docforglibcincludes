var swap_8h =
[
    [ "SWAP_FLAG_PREFER", "swap_8h.html#a4a2b9e66e41a110c7190fcdc56828f99", null ],
    [ "SWAP_FLAG_PRIO_MASK", "swap_8h.html#ae6662638269fe976fbc834fdd00ffdda", null ],
    [ "SWAP_FLAG_PRIO_SHIFT", "swap_8h.html#ac292af253016c9a9ff6b3afb7dd56ba3", null ],
    [ "SWAP_FLAG_DISCARD", "swap_8h.html#a61b4c7b51f7b051736cb5dd4d767411e", null ],
    [ "swapon", "swap_8h.html#acf85cb9a228caa49dc0c13771a82094f", null ],
    [ "swapoff", "swap_8h.html#ab5a083ec900b57466d93d60c51256fd2", null ]
];