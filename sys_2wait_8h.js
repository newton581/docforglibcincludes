var sys_2wait_8h =
[
    [ "__pid_t_defined", "sys_2wait_8h.html#a78397b687c0ddca84c27c22f8827a7a1", null ],
    [ "WEXITSTATUS", "sys_2wait_8h.html#a77f17ed4771a558a0f16e5f3aecee222", null ],
    [ "WTERMSIG", "sys_2wait_8h.html#a94ec02a12424092a8391069adcd2ff73", null ],
    [ "WSTOPSIG", "sys_2wait_8h.html#afe978e441cdea20693af4fe517b53594", null ],
    [ "WIFEXITED", "sys_2wait_8h.html#aa5fda3caf01a15f80a9f2542ccc67b94", null ],
    [ "WIFSIGNALED", "sys_2wait_8h.html#add863a14eeaad9d01db1bae7e46aff1a", null ],
    [ "WIFSTOPPED", "sys_2wait_8h.html#aa2687811d048892a4fa1f14f74843767", null ],
    [ "wait", "sys_2wait_8h.html#a45c00ebdc7c9a8dbfa115b38afdd754b", null ],
    [ "waitpid", "sys_2wait_8h.html#aa35dbece97881ee00888a5f99e3c8e65", null ],
    [ "pid_t", "sys_2wait_8h.html#a353168e18818c4072d5e8a1782125d12", null ]
];