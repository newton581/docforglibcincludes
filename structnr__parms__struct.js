var structnr__parms__struct =
[
    [ "quality", "structnr__parms__struct.html#a6ffc43144faa4433f99670f6ef9c725e", null ],
    [ "obs_count", "structnr__parms__struct.html#a18d78ac227a0d19a93fb8eab80abfed8", null ],
    [ "ttl", "structnr__parms__struct.html#a5eac99b952ebebc2c4e427a6cd72e724", null ],
    [ "timeout", "structnr__parms__struct.html#a91caf3239294cd621b00e1c33e55df70", null ],
    [ "ack_delay", "structnr__parms__struct.html#a701f41e484219dd04e52c9052c100e47", null ],
    [ "busy_delay", "structnr__parms__struct.html#a6d2fafa7dd65e552ac72f9ece583776a", null ],
    [ "tries", "structnr__parms__struct.html#a843bb97772c8368401bc5d365bb7deb2", null ],
    [ "window", "structnr__parms__struct.html#a0a33a06f4796dd1b541d93989b290632", null ],
    [ "paclen", "structnr__parms__struct.html#a0469eb9d992b95a4bb42ffb6ccbba0f5", null ]
];