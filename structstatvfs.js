var structstatvfs =
[
    [ "f_bsize", "structstatvfs.html#a1f2ab1c4bd96e136fd3d7a260443c4fd", null ],
    [ "f_frsize", "structstatvfs.html#a8a878cbc55302209f2d1c0bee108312c", null ],
    [ "f_blocks", "structstatvfs.html#af9d2a09dadbc47efa46f39f2a4d24bc4", null ],
    [ "f_bfree", "structstatvfs.html#aed7f3cc46f85f647963689d68feda81c", null ],
    [ "f_bavail", "structstatvfs.html#ad8843edf721adff0ab7ab9b9d7e97b1e", null ],
    [ "f_files", "structstatvfs.html#a29256866c5c34b542965165d359eb59a", null ],
    [ "f_ffree", "structstatvfs.html#a5eb2afcbe450c17976fd653a7eadf517", null ],
    [ "f_favail", "structstatvfs.html#a88941e75d670e1f6e585ad897f095f21", null ],
    [ "f_fsid", "structstatvfs.html#a0f9a2ac8746b31342ab6e4ec66f89a11", null ],
    [ "f_flag", "structstatvfs.html#ac42d3c47d25bfa0f8a1e0841a95a813f", null ],
    [ "f_namemax", "structstatvfs.html#a1d3c4fc9a86e14ea81907432c3bf4a7c", null ],
    [ "__f_spare", "structstatvfs.html#a53d52e99e01d4127b0c0d83cb0b4b658", null ]
];