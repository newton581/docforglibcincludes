var sys_2param_8h =
[
    [ "__need_NULL", "sys_2param_8h.html#a6c27666e43b87c08fa8b2e8aa324601c", null ],
    [ "NBBY", "sys_2param_8h.html#ac3e045b1eb12fc6a000555fda916a023", null ],
    [ "NCARGS", "sys_2param_8h.html#a80885a8098e1f68ee6f46614a6d66629", null ],
    [ "NOGROUP", "sys_2param_8h.html#ae808967e53fe8df76d0671033db0be8b", null ],
    [ "NODEV", "sys_2param_8h.html#a61f401f53d3195121a14210375b2f86b", null ],
    [ "DEV_BSIZE", "sys_2param_8h.html#a9c8466b6111a11195a5cd2478c648e32", null ],
    [ "setbit", "sys_2param_8h.html#ab07620b0f4646de0afd8cdac7e4ca074", null ],
    [ "clrbit", "sys_2param_8h.html#afdcc6f67bfbd6aeeb042d5e7ab2be41d", null ],
    [ "isset", "sys_2param_8h.html#a8a86f0e917c9c62dd630a5e35f16eaf9", null ],
    [ "isclr", "sys_2param_8h.html#aab0094c7138e084845aa274dd8b9a0e2", null ],
    [ "howmany", "sys_2param_8h.html#a5986a9a34b5ef2d56f42336b32ef85c7", null ],
    [ "roundup", "sys_2param_8h.html#aeb47018f732c79a339c5617ef0cbab65", null ],
    [ "powerof2", "sys_2param_8h.html#a2766c9b72579fe88c4f337b4e007c8a7", null ],
    [ "MIN", "sys_2param_8h.html#a3acffbd305ee72dcd4593c0d8af64a4f", null ],
    [ "MAX", "sys_2param_8h.html#afa99ec4acc4ecb2dc3c2d05da15d0e3f", null ]
];