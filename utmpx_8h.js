var utmpx_8h =
[
    [ "__pid_t_defined", "utmpx_8h.html#a78397b687c0ddca84c27c22f8827a7a1", null ],
    [ "pid_t", "utmpx_8h.html#a63bef27b1fbd4a91faa273a387a38dc2", null ],
    [ "setutxent", "utmpx_8h.html#a2494166e72f5ac6009487ea12a6a1e47", null ],
    [ "endutxent", "utmpx_8h.html#aec344f7911d60551d08b015e7e7c4725", null ],
    [ "getutxent", "utmpx_8h.html#acf98d27ba342a724edaf36082057c897", null ],
    [ "getutxid", "utmpx_8h.html#ade001aac8e836f1587243a746d4319af", null ],
    [ "getutxline", "utmpx_8h.html#a1165b81ce785bab4525303bf959046ec", null ],
    [ "pututxline", "utmpx_8h.html#a7082661075102fd53c6f8f815f2a94c1", null ]
];