var sys_2procfs_8h =
[
    [ "elf_siginfo", "structelf__siginfo.html", "structelf__siginfo" ],
    [ "elf_prstatus", "structelf__prstatus.html", "structelf__prstatus" ],
    [ "elf_prpsinfo", "structelf__prpsinfo.html", "structelf__prpsinfo" ],
    [ "ELF_PRARGSZ", "sys_2procfs_8h.html#ad877f798effdb5a69362cbaddefa6e3d", null ],
    [ "psaddr_t", "sys_2procfs_8h.html#ad65a744093c157125797432097349eed", null ],
    [ "prgregset_t", "sys_2procfs_8h.html#acd8eec712d7b4904ec18083918168a6d", null ],
    [ "prfpregset_t", "sys_2procfs_8h.html#ac0db711a973a84ee0f0fb9c6a313dab1", null ],
    [ "lwpid_t", "sys_2procfs_8h.html#ab5bb49db6f9bbca2c10617e78b27c93f", null ],
    [ "prstatus_t", "sys_2procfs_8h.html#a7d672b5608e28045270a75c072dcd87e", null ],
    [ "prpsinfo_t", "sys_2procfs_8h.html#a703b1e93131de5604fe50005ac893b8b", null ]
];