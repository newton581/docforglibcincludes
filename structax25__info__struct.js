var structax25__info__struct =
[
    [ "n2", "structax25__info__struct.html#a104a61889d09cdb61a99327f00f3e060", null ],
    [ "n2count", "structax25__info__struct.html#acf7c6a6d6403ac55d4b1ec5fe00c3f30", null ],
    [ "t1", "structax25__info__struct.html#aa3b3a971511fe92c62d53f9661f6b577", null ],
    [ "t1timer", "structax25__info__struct.html#a165e1b31f7bd08c146038f82dc9ecf39", null ],
    [ "t2", "structax25__info__struct.html#ab96fb6273e731eb701e4664fb4064590", null ],
    [ "t2timer", "structax25__info__struct.html#a7596152390f9fa8a75a492cbc1d938c0", null ],
    [ "t3", "structax25__info__struct.html#afb4a68791c8495dbc1f015a3d1bbe5f4", null ],
    [ "t3timer", "structax25__info__struct.html#a574b3bc0db6219abd59a112b394ed93a", null ],
    [ "idle", "structax25__info__struct.html#a64ce4477274912abeee868e14de9bca5", null ],
    [ "idletimer", "structax25__info__struct.html#acac37c61380629e83072f7a5912ead0e", null ],
    [ "state", "structax25__info__struct.html#a6017db66d258caa900275dbcdb650d34", null ],
    [ "rcv_q", "structax25__info__struct.html#a41d9a4447657640dc25b585f55b3f434", null ],
    [ "snd_q", "structax25__info__struct.html#af023b66bb8a9db18afbecaf3b13e89c5", null ]
];