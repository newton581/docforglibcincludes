var searchData=
[
  ['notify_5fautobpt_17751',['NOTIFY_AUTOBPT',['../thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4fa1e811d28b111355bf4125b05af7f0292',1,'thread_db.h']]],
  ['notify_5fbpt_17752',['NOTIFY_BPT',['../thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4fa1c3948b71ca8133c24b64c719fecb05f',1,'thread_db.h']]],
  ['notify_5fsyscall_17753',['NOTIFY_SYSCALL',['../thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4fa4ffa647540fafbc2590f12fd4e2eafe8',1,'thread_db.h']]],
  ['ns_5fc_5f2_17754',['ns_c_2',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38a1ecf754ca2a80b022ebf1ef0d94cbbce',1,'nameser.h']]],
  ['ns_5fc_5fany_17755',['ns_c_any',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38ad433be52ad037291b89eee08fbe9ff18',1,'nameser.h']]],
  ['ns_5fc_5fchaos_17756',['ns_c_chaos',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38a279cba1dbbdb453f42562d913ba270af',1,'nameser.h']]],
  ['ns_5fc_5fhs_17757',['ns_c_hs',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38aa4c91f289e1da54becd31da7a8c9b7c6',1,'nameser.h']]],
  ['ns_5fc_5fin_17758',['ns_c_in',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38a599d84d91361911643a2a74c1f807514',1,'nameser.h']]],
  ['ns_5fc_5finvalid_17759',['ns_c_invalid',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38a7734d24c418d73f45349966abf2addeb',1,'nameser.h']]],
  ['ns_5fc_5fmax_17760',['ns_c_max',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38a5a1f27e93a20655105dc99a29033c6cb',1,'nameser.h']]],
  ['ns_5fc_5fnone_17761',['ns_c_none',['../nameser_8h.html#a9ca67f2f394f432959835a5660634e38abe0845de7251dffa047777bd742dc3b4',1,'nameser.h']]],
  ['ns_5ff_5faa_17762',['ns_f_aa',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaafd770dd93594c9fc3f9c2005787a2ea5',1,'nameser.h']]],
  ['ns_5ff_5fad_17763',['ns_f_ad',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaa773d5f1bc12feaba52d10b3f4a0a9a91',1,'nameser.h']]],
  ['ns_5ff_5fcd_17764',['ns_f_cd',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaa9ae6db13bb05c8011095467fe3a80482',1,'nameser.h']]],
  ['ns_5ff_5fmax_17765',['ns_f_max',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaa06c92a06e13fdf6bc8d1abf2bef63cdd',1,'nameser.h']]],
  ['ns_5ff_5fopcode_17766',['ns_f_opcode',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaaf13308e3fa57c3930d6eeca6fbd6a2ec',1,'nameser.h']]],
  ['ns_5ff_5fqr_17767',['ns_f_qr',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaa39a6497553303af38eec5af8d386ed22',1,'nameser.h']]],
  ['ns_5ff_5fra_17768',['ns_f_ra',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaac077fd84be8f9f78d4eb63ad5bb895aa',1,'nameser.h']]],
  ['ns_5ff_5frcode_17769',['ns_f_rcode',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaaa56f1612fcab765a69846beaed5cb058',1,'nameser.h']]],
  ['ns_5ff_5frd_17770',['ns_f_rd',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaa5ab4e71a272e98df7950483ccc158972',1,'nameser.h']]],
  ['ns_5ff_5ftc_17771',['ns_f_tc',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaa1cbcabb81c35f708d1a01bfd8c4d3101',1,'nameser.h']]],
  ['ns_5ff_5fz_17772',['ns_f_z',['../nameser_8h.html#a134e2bc9add4def8543a34aea41e7cfaaec6c4f9fd445bccf026dcc7b692cd0e7',1,'nameser.h']]],
  ['ns_5fo_5fiquery_17773',['ns_o_iquery',['../nameser_8h.html#ab23579e6b1f822547a011d13046af231a316088324a80501f51f02b04354adf52',1,'nameser.h']]],
  ['ns_5fo_5fmax_17774',['ns_o_max',['../nameser_8h.html#ab23579e6b1f822547a011d13046af231abb83746a3b88245a750627fd29e3f54e',1,'nameser.h']]],
  ['ns_5fo_5fnotify_17775',['ns_o_notify',['../nameser_8h.html#ab23579e6b1f822547a011d13046af231a13b09a3e48e14fe1300532ae0b4f913c',1,'nameser.h']]],
  ['ns_5fo_5fquery_17776',['ns_o_query',['../nameser_8h.html#ab23579e6b1f822547a011d13046af231a7a597a358e5c668dd9ca041e84effae7',1,'nameser.h']]],
  ['ns_5fo_5fstatus_17777',['ns_o_status',['../nameser_8h.html#ab23579e6b1f822547a011d13046af231a98efe73746d0e6bd454f7ff24b2a5c0f',1,'nameser.h']]],
  ['ns_5fo_5fupdate_17778',['ns_o_update',['../nameser_8h.html#ab23579e6b1f822547a011d13046af231af0432d05705e5103c65b7d67f8e4941c',1,'nameser.h']]],
  ['ns_5fr_5fbadkey_17779',['ns_r_badkey',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a901811aebb250183bd39559752032e08',1,'nameser.h']]],
  ['ns_5fr_5fbadsig_17780',['ns_r_badsig',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18adc65c0945a69a346d444ac9ef3c50cfd',1,'nameser.h']]],
  ['ns_5fr_5fbadtime_17781',['ns_r_badtime',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a09593dcf4a497d1601c185e5860dcdcc',1,'nameser.h']]],
  ['ns_5fr_5fbadvers_17782',['ns_r_badvers',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18ab486e5e56967730c2e634efb849e9a4a',1,'nameser.h']]],
  ['ns_5fr_5fformerr_17783',['ns_r_formerr',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a2be469aa639095a77173c2338b715c49',1,'nameser.h']]],
  ['ns_5fr_5fmax_17784',['ns_r_max',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18aa24df6d9acd383b48b24b0da358beefa',1,'nameser.h']]],
  ['ns_5fr_5fnoerror_17785',['ns_r_noerror',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a1eb282d0f0647bf9435ed7bee942d42e',1,'nameser.h']]],
  ['ns_5fr_5fnotauth_17786',['ns_r_notauth',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a93408c2f448dc64d237d4145b76451b0',1,'nameser.h']]],
  ['ns_5fr_5fnotimpl_17787',['ns_r_notimpl',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18aaf594bec1c41229bf885a8c73a49832a',1,'nameser.h']]],
  ['ns_5fr_5fnotzone_17788',['ns_r_notzone',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a9051227a4f22f60c7565d0d485a0efb5',1,'nameser.h']]],
  ['ns_5fr_5fnxdomain_17789',['ns_r_nxdomain',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a8ac2ec9f93ef74055c07b5db1d6dca38',1,'nameser.h']]],
  ['ns_5fr_5fnxrrset_17790',['ns_r_nxrrset',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18ae5a496bd9a1335389ff4325fb0c9151e',1,'nameser.h']]],
  ['ns_5fr_5frefused_17791',['ns_r_refused',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a70a4bf96a4293994ab8a37a32183e1e3',1,'nameser.h']]],
  ['ns_5fr_5fservfail_17792',['ns_r_servfail',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a86153aac395e547876fe875a7847075b',1,'nameser.h']]],
  ['ns_5fr_5fyxdomain_17793',['ns_r_yxdomain',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18ab749c7c6b9c0f88892abbb6b6bc2f718',1,'nameser.h']]],
  ['ns_5fr_5fyxrrset_17794',['ns_r_yxrrset',['../nameser_8h.html#abeefefd7a232fc91d4a694a5dae5fa18a075926b75238f7b2d7a64a51c8db9424',1,'nameser.h']]],
  ['ns_5fs_5fan_17795',['ns_s_an',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584ad82f75a6d6fa065e4a336b1873516141',1,'nameser.h']]],
  ['ns_5fs_5far_17796',['ns_s_ar',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584aa94c2cca706b27600cf7d7bc27be497d',1,'nameser.h']]],
  ['ns_5fs_5fmax_17797',['ns_s_max',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584a3ad8f17a80cd860899246079fb61855c',1,'nameser.h']]],
  ['ns_5fs_5fns_17798',['ns_s_ns',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584ab36a66f31e7d3c5616b2921ad160fc90',1,'nameser.h']]],
  ['ns_5fs_5fpr_17799',['ns_s_pr',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584ab3770b05407ac42014bde466b2a1b8f3',1,'nameser.h']]],
  ['ns_5fs_5fqd_17800',['ns_s_qd',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584a5ad5aacf780c52011d9bb4389c05269b',1,'nameser.h']]],
  ['ns_5fs_5fud_17801',['ns_s_ud',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584a476035c019406652d4c84181f667cfe7',1,'nameser.h']]],
  ['ns_5fs_5fzn_17802',['ns_s_zn',['../nameser_8h.html#adbdfe09293c53c30951ad6136294e584a5f58c6665d5602fe9e12062a0c09ef0b',1,'nameser.h']]],
  ['ns_5ft_5fa_17803',['ns_t_a',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90aa9599953ddca4ce2b127f46e803742f2',1,'nameser.h']]],
  ['ns_5ft_5fa6_17804',['ns_t_a6',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a415f47326cce9c95b715a1ad7b938f31',1,'nameser.h']]],
  ['ns_5ft_5faaaa_17805',['ns_t_aaaa',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90acb7fb5de403a2d5d2e3d58800974d102',1,'nameser.h']]],
  ['ns_5ft_5fafsdb_17806',['ns_t_afsdb',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a6c6580aade43919f63b9a8e651cf6ad5',1,'nameser.h']]],
  ['ns_5ft_5fany_17807',['ns_t_any',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4e40b401b6c8efde17393fad7fdf404a',1,'nameser.h']]],
  ['ns_5ft_5fapl_17808',['ns_t_apl',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90aa3db43412c16c6bc6faa911d01b71c49',1,'nameser.h']]],
  ['ns_5ft_5fatma_17809',['ns_t_atma',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a3c5d6668108549bdb26b848e30e9db19',1,'nameser.h']]],
  ['ns_5ft_5favc_17810',['ns_t_avc',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90aaf62c16bde4b85c762b8b1d110f1dca6',1,'nameser.h']]],
  ['ns_5ft_5faxfr_17811',['ns_t_axfr',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a0d0bee83063668d39a1a3b24054f4f96',1,'nameser.h']]],
  ['ns_5ft_5fcaa_17812',['ns_t_caa',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90af880b85ca30655ed149dc035995df51b',1,'nameser.h']]],
  ['ns_5ft_5fcdnskey_17813',['ns_t_cdnskey',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a8e5de38997e71d86ab1c7d2582b6e617',1,'nameser.h']]],
  ['ns_5ft_5fcds_17814',['ns_t_cds',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a11cd7df77875f305840d085f5bf72714',1,'nameser.h']]],
  ['ns_5ft_5fcert_17815',['ns_t_cert',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a7ff26f6822291947c3a3d5fc25956d87',1,'nameser.h']]],
  ['ns_5ft_5fcname_17816',['ns_t_cname',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90afd02b6a5a1c0551b2a106d0cb061d8cd',1,'nameser.h']]],
  ['ns_5ft_5fcsync_17817',['ns_t_csync',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ae225e71a3a43f9fe3bf5ce9f141eeeab',1,'nameser.h']]],
  ['ns_5ft_5fdhcid_17818',['ns_t_dhcid',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a81307576c20b9b358d1461b59f8986d3',1,'nameser.h']]],
  ['ns_5ft_5fdlv_17819',['ns_t_dlv',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4708961b1473ab73eab6bb45753075c9',1,'nameser.h']]],
  ['ns_5ft_5fdname_17820',['ns_t_dname',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a0f709fb39808522dc259498cea6fa36c',1,'nameser.h']]],
  ['ns_5ft_5fdnskey_17821',['ns_t_dnskey',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a5a01b9dcbfc12065d1c454913bdcb933',1,'nameser.h']]],
  ['ns_5ft_5fds_17822',['ns_t_ds',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a130e0fe3e549a6af939746581387b57a',1,'nameser.h']]],
  ['ns_5ft_5feid_17823',['ns_t_eid',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90af9f1453bf664255863b130d68958c8a4',1,'nameser.h']]],
  ['ns_5ft_5feui48_17824',['ns_t_eui48',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a034c71b54d486525b1f828d7ca421598',1,'nameser.h']]],
  ['ns_5ft_5feui64_17825',['ns_t_eui64',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90aa9c33aaeebe67ad03b1d4248b636bc86',1,'nameser.h']]],
  ['ns_5ft_5fgid_17826',['ns_t_gid',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90af9e9708a6b62938b27ab833bea98b6f0',1,'nameser.h']]],
  ['ns_5ft_5fgpos_17827',['ns_t_gpos',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a03788d1f2e0ef020931443fd3c81fb7c',1,'nameser.h']]],
  ['ns_5ft_5fhinfo_17828',['ns_t_hinfo',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ac874763c0fd9c4aa8dccf4fe901da87a',1,'nameser.h']]],
  ['ns_5ft_5fhip_17829',['ns_t_hip',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a142c047c0f23ec736bd329f39616a993',1,'nameser.h']]],
  ['ns_5ft_5finvalid_17830',['ns_t_invalid',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a52b7f190bf2300036ce0180a545d5c51',1,'nameser.h']]],
  ['ns_5ft_5fipseckey_17831',['ns_t_ipseckey',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a9f120eb07df8141a773d2b89ad1853dd',1,'nameser.h']]],
  ['ns_5ft_5fisdn_17832',['ns_t_isdn',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a460ca00381014dee0a84a2c453fb48ac',1,'nameser.h']]],
  ['ns_5ft_5fixfr_17833',['ns_t_ixfr',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ab2aa2a87a3c8cb74f4a3517d926e7304',1,'nameser.h']]],
  ['ns_5ft_5fkey_17834',['ns_t_key',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a051304494843dab237034c64df6e3354',1,'nameser.h']]],
  ['ns_5ft_5fkx_17835',['ns_t_kx',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a2f843681f7757fee2884ea2a36bf4fd5',1,'nameser.h']]],
  ['ns_5ft_5fl32_17836',['ns_t_l32',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4e844672c9a948a4bf0232b40f6551d4',1,'nameser.h']]],
  ['ns_5ft_5fl64_17837',['ns_t_l64',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a84d883c48d88531f287e209611a35280',1,'nameser.h']]],
  ['ns_5ft_5floc_17838',['ns_t_loc',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ac0187ddd96c2e92ff3fe58a68eddb06b',1,'nameser.h']]],
  ['ns_5ft_5flp_17839',['ns_t_lp',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4b25967be9905a6b38ffb4fad269fc29',1,'nameser.h']]],
  ['ns_5ft_5fmaila_17840',['ns_t_maila',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90abe3cbafb4ad258020db738de822de2c8',1,'nameser.h']]],
  ['ns_5ft_5fmailb_17841',['ns_t_mailb',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90afe54d04626ed599fb3bde53e243b9157',1,'nameser.h']]],
  ['ns_5ft_5fmax_17842',['ns_t_max',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ab3c0108a367075f88e2727d109ad68bc',1,'nameser.h']]],
  ['ns_5ft_5fmb_17843',['ns_t_mb',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ae411392dca8d97a1d758b445375f8e7d',1,'nameser.h']]],
  ['ns_5ft_5fmd_17844',['ns_t_md',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a9b13df9e225c669be9fb8d184fe97f18',1,'nameser.h']]],
  ['ns_5ft_5fmf_17845',['ns_t_mf',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a933f55f9798472bfee8bf018ba7fb3ca',1,'nameser.h']]],
  ['ns_5ft_5fmg_17846',['ns_t_mg',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90abae2654c3e79a69ce40f3952347e6d91',1,'nameser.h']]],
  ['ns_5ft_5fminfo_17847',['ns_t_minfo',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ac03f05cccc2e7c82dce734cf5ecd3c51',1,'nameser.h']]],
  ['ns_5ft_5fmr_17848',['ns_t_mr',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a175c6fe5ef7ed3887d0494a04df651a7',1,'nameser.h']]],
  ['ns_5ft_5fmx_17849',['ns_t_mx',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a443de20ccf1990d8eb44434c14f5da50',1,'nameser.h']]],
  ['ns_5ft_5fnaptr_17850',['ns_t_naptr',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a739491ab5be81e9c04566f7c902b157f',1,'nameser.h']]],
  ['ns_5ft_5fnid_17851',['ns_t_nid',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ac72a83628b871ef97ba7bc32a160ae19',1,'nameser.h']]],
  ['ns_5ft_5fnimloc_17852',['ns_t_nimloc',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a8490a626f7701fe85a4be79c02d3c773',1,'nameser.h']]],
  ['ns_5ft_5fninfo_17853',['ns_t_ninfo',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a6c96531cc9e1f7305c3f526d9204588a',1,'nameser.h']]],
  ['ns_5ft_5fns_17854',['ns_t_ns',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a6ab63fb871b987c5910aa9cb44a43482',1,'nameser.h']]],
  ['ns_5ft_5fnsap_17855',['ns_t_nsap',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90af5723e83c74b531d23393c080d60c995',1,'nameser.h']]],
  ['ns_5ft_5fnsap_5fptr_17856',['ns_t_nsap_ptr',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ae3b9164dbda4bc4ce667b28d7365d58c',1,'nameser.h']]],
  ['ns_5ft_5fnsec_17857',['ns_t_nsec',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a3fb2b1ca4a9e9f2e29c2773f8c9b59df',1,'nameser.h']]],
  ['ns_5ft_5fnsec3_17858',['ns_t_nsec3',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a67311886815d45bb5c0d1dd237780f1c',1,'nameser.h']]],
  ['ns_5ft_5fnsec3param_17859',['ns_t_nsec3param',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a74de1e1652c7da838f641d9a69532d15',1,'nameser.h']]],
  ['ns_5ft_5fnull_17860',['ns_t_null',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a223d6fa746a16c19065de6011ecc32d2',1,'nameser.h']]],
  ['ns_5ft_5fnxt_17861',['ns_t_nxt',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a7c01594dbe44a45ff41436c419582349',1,'nameser.h']]],
  ['ns_5ft_5fopenpgpkey_17862',['ns_t_openpgpkey',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a37d9f5c770f075b875bbdb784e3a1cb5',1,'nameser.h']]],
  ['ns_5ft_5fopt_17863',['ns_t_opt',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a2a56d3347bad585ad0dbabe0fcbf7801',1,'nameser.h']]],
  ['ns_5ft_5fptr_17864',['ns_t_ptr',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a20d0b49a0c151ba61ad8b94721ebc805',1,'nameser.h']]],
  ['ns_5ft_5fpx_17865',['ns_t_px',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90aefeaeebc3ccdd327a7618501d42a6a2f',1,'nameser.h']]],
  ['ns_5ft_5frkey_17866',['ns_t_rkey',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a8f7097d1b2197e1106f2bd37220f1871',1,'nameser.h']]],
  ['ns_5ft_5frp_17867',['ns_t_rp',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a279ff9d20ad89c24deaaa88089faae17',1,'nameser.h']]],
  ['ns_5ft_5frrsig_17868',['ns_t_rrsig',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4ef39581f34c2b463be05cb94740c980',1,'nameser.h']]],
  ['ns_5ft_5frt_17869',['ns_t_rt',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90aed76d01189bb43ca756c0dc03ba5e409',1,'nameser.h']]],
  ['ns_5ft_5fsig_17870',['ns_t_sig',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a52debf87f265ff0772e789c6d1144187',1,'nameser.h']]],
  ['ns_5ft_5fsink_17871',['ns_t_sink',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a16ced3a3c9cebe9ecb5a836b645c111d',1,'nameser.h']]],
  ['ns_5ft_5fsmimea_17872',['ns_t_smimea',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a8dde0ea0dd16f11bf2fe9b558e49ff3f',1,'nameser.h']]],
  ['ns_5ft_5fsoa_17873',['ns_t_soa',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a449bf4be38931cf4eee522850616e21c',1,'nameser.h']]],
  ['ns_5ft_5fspf_17874',['ns_t_spf',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a94d56b7c68fd9da250572a15e7a7fb42',1,'nameser.h']]],
  ['ns_5ft_5fsrv_17875',['ns_t_srv',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a5e5a92edc50d7d4bfb574b5668e76aab',1,'nameser.h']]],
  ['ns_5ft_5fsshfp_17876',['ns_t_sshfp',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a659ce381c75657d272e4904a1b5126e1',1,'nameser.h']]],
  ['ns_5ft_5fta_17877',['ns_t_ta',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a3f33edb3382b2a086991f794f5cf0082',1,'nameser.h']]],
  ['ns_5ft_5ftalink_17878',['ns_t_talink',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4bf5bded865fa16329b46f6c43cc304a',1,'nameser.h']]],
  ['ns_5ft_5ftkey_17879',['ns_t_tkey',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90abcaa5a3ed30d89e7aa92fc819790b900',1,'nameser.h']]],
  ['ns_5ft_5ftlsa_17880',['ns_t_tlsa',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a665d16eaeb5b124c6f2b24cf03f63a0b',1,'nameser.h']]],
  ['ns_5ft_5ftsig_17881',['ns_t_tsig',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90afcadabb4e65706661d9d0977457a4d6f',1,'nameser.h']]],
  ['ns_5ft_5ftxt_17882',['ns_t_txt',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ac564cc86841fbe8044f3bb438f54d371',1,'nameser.h']]],
  ['ns_5ft_5fuid_17883',['ns_t_uid',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a1c95342833d88ce2146c347ceac408f5',1,'nameser.h']]],
  ['ns_5ft_5fuinfo_17884',['ns_t_uinfo',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a635eeee43ed37a48aedd28c95679d126',1,'nameser.h']]],
  ['ns_5ft_5funspec_17885',['ns_t_unspec',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a450c6f8bd2bee06b0760f9089b79a828',1,'nameser.h']]],
  ['ns_5ft_5furi_17886',['ns_t_uri',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90ac5fc9c75b13860b3c9c0d7a2e3851530',1,'nameser.h']]],
  ['ns_5ft_5fwks_17887',['ns_t_wks',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a4cbbc34be74f4f04307564ce6d5b5880',1,'nameser.h']]],
  ['ns_5ft_5fx25_17888',['ns_t_x25',['../nameser_8h.html#aff8cddbefe0163bed2f38084955b2b90a8c7de495b67c21d87b19a64175023569',1,'nameser.h']]],
  ['ns_5fuop_5fadd_17889',['ns_uop_add',['../nameser_8h.html#a881001fac065e674e40d0287d15ec109ad919faacae0405bef76b8d0aa34c15e0',1,'nameser.h']]],
  ['ns_5fuop_5fdelete_17890',['ns_uop_delete',['../nameser_8h.html#a881001fac065e674e40d0287d15ec109a4635ff460b6d2ac0f4e182d78b17d0a7',1,'nameser.h']]],
  ['ns_5fuop_5fmax_17891',['ns_uop_max',['../nameser_8h.html#a881001fac065e674e40d0287d15ec109aa93162752df03538635271606b3e53c8',1,'nameser.h']]],
  ['nss_5fstatus_5fnotfound_17892',['NSS_STATUS_NOTFOUND',['../nss_8h.html#a1afd5e11f01212d239b1241cc63e9fd9a7826d77f40eb6d71baa31fb1cb39b47e',1,'nss.h']]],
  ['nss_5fstatus_5freturn_17893',['NSS_STATUS_RETURN',['../nss_8h.html#a1afd5e11f01212d239b1241cc63e9fd9ac07d55eb0741623a9647def325470a61',1,'nss.h']]],
  ['nss_5fstatus_5fsuccess_17894',['NSS_STATUS_SUCCESS',['../nss_8h.html#a1afd5e11f01212d239b1241cc63e9fd9a306334549351735318efccb7ef83efc7',1,'nss.h']]],
  ['nss_5fstatus_5ftryagain_17895',['NSS_STATUS_TRYAGAIN',['../nss_8h.html#a1afd5e11f01212d239b1241cc63e9fd9aac0d82f63f8c2070cb4a732098073258',1,'nss.h']]],
  ['nss_5fstatus_5funavail_17896',['NSS_STATUS_UNAVAIL',['../nss_8h.html#a1afd5e11f01212d239b1241cc63e9fd9a0e8ff5460548fab8848969df38a81825',1,'nss.h']]]
];
