var searchData=
[
  ['scm_5frights_18047',['SCM_RIGHTS',['../bits_2socket_8h.html#adf764cbdea00d65edcd07bb9953ad2b7a09dc94e1e7281624b572732a1a319021',1,'socket.h']]],
  ['sfd_5fcloexec_18048',['SFD_CLOEXEC',['../bits_2signalfd_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba6b41d08cc988a3ed1ca6de606ceb29bc',1,'signalfd.h']]],
  ['sfd_5fnonblock_18049',['SFD_NONBLOCK',['../bits_2signalfd_8h.html#a06fc87d81c62e9abb8790b6e5713c55baa7309fdbd6701d30ca0a4fc942deedc6',1,'signalfd.h']]],
  ['short_5finode_18050',['SHORT_INODE',['../personality_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba5899c520d7812f41df9f84b230bde78b',1,'personality.h']]],
  ['shut_5frd_18051',['SHUT_RD',['../sys_2socket_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba1c2982a59d309707236caafd09e43ecd',1,'socket.h']]],
  ['shut_5fwr_18052',['SHUT_WR',['../sys_2socket_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba1c9e3081960c88d43f9b6c686235a26e',1,'socket.h']]],
  ['si_5fasyncio_18053',['SI_ASYNCIO',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba6bc6ee17a5caf29ac7acb6951e415aef',1,'siginfo-consts.h']]],
  ['si_5fasyncnl_18054',['SI_ASYNCNL',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bac755480a3aa8644fa4ee23408c8dad19',1,'siginfo-consts.h']]],
  ['si_5fdethread_18055',['SI_DETHREAD',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba9933dd856b1b3f3d889bf360a273c5ed',1,'siginfo-consts.h']]],
  ['si_5fkernel_18056',['SI_KERNEL',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bace90155725a2f96e1cdeb0ea79aea549',1,'siginfo-consts.h']]],
  ['si_5fmesgq_18057',['SI_MESGQ',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba7443d8a198be73b7a41a9e2ab23ba4c9',1,'siginfo-consts.h']]],
  ['si_5fqueue_18058',['SI_QUEUE',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bab4fb10183f527be7c82b5b67783d6127',1,'siginfo-consts.h']]],
  ['si_5fsigio_18059',['SI_SIGIO',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55baec7ae161f1ee0c7693fe6b33230b7173',1,'siginfo-consts.h']]],
  ['si_5ftimer_18060',['SI_TIMER',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bab5bb35822b723a05df48499c72644adc',1,'siginfo-consts.h']]],
  ['si_5ftkill_18061',['SI_TKILL',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba77a4d2cb456f2dbbe013be04cf4dfc7a',1,'siginfo-consts.h']]],
  ['si_5fuser_18062',['SI_USER',['../siginfo-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bac3b22bb72bb2e0142913ea862e5c5245',1,'siginfo-consts.h']]],
  ['sigev_5fnone_18063',['SIGEV_NONE',['../sigevent-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bab50f50b8032db6a6ab72d06761aa3791',1,'sigevent-consts.h']]],
  ['sigev_5fsignal_18064',['SIGEV_SIGNAL',['../sigevent-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba32d3ada7b447fa5d6f9017979261ffbd',1,'sigevent-consts.h']]],
  ['sigev_5fthread_18065',['SIGEV_THREAD',['../sigevent-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55baf8415d2aab00beb8db03aa9a6bad7e20',1,'sigevent-consts.h']]],
  ['sigev_5fthread_5fid_18066',['SIGEV_THREAD_ID',['../sigevent-consts_8h.html#a06fc87d81c62e9abb8790b6e5713c55bad960cdfd6fc986dbe487c302910a5d5b',1,'sigevent-consts.h']]],
  ['sock_5fcloexec_18067',['SOCK_CLOEXEC',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fda5e40b65f834b510060565c497a9cb5e8',1,'socket_type.h']]],
  ['sock_5fdccp_18068',['SOCK_DCCP',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fdad65863c6a66dc3f193ad543dab7111b6',1,'socket_type.h']]],
  ['sock_5fdgram_18069',['SOCK_DGRAM',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fda006b373a518eeeb717573f91e70d7fcc',1,'socket_type.h']]],
  ['sock_5fnonblock_18070',['SOCK_NONBLOCK',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fda65d6d58ba64df05ea5342d417d6e17fd',1,'socket_type.h']]],
  ['sock_5fpacket_18071',['SOCK_PACKET',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fdadd9c48fb0c51bac8af6391a790ba3aa5',1,'socket_type.h']]],
  ['sock_5fraw_18072',['SOCK_RAW',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fdad78d54561daf9c4a7cda0ce115e3f231',1,'socket_type.h']]],
  ['sock_5frdm_18073',['SOCK_RDM',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fda09c45b73b01e07ba11a4c565f4e0b196',1,'socket_type.h']]],
  ['sock_5fseqpacket_18074',['SOCK_SEQPACKET',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fda10b933dcd29a563bf6b17c3fd513e4df',1,'socket_type.h']]],
  ['sock_5fstream_18075',['SOCK_STREAM',['../socket__type_8h.html#a832a2ee6ed10d8da825fc3a6e32db0fdae3b7fb9487113a31d403b23aaeaad424',1,'socket_type.h']]],
  ['ss_5fonstack_18076',['SS_ONSTACK',['../ss__flags_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba8ffbc32da64eddd752b8bf0489930505',1,'ss_flags.h']]],
  ['st_5fnosuid_18077',['ST_NOSUID',['../bits_2statvfs_8h.html#a06fc87d81c62e9abb8790b6e5713c55bae4dadf901d809430cffe5efdb65950dc',1,'statvfs.h']]],
  ['st_5frdonly_18078',['ST_RDONLY',['../bits_2statvfs_8h.html#a06fc87d81c62e9abb8790b6e5713c55badb21ad8cba9c343d305a1acca097214f',1,'statvfs.h']]],
  ['sticky_5ftimeouts_18079',['STICKY_TIMEOUTS',['../personality_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba4c4ec90af3337bf1fd6cbbcfd0bbd3fb',1,'personality.h']]]
];
