var searchData=
[
  ['open_14149',['open',['../fcntl2_8h.html#a113aa6ee9a861d4acb9c7462d6315a45',1,'open(const char *__path, int __oflag,...):&#160;fcntl2.h'],['../fcntl_8h.html#a593f525c4ac0135c9f06275378e92334',1,'open(const char *__file, int __oflag,...) __nonnull((1)):&#160;fcntl2.h']]],
  ['openat_14150',['openat',['../fcntl_8h.html#a1d938ab44dd526e97119850ac2729f7f',1,'fcntl.h']]],
  ['opendir_14151',['opendir',['../dirent_8h.html#ab6ddc78eb560dd346ed0636beaff97e1',1,'dirent.h']]],
  ['openlog_14152',['openlog',['../sys_2syslog_8h.html#a1926b66e857d779b0ab70d4d5b8c163b',1,'syslog.h']]],
  ['openpty_14153',['openpty',['../pty_8h.html#a1dd4775326ed5c2c8df9e83921ed453b',1,'pty.h']]],
  ['or_14154',['or',['../math-vector-fortran_8h.html#a76c81a1e46f244da36b641e97d265019',1,'math-vector-fortran.h']]]
];
