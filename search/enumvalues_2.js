var searchData=
[
  ['cert_5ft_5foid_17507',['cert_t_oid',['../nameser_8h.html#a9e1f8bb89643723e13ff22515ffc571fa1f076b29c7fee83f9d8d4608f296484d',1,'nameser.h']]],
  ['cert_5ft_5fpgp_17508',['cert_t_pgp',['../nameser_8h.html#a9e1f8bb89643723e13ff22515ffc571faa824227e6cf6f327e601ebea847c42cf',1,'nameser.h']]],
  ['cert_5ft_5fpkix_17509',['cert_t_pkix',['../nameser_8h.html#a9e1f8bb89643723e13ff22515ffc571fab7ebaf8dfaf6e99cb67dbd18f1f6412d',1,'nameser.h']]],
  ['cert_5ft_5fspki_17510',['cert_t_spki',['../nameser_8h.html#a9e1f8bb89643723e13ff22515ffc571fa1a4f809b6f1e7f44d0d2964867852b55',1,'nameser.h']]],
  ['cert_5ft_5furl_17511',['cert_t_url',['../nameser_8h.html#a9e1f8bb89643723e13ff22515ffc571fa840e2ae96e68084734d5849d5cb1a594',1,'nameser.h']]],
  ['codeset_17512',['CODESET',['../langinfo_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba5e3e63c89f8221f015c09513f4497c4b',1,'langinfo.h']]]
];
