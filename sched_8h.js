var sched_8h =
[
    [ "__need_size_t", "sched_8h.html#a2898830827f947f003d187388f7e6c72", null ],
    [ "__need_NULL", "sched_8h.html#a6c27666e43b87c08fa8b2e8aa324601c", null ],
    [ "__pid_t_defined", "sched_8h.html#a78397b687c0ddca84c27c22f8827a7a1", null ],
    [ "sched_priority", "sched_8h.html#a3b077140ce0400a06c5196d2ec0344d8", null ],
    [ "__sched_priority", "sched_8h.html#a6bba9484228b0aaf27ac51013831aeca", null ],
    [ "pid_t", "sched_8h.html#a63bef27b1fbd4a91faa273a387a38dc2", null ],
    [ "sched_setparam", "sched_8h.html#a4db66f3be4c964728ae554de27dd3e65", null ],
    [ "sched_getparam", "sched_8h.html#a554c73e5de8a5e093573b1ae0f395261", null ],
    [ "sched_setscheduler", "sched_8h.html#a77a0948278fd1e60f19a94f602ca66c9", null ],
    [ "sched_getscheduler", "sched_8h.html#a74e15b31acc70a6af5576df5f1de928d", null ],
    [ "sched_yield", "sched_8h.html#a960e3af53089fdbaa5a842eebbeaeb60", null ],
    [ "sched_get_priority_max", "sched_8h.html#a843bde691b64bd44c787fff7d72d0269", null ],
    [ "sched_get_priority_min", "sched_8h.html#ab6fa3009dd6e169370bc7bb80d9a156a", null ],
    [ "sched_rr_get_interval", "sched_8h.html#abe31856d8af8bd69feb8b0f18583a141", null ]
];