var bits_2sem_8h =
[
    [ "SEM_UNDO", "bits_2sem_8h.html#aefccb668e114dcb8679d2277037ed0f3", null ],
    [ "GETPID", "bits_2sem_8h.html#a7619c450d37a313aec4406af35c363ac", null ],
    [ "GETVAL", "bits_2sem_8h.html#ae5ff5308fcf0019e6049e2a021260e1f", null ],
    [ "GETALL", "bits_2sem_8h.html#a6e9419d7cf0814ee2a938f8b44838b97", null ],
    [ "GETNCNT", "bits_2sem_8h.html#a5eeaf104173e1c32bf4d2a09c2c94618", null ],
    [ "GETZCNT", "bits_2sem_8h.html#a14b89f6fc84ce7ea0b8a0f2f7ba570ba", null ],
    [ "SETVAL", "bits_2sem_8h.html#a43a37000d4dda76820e1d8b3a3cd6ade", null ],
    [ "SETALL", "bits_2sem_8h.html#af1d0fe980b0238f8d9a87315b71fc090", null ],
    [ "_SEM_SEMUN_UNDEFINED", "bits_2sem_8h.html#a029835c6635f55eab0928f0a2f9684eb", null ]
];