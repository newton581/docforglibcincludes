var nameser__compat_8h =
[
    [ "HEADER", "structHEADER.html", "structHEADER" ],
    [ "PACKETSZ", "nameser__compat_8h.html#aec9c73a8c49efe97e0545187fa8b856d", null ],
    [ "MAXDNAME", "nameser__compat_8h.html#a7e49780fcec2ca98abd7b5acb9c51991", null ],
    [ "MAXCDNAME", "nameser__compat_8h.html#a435ea8894e87701f66e9f36dbee3048e", null ],
    [ "MAXLABEL", "nameser__compat_8h.html#a57ef44e89326a371a47cfd186bf1f6a8", null ],
    [ "HFIXEDSZ", "nameser__compat_8h.html#ae4037ec89590e6cb22786be5319e7ad2", null ],
    [ "QFIXEDSZ", "nameser__compat_8h.html#a23b1a00ed028a175c227b33315cb696a", null ],
    [ "RRFIXEDSZ", "nameser__compat_8h.html#afa19d50689106533b5971913edcc34a3", null ],
    [ "INT32SZ", "nameser__compat_8h.html#a9dff6e3ce489e09dee1e90f407367775", null ],
    [ "INT16SZ", "nameser__compat_8h.html#af9f8961aa7a7fee1b23f4e8b16db2d22", null ],
    [ "INT8SZ", "nameser__compat_8h.html#a51fbeb7f4314cf26a19bedcede2923f7", null ],
    [ "INADDRSZ", "nameser__compat_8h.html#a001221f197fa18c54d6fc0833afe43ce", null ],
    [ "IN6ADDRSZ", "nameser__compat_8h.html#ad67a4ada9fe9e82b4814d0802f194ef9", null ],
    [ "INDIR_MASK", "nameser__compat_8h.html#a1c6be50272fd222ea4341cf8d7161ddb", null ],
    [ "NAMESERVER_PORT", "nameser__compat_8h.html#ab53a3cbe9cc44ecb87eb8e72ea7dc262", null ],
    [ "S_ZONE", "nameser__compat_8h.html#a7dc7d838dd79008ceeba896aaa275bb5", null ],
    [ "S_PREREQ", "nameser__compat_8h.html#ab8065b9c3fdf65ff0f6df50c642695c3", null ],
    [ "S_UPDATE", "nameser__compat_8h.html#ae52beec385f5cf9fae1947318f195ed1", null ],
    [ "S_ADDT", "nameser__compat_8h.html#a8e2dc428703445f78a41e38917201fd8", null ],
    [ "QUERY", "nameser__compat_8h.html#ae5ff1f63e632fba61c45cfb0a2b19066", null ],
    [ "IQUERY", "nameser__compat_8h.html#a019a06f90cd887d8022f3dcce1180b51", null ],
    [ "STATUS", "nameser__compat_8h.html#a59279bee44f34d08b3cbf3a89fb0d8d9", null ],
    [ "NS_NOTIFY_OP", "nameser__compat_8h.html#abf3ed5d1601d6b8ac5d61bc4f37abefb", null ],
    [ "NS_UPDATE_OP", "nameser__compat_8h.html#ac581d85cfe66906751d979f3e6f4c8b0", null ],
    [ "NOERROR", "nameser__compat_8h.html#a7c8e007a5b28cfb6d606095153d7e764", null ],
    [ "FORMERR", "nameser__compat_8h.html#ac80ccd3411b3398c6e8cd1a96627a4fa", null ],
    [ "SERVFAIL", "nameser__compat_8h.html#ad2a930d5bda1668f19001886613edbaa", null ],
    [ "NXDOMAIN", "nameser__compat_8h.html#ab710bad10a5e84f08cb36ed4950ff1b0", null ],
    [ "NOTIMP", "nameser__compat_8h.html#a78a3e1e631331ac035e38bea9196336b", null ],
    [ "REFUSED", "nameser__compat_8h.html#ac846cd5d68d1b8a88c2f77ddade87c6e", null ],
    [ "YXDOMAIN", "nameser__compat_8h.html#a1de510056f7e7e66851f16d2dc02d37d", null ],
    [ "YXRRSET", "nameser__compat_8h.html#a565a5c2701fba49099da45f557f74e8c", null ],
    [ "NXRRSET", "nameser__compat_8h.html#a741770195ddf8b69b587b4ca95b0d661", null ],
    [ "NOTAUTH", "nameser__compat_8h.html#a748a330b049c2a6b3fd1bc951a3c521f", null ],
    [ "NOTZONE", "nameser__compat_8h.html#a48c67a5955d4292e205f43ac9ad0d053", null ],
    [ "DELETE", "nameser__compat_8h.html#abbbe5949f3c1f72e439924f8cf503509", null ],
    [ "ADD", "nameser__compat_8h.html#a97fe5470fb1ac167c713671655ff3e52", null ],
    [ "T_A", "nameser__compat_8h.html#a11241ed27971fdd50a08f46de115cc89", null ],
    [ "T_NS", "nameser__compat_8h.html#a0a6ac5132f1f9199707b0b83d0c0ffbd", null ],
    [ "T_MD", "nameser__compat_8h.html#afbaecd243f545ada17ffab2da8f55296", null ],
    [ "T_MF", "nameser__compat_8h.html#a41b858a0bf5df3d62b590a1483255c9a", null ],
    [ "T_CNAME", "nameser__compat_8h.html#a3b1f450c75d14e4a17f4d157641999e5", null ],
    [ "T_SOA", "nameser__compat_8h.html#a951721e7ce0e59fbcb618f0036ebb4cb", null ],
    [ "T_MB", "nameser__compat_8h.html#ab96aaa2f27ec917f0fc981bb0977f0e9", null ],
    [ "T_MG", "nameser__compat_8h.html#aeb7938e0a0cd295c7c4a5e1f67cdb8d4", null ],
    [ "T_MR", "nameser__compat_8h.html#a400000cc23c229264dfc40bea52614d2", null ],
    [ "T_NULL", "nameser__compat_8h.html#afb8e7ff3357caa8dd3252695cefece51", null ],
    [ "T_WKS", "nameser__compat_8h.html#a87ac06dc0d7c15d15cefeadc97b0ba7e", null ],
    [ "T_PTR", "nameser__compat_8h.html#a26e251e7a38ee37f65a6895d49ccf17f", null ],
    [ "T_HINFO", "nameser__compat_8h.html#a0f4d4d3a8e1e38927ea2f4d92d28afb2", null ],
    [ "T_MINFO", "nameser__compat_8h.html#a946bd2b3282942fed85f943bc124562d", null ],
    [ "T_MX", "nameser__compat_8h.html#a379fb519f8a88b3f63f69e519d801cee", null ],
    [ "T_TXT", "nameser__compat_8h.html#a78854da665a47f26bc432d33a9e880ae", null ],
    [ "T_RP", "nameser__compat_8h.html#a7de5f753762756a9e198aa948e45e841", null ],
    [ "T_AFSDB", "nameser__compat_8h.html#a32efcb6ef40fc2bc546ecd7bd457552e", null ],
    [ "T_X25", "nameser__compat_8h.html#a0578e5bdd50d51105941a9c9bf980e37", null ],
    [ "T_ISDN", "nameser__compat_8h.html#ae451340e41889528075a785065c48e79", null ],
    [ "T_RT", "nameser__compat_8h.html#ab9a76f27f872767d27c8a7ac6fde5c48", null ],
    [ "T_NSAP", "nameser__compat_8h.html#a851cfabd3cce351da8a31eec1ef1bcf2", null ],
    [ "T_NSAP_PTR", "nameser__compat_8h.html#a7d2b3a8cbc57464b4f5e8388e6f60098", null ],
    [ "T_SIG", "nameser__compat_8h.html#a6e224ad0c2dba8c46b6be41640a23631", null ],
    [ "T_KEY", "nameser__compat_8h.html#a77d5459649c5af68a94ef9f378f8eb63", null ],
    [ "T_PX", "nameser__compat_8h.html#acf1931b3fe446dfd0953183cf663e7b0", null ],
    [ "T_GPOS", "nameser__compat_8h.html#adcd39a2fdca0578361ecdd7775132311", null ],
    [ "T_AAAA", "nameser__compat_8h.html#a9988cc1cc24b640d7fc9d8f602104405", null ],
    [ "T_LOC", "nameser__compat_8h.html#a44f0645b1fdbdea5a841e2c98486a686", null ],
    [ "T_NXT", "nameser__compat_8h.html#a46ff0be2310317b9776838d18eb4eaf1", null ],
    [ "T_EID", "nameser__compat_8h.html#a41085cbdde81a5b13c711cd154ec8ca1", null ],
    [ "T_NIMLOC", "nameser__compat_8h.html#a48c3e574fc82d25cbbe66bfe94bf8773", null ],
    [ "T_SRV", "nameser__compat_8h.html#ae4abc03ccc0e798b4057413522f5c9ed", null ],
    [ "T_ATMA", "nameser__compat_8h.html#a50a876a1e61b06b90234a53871e075e7", null ],
    [ "T_NAPTR", "nameser__compat_8h.html#a09c2681b3582aa1c71f804cc9983b777", null ],
    [ "T_KX", "nameser__compat_8h.html#a1cd76ea3b22b4dbf44e1e5d6f8cfc943", null ],
    [ "T_CERT", "nameser__compat_8h.html#a08332e53d6661271effd243fb076c410", null ],
    [ "T_A6", "nameser__compat_8h.html#a96a11dec3c0a637f35d767576718cea9", null ],
    [ "T_DNAME", "nameser__compat_8h.html#a5257c035e53a44519323d3b84ed79fed", null ],
    [ "T_SINK", "nameser__compat_8h.html#aef6de21bc77f91c0b605de46d08d2828", null ],
    [ "T_OPT", "nameser__compat_8h.html#ac53fc554ae53a5cab5e352faa9851e99", null ],
    [ "T_APL", "nameser__compat_8h.html#ae8072629e0ee8c21e5f1d0f0453249f2", null ],
    [ "T_DS", "nameser__compat_8h.html#ae4d8ea4ae5f3ff673f4e6db3875bcadf", null ],
    [ "T_SSHFP", "nameser__compat_8h.html#aa4ebc2e7545f6ecc0be8d32ed8925fed", null ],
    [ "T_IPSECKEY", "nameser__compat_8h.html#af858740b6ee566d3cefcc436e1344df4", null ],
    [ "T_RRSIG", "nameser__compat_8h.html#a2f8bb2045036ba572438572f06c74f55", null ],
    [ "T_NSEC", "nameser__compat_8h.html#aabacb9508f28833641a2d1307794ec13", null ],
    [ "T_DNSKEY", "nameser__compat_8h.html#aba62eff04d7c0bc4faeaeb92a43909a0", null ],
    [ "T_DHCID", "nameser__compat_8h.html#a8ed4a28e4a8f487ef99bacb87040ef6d", null ],
    [ "T_NSEC3", "nameser__compat_8h.html#ab9850373df2346de957247a0a0a907ff", null ],
    [ "T_NSEC3PARAM", "nameser__compat_8h.html#ae09da2776b8f1bd0b4360bc7734a33f9", null ],
    [ "T_TLSA", "nameser__compat_8h.html#aa13676b1d0ec600e6400d06dbefcc18c", null ],
    [ "T_SMIMEA", "nameser__compat_8h.html#a1b5619c874ccd3db5324b7adc748f118", null ],
    [ "T_HIP", "nameser__compat_8h.html#a75496873b3edeac6440c5fc41e1235aa", null ],
    [ "T_NINFO", "nameser__compat_8h.html#a2a6af50e5d02460e8a5f577bde882297", null ],
    [ "T_RKEY", "nameser__compat_8h.html#ae458dc3756e351a598053fa92fc9e787", null ],
    [ "T_TALINK", "nameser__compat_8h.html#af802031db707b47046063c014ed7c5ec", null ],
    [ "T_CDS", "nameser__compat_8h.html#a05c115face460fad481a4b80da3e66e0", null ],
    [ "T_CDNSKEY", "nameser__compat_8h.html#a70a139cc46f335b027a7263108879881", null ],
    [ "T_OPENPGPKEY", "nameser__compat_8h.html#a936fee2defad8162abef8a5da98d2a1f", null ],
    [ "T_CSYNC", "nameser__compat_8h.html#ad60108bd7c16e3bd6ea91f7fbc5fc02c", null ],
    [ "T_SPF", "nameser__compat_8h.html#a8c83b36fd7aff1a3c9cfa500a0c5dd02", null ],
    [ "T_UINFO", "nameser__compat_8h.html#afc0dbfbb74674695a09b1fe6059710ad", null ],
    [ "T_UID", "nameser__compat_8h.html#a966b7b0dbaf7b32df19a28c26e289a8c", null ],
    [ "T_GID", "nameser__compat_8h.html#a328644041848577dd873d25afba92299", null ],
    [ "T_UNSPEC", "nameser__compat_8h.html#ab614c1552f9933fee9584eec4d7e762e", null ],
    [ "T_NID", "nameser__compat_8h.html#a82c1f12128c88be2e18930f861b1bec7", null ],
    [ "T_L32", "nameser__compat_8h.html#ac534c8dae3f43a9f986dd4fa9a0e61d9", null ],
    [ "T_L64", "nameser__compat_8h.html#a1e3fade4b35bb22fa95c4a7e68887bba", null ],
    [ "T_LP", "nameser__compat_8h.html#aa70809eba763fb53f370d492288180f5", null ],
    [ "T_EUI48", "nameser__compat_8h.html#a44b06577f54f6a69b52869b113cd68f6", null ],
    [ "T_EUI64", "nameser__compat_8h.html#abfd229d95f3fbcb55897cfe84f98451a", null ],
    [ "T_TKEY", "nameser__compat_8h.html#af9009892624963a25001f3ab356c1107", null ],
    [ "T_TSIG", "nameser__compat_8h.html#ab5a381a3f62b867ea050e01e7fec138a", null ],
    [ "T_IXFR", "nameser__compat_8h.html#a39b0bbd946c70be964c9ebc7d2b924a0", null ],
    [ "T_AXFR", "nameser__compat_8h.html#a5c138445dd7a660d2c574c094a8fedfa", null ],
    [ "T_MAILB", "nameser__compat_8h.html#aeb3f3b5b73465a6ed020f667dc5a9bd0", null ],
    [ "T_MAILA", "nameser__compat_8h.html#a178e0dc3f735cdb0a0041651f987a2dc", null ],
    [ "T_ANY", "nameser__compat_8h.html#aaa8cccf76c5fbc1c7f8ece56a9226560", null ],
    [ "T_URI", "nameser__compat_8h.html#a6187d49fb59dd8f2fd5e8dae8a470c2a", null ],
    [ "T_CAA", "nameser__compat_8h.html#acc1e3b5279b295a6c5e91c8c34a6607e", null ],
    [ "T_AVC", "nameser__compat_8h.html#acf93114bdba4802b31a1e0ddf9ec3083", null ],
    [ "T_TA", "nameser__compat_8h.html#a78898c57e466fb112ebcb0138cf46cf3", null ],
    [ "T_DLV", "nameser__compat_8h.html#ab0afd83cface42455e9415ff039b00d5", null ],
    [ "C_IN", "nameser__compat_8h.html#a35948d7e3f6c9b1e187879d6fb1db4d8", null ],
    [ "C_CHAOS", "nameser__compat_8h.html#a1a4e22b4aae62b5054360e91b566624d", null ],
    [ "C_HS", "nameser__compat_8h.html#a13269220f12c5460425330c607e1b592", null ],
    [ "C_NONE", "nameser__compat_8h.html#a52714bc5888ffbb7be572e7bf0102f56", null ],
    [ "C_ANY", "nameser__compat_8h.html#a06925467b895f4750a285abeef6120de", null ],
    [ "GETSHORT", "nameser__compat_8h.html#ab6f3d0c18b639d4d6d170d4c06597f7c", null ],
    [ "GETLONG", "nameser__compat_8h.html#a5439d5495c8b769cc66c806e08439c15", null ],
    [ "PUTSHORT", "nameser__compat_8h.html#a3c43446cfc80ad80c64eb4e419ed3a7b", null ],
    [ "PUTLONG", "nameser__compat_8h.html#a274f2d5dcdbfd591d0156717b858462c", null ]
];