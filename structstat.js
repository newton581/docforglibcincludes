var structstat =
[
    [ "st_dev", "structstat.html#afb143a1534bf763afd6b353ea3cad102", null ],
    [ "__pad1", "structstat.html#a2b544e804fa0bd733c4eb1a4d7a73e9d", null ],
    [ "st_ino", "structstat.html#a1d7b4c479cacf6557db745dcc18ec603", null ],
    [ "st_mode", "structstat.html#ad0ca66d10647cc5f8aaf1d06ab34ed54", null ],
    [ "st_nlink", "structstat.html#a1d8d4cf9b348a7f706fef29aabd9bd13", null ],
    [ "st_uid", "structstat.html#a46d053fe9dd5e601d6c2e3f097819336", null ],
    [ "st_gid", "structstat.html#aa01a5406f3f4752281673e505aeea93f", null ],
    [ "st_rdev", "structstat.html#a19878d2506fccbada6f99ff8351e54fc", null ],
    [ "__pad2", "structstat.html#a9d8e2ceaec036a1891ad6d72ae6f8fd2", null ],
    [ "st_size", "structstat.html#af391341b7f8561e8cbd08602700990a5", null ],
    [ "st_blksize", "structstat.html#a147cec6b739f3e78af39f5adba5ad45b", null ],
    [ "st_blocks", "structstat.html#a4c3fb39947cc81328e6f8ab0cec14681", null ],
    [ "st_atime", "structstat.html#aecf68a6e9cfc67c79cedd48c91f3d7e0", null ],
    [ "st_atimensec", "structstat.html#aed59bb639238603e639bfc10af632ba7", null ],
    [ "st_mtime", "structstat.html#a953b03d9a906e32e6fd391a76b994051", null ],
    [ "st_mtimensec", "structstat.html#afcd4f0dc484e5b92e57fd166c39ba96e", null ],
    [ "st_ctime", "structstat.html#a13f9bf485fc42cdd9475ad82221008ad", null ],
    [ "st_ctimensec", "structstat.html#ae7eb94b03dcbf0fe3612fcd6f7bf9e6e", null ],
    [ "__glibc_reserved4", "structstat.html#a155ff9b91f88881a10f30c446e80b86a", null ],
    [ "__glibc_reserved5", "structstat.html#a0a411a2e4cedf1b305cdc88b448d6016", null ]
];