var bits_2utmpx_8h =
[
    [ "__exit_status", "struct____exit__status.html", "struct____exit__status" ],
    [ "utmpx", "structutmpx.html", "structutmpx" ],
    [ "__UT_LINESIZE", "bits_2utmpx_8h.html#a0e16c6c874252dc5f938e6fa41aeaa65", null ],
    [ "__UT_NAMESIZE", "bits_2utmpx_8h.html#a66d4b9e52343bbfc03595b24ee1ffeda", null ],
    [ "__UT_HOSTSIZE", "bits_2utmpx_8h.html#a9d3947a80a9b596308057d27619c7d2a", null ],
    [ "EMPTY", "bits_2utmpx_8h.html#a2b7cf2a3641be7b89138615764d60ba3", null ],
    [ "BOOT_TIME", "bits_2utmpx_8h.html#a5ab03916aa2a1644de4a108701c277f1", null ],
    [ "NEW_TIME", "bits_2utmpx_8h.html#a38916c043afd93f3f00a347ccf38c96d", null ],
    [ "OLD_TIME", "bits_2utmpx_8h.html#a1488cd47aa881be5e36958b523c0fe43", null ],
    [ "INIT_PROCESS", "bits_2utmpx_8h.html#aba829c3f490a7679aa7b74215346f6b8", null ],
    [ "LOGIN_PROCESS", "bits_2utmpx_8h.html#afce7999b601eaaf3071f8eca8b9806c3", null ],
    [ "USER_PROCESS", "bits_2utmpx_8h.html#aeea353e4cd967a42bff0d68d76ba92e4", null ],
    [ "DEAD_PROCESS", "bits_2utmpx_8h.html#afb52c6d8bcd23109edf0ebc5fa96bc78", null ]
];