var gshadow_8h =
[
    [ "sgrp", "structsgrp.html", "structsgrp" ],
    [ "__need_size_t", "gshadow_8h.html#a2898830827f947f003d187388f7e6c72", null ],
    [ "GSHADOW", "gshadow_8h.html#a52c7708d9e2a1b6a3144da01dc102e89", null ],
    [ "setsgent", "gshadow_8h.html#a4a6e5fbee08a081a3fd8b0ee90dee172", null ],
    [ "endsgent", "gshadow_8h.html#a0fb18179bc91d369b91ea8abd1157022", null ],
    [ "getsgent", "gshadow_8h.html#a0aeff8a3f741d92094095d732e91e662", null ],
    [ "getsgnam", "gshadow_8h.html#a2baa6db3835311b4a3e2a79e31fc092c", null ],
    [ "sgetsgent", "gshadow_8h.html#ab029db21718e7d1ce68ae5c74d913e15", null ],
    [ "fgetsgent", "gshadow_8h.html#aa8b6d69a035f0eda0cb3a7848f6b6213", null ],
    [ "putsgent", "gshadow_8h.html#ad68493da1aebaa520a68bac47848fd57", null ],
    [ "getsgent_r", "gshadow_8h.html#a36b1670679967fb0bbbab59f8f690281", null ],
    [ "getsgnam_r", "gshadow_8h.html#afc1a4142c1050fe93b52050e1f775ba9", null ],
    [ "sgetsgent_r", "gshadow_8h.html#af311cd9a14f9b4c0187df540ac346a08", null ],
    [ "fgetsgent_r", "gshadow_8h.html#a3b23129568ec7ba7dd8a2a090d796767", null ]
];