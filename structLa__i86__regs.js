var structLa__i86__regs =
[
    [ "lr_edx", "structLa__i86__regs.html#acc2453233349cd082c4dceca4dabc81d", null ],
    [ "lr_ecx", "structLa__i86__regs.html#a51ea9ae1a7cc041f7a6a1b58e16df1b7", null ],
    [ "lr_eax", "structLa__i86__regs.html#a6a6de1325bf1edb171a4360770b37e74", null ],
    [ "lr_ebp", "structLa__i86__regs.html#a1bd44f4d3137cefdfbbc0b6ba41dacd8", null ],
    [ "lr_esp", "structLa__i86__regs.html#a16a979cd13c8ca07ac40e7fb7837c39c", null ]
];