var structFTS =
[
    [ "fts_cur", "structFTS.html#a08ecc2deb063af1798129d9313546826", null ],
    [ "fts_child", "structFTS.html#a9f171b866eb11d48d7da97257745fc13", null ],
    [ "fts_array", "structFTS.html#a53df707bc19d57beb6e62bc367926d06", null ],
    [ "fts_dev", "structFTS.html#aa3c11f29f2bbc6b9e94210d64c3b4186", null ],
    [ "fts_path", "structFTS.html#ad926a226b225fc21c7808f880649be5d", null ],
    [ "fts_rfd", "structFTS.html#a3a5fc5f85de1c28beaed4c39d7679412", null ],
    [ "fts_pathlen", "structFTS.html#aea68b3302cc8e8256ec5799432a3752b", null ],
    [ "fts_nitems", "structFTS.html#afc2dc8df41ca182f85489dfe4ce38c4d", null ],
    [ "fts_compar", "structFTS.html#ac4fa5419634498c49e0f5df26b63be87", null ],
    [ "fts_options", "structFTS.html#ab92db932926b32e5f727745c8e9fb26d", null ]
];