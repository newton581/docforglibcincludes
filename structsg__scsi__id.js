var structsg__scsi__id =
[
    [ "host_no", "structsg__scsi__id.html#a8e206cbc344270ec75fcca97b30db77d", null ],
    [ "channel", "structsg__scsi__id.html#a4993c85cf9b29648889463319b417dc9", null ],
    [ "scsi_id", "structsg__scsi__id.html#afd04e2576b6a00d98a74c2652d395492", null ],
    [ "lun", "structsg__scsi__id.html#a62a9c3f6485b1eb0d3afe18b4754f036", null ],
    [ "scsi_type", "structsg__scsi__id.html#a6adedc78b34a37a36521a50563dd7107", null ],
    [ "h_cmd_per_lun", "structsg__scsi__id.html#a259da5789935d57ec8ab577d30ed3484", null ],
    [ "d_queue_depth", "structsg__scsi__id.html#a54d6e94b62791ee2ea26b55f221a5809", null ],
    [ "unused", "structsg__scsi__id.html#ad7534ae62076a1326674d69d785f6bb3", null ]
];