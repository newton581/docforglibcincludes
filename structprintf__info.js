var structprintf__info =
[
    [ "prec", "structprintf__info.html#a073a8fdf4633c71c25de21c2f07341a1", null ],
    [ "width", "structprintf__info.html#a777bbd85334fb65f3b152e3d25d4ffc5", null ],
    [ "spec", "structprintf__info.html#aab42c1ea8e703270576e46a8e3e4578d", null ],
    [ "is_long_double", "structprintf__info.html#a259ab87f6669638d70dcf5ae78e8149e", null ],
    [ "is_short", "structprintf__info.html#ad0fe4bb056f1f00787bdab5a9f0823b2", null ],
    [ "is_long", "structprintf__info.html#af86ef3f9382d44a96dbb13a11cb244c6", null ],
    [ "alt", "structprintf__info.html#ab3494d19c3056274949b4b99b740a5c4", null ],
    [ "space", "structprintf__info.html#ab44b9cf30e92eb7dbdd02ed3d6f4f3bd", null ],
    [ "left", "structprintf__info.html#a118867fe1773d16870c932d363574b32", null ],
    [ "showsign", "structprintf__info.html#ad8d50200ff8e7f150095866a9419f0e3", null ],
    [ "group", "structprintf__info.html#a76548c9abb57f691c2f39bd3e5512479", null ],
    [ "extra", "structprintf__info.html#a3e7d2fb107cca4e9f33ac2a4a112b92a", null ],
    [ "is_char", "structprintf__info.html#a8783f9db7c084f59bed955ef8ae4e283", null ],
    [ "wide", "structprintf__info.html#a4e2da7324d8ce657f8f7166014966bdf", null ],
    [ "i18n", "structprintf__info.html#af9e3530e36c63e72667cd25bfe09671f", null ],
    [ "is_binary128", "structprintf__info.html#a0ce3ec000cba56866d737dfaa28793e9", null ],
    [ "__pad", "structprintf__info.html#a60c7cd28853dcf726e8593907597e70e", null ],
    [ "user", "structprintf__info.html#ae891395600211dc414e2bdd9be6744d3", null ],
    [ "pad", "structprintf__info.html#a7ab014e4457af8919a45af43719b1c0d", null ]
];