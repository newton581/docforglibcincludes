var termios_baud_8h =
[
    [ "B57600", "termios-baud_8h.html#a4bde6a10c31adfe1959fb9724e13029a", null ],
    [ "B115200", "termios-baud_8h.html#aef3a1340ea0fba1573691a9e74a43d11", null ],
    [ "B230400", "termios-baud_8h.html#aa0a8a3b09c593c7be5bfbbeb09f14d76", null ],
    [ "B460800", "termios-baud_8h.html#a644a0912fa1e82fd24c0516b4723ee59", null ],
    [ "B500000", "termios-baud_8h.html#a6269f12a17dbd41042e0d4c897a89b96", null ],
    [ "B576000", "termios-baud_8h.html#a227c117ac1c7d60d2728f5d68aaf79a0", null ],
    [ "B921600", "termios-baud_8h.html#ae2d3cdb046f933a4a6ccd5497516c2a5", null ],
    [ "B1000000", "termios-baud_8h.html#a32dc4578657dac59f79f0bc7ffc9416f", null ],
    [ "B1152000", "termios-baud_8h.html#a7dc1d184634d970682e5b138686c5a20", null ],
    [ "B1500000", "termios-baud_8h.html#a9ea600f5048d4b2b1dfa1249521701bd", null ],
    [ "B2000000", "termios-baud_8h.html#ac8f494082416f4d63fde6c6289c4968d", null ],
    [ "B2500000", "termios-baud_8h.html#a0df33f40257015142d65eb544176dda5", null ],
    [ "B3000000", "termios-baud_8h.html#acb80c15451346a6ae479344124cea120", null ],
    [ "B3500000", "termios-baud_8h.html#a0db8961ca1fb43dbb690b6d2ee7217f8", null ],
    [ "B4000000", "termios-baud_8h.html#a0078dd80905a2b3dbccff97e5bea8054", null ],
    [ "__MAX_BAUD", "termios-baud_8h.html#a2f32f7fec7556a8fff4a78e6a29f132b", null ]
];