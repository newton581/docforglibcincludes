var posix2__lim_8h =
[
    [ "_POSIX2_BC_BASE_MAX", "posix2__lim_8h.html#a0be7c8beefa6e928586ea03c82714739", null ],
    [ "_POSIX2_BC_DIM_MAX", "posix2__lim_8h.html#ab9a92bd0c5ef6154955ed8f9014a3cab", null ],
    [ "_POSIX2_BC_SCALE_MAX", "posix2__lim_8h.html#a5ae8e4a7682fa9840c21ba6c90e775fd", null ],
    [ "_POSIX2_BC_STRING_MAX", "posix2__lim_8h.html#a13f5f2a7adf95a84f5d57a67bbf007a5", null ],
    [ "_POSIX2_COLL_WEIGHTS_MAX", "posix2__lim_8h.html#ab12d459f79de6de0853cd7c993cc34ee", null ],
    [ "_POSIX2_EXPR_NEST_MAX", "posix2__lim_8h.html#a72cfb82396fd0cac222b9e1cc9a93f47", null ],
    [ "_POSIX2_LINE_MAX", "posix2__lim_8h.html#a55adec3d70aaa49ecf8a19dcf423aa77", null ],
    [ "_POSIX2_RE_DUP_MAX", "posix2__lim_8h.html#a95ce5aa528eac29d587d4d9e0c6d268e", null ],
    [ "_POSIX2_CHARCLASS_NAME_MAX", "posix2__lim_8h.html#a24bbc516af0aed01d96ed634de934c5b", null ],
    [ "BC_BASE_MAX", "posix2__lim_8h.html#ad9163fb9bc7582e3d7401807e1d9b67f", null ],
    [ "BC_DIM_MAX", "posix2__lim_8h.html#a2217a386f6ec94149608342b14615534", null ],
    [ "BC_SCALE_MAX", "posix2__lim_8h.html#a59cebe9dd21b1bcc4ebc084b3c9745ce", null ],
    [ "BC_STRING_MAX", "posix2__lim_8h.html#a0410b84da7eb186d0dc410b1d1e527f3", null ],
    [ "COLL_WEIGHTS_MAX", "posix2__lim_8h.html#a464c8aafd7adb5e59db973bf2e695fd8", null ],
    [ "EXPR_NEST_MAX", "posix2__lim_8h.html#a8658a06f37ba6ddc698bc4eed25ed94a", null ],
    [ "LINE_MAX", "posix2__lim_8h.html#aae3371f3a2ed27aefae9c4e26982fc99", null ],
    [ "CHARCLASS_NAME_MAX", "posix2__lim_8h.html#a4d7b5062719e4bbb66f269d54d0a4aae", null ],
    [ "RE_DUP_MAX", "posix2__lim_8h.html#aa7d072d0a88c61d3692dc547ea27538e", null ]
];