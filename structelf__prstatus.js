var structelf__prstatus =
[
    [ "pr_info", "structelf__prstatus.html#a2ca898878a9b306f1c4c65a65d537dfe", null ],
    [ "pr_cursig", "structelf__prstatus.html#ae9d3fd77cef8c8ba4eb506232abe8b68", null ],
    [ "pr_sigpend", "structelf__prstatus.html#a9ef818a34a6459aebae9f5780fbfa2c6", null ],
    [ "pr_sighold", "structelf__prstatus.html#a71d75a73d548a342dc9a16e0ef4a81b5", null ],
    [ "pr_pid", "structelf__prstatus.html#a8e6ac7a802a6b8cb448324fa6b6ea4a3", null ],
    [ "pr_ppid", "structelf__prstatus.html#ae3c422c32c686a706c4c929d7d40f09c", null ],
    [ "pr_pgrp", "structelf__prstatus.html#ad4e373a2d2e1401e7650d6470116d786", null ],
    [ "pr_sid", "structelf__prstatus.html#aee850fa68acaddc6f3247441c91cbb5c", null ],
    [ "pr_utime", "structelf__prstatus.html#ab9ca9e23c0e0a38b4ca8fb6e9520a9b5", null ],
    [ "pr_stime", "structelf__prstatus.html#a63eec14d1c3cb81f0bd622faef1292a2", null ],
    [ "pr_cutime", "structelf__prstatus.html#a864059038237647b83ee71590032cbb1", null ],
    [ "pr_cstime", "structelf__prstatus.html#a3ddb81d59df953bd7ebb1f7d823ad11e", null ],
    [ "pr_reg", "structelf__prstatus.html#a6e6be4178890efae96f586365d07ee8b", null ],
    [ "pr_fpvalid", "structelf__prstatus.html#a38d404be781fe1432c3569b809354d7e", null ]
];