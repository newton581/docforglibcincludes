var structacct__v3 =
[
    [ "ac_flag", "structacct__v3.html#a8c05230e7021d25342b379d001000557", null ],
    [ "ac_version", "structacct__v3.html#af053be28bb418a8301cfe7814729796e", null ],
    [ "ac_tty", "structacct__v3.html#a8142bd2f75730dc39758ad506b9ae3a9", null ],
    [ "ac_exitcode", "structacct__v3.html#a8f4a30fd248436aafd87da2aca3ca333", null ],
    [ "ac_uid", "structacct__v3.html#a439f8234727b7c51c6adabc9902507c7", null ],
    [ "ac_gid", "structacct__v3.html#acb13b82b218fc3247f1f88dbb9aaf28f", null ],
    [ "ac_pid", "structacct__v3.html#a2daf34c9732269152e8096c6026a4c2a", null ],
    [ "ac_ppid", "structacct__v3.html#a29f7cce299216f96b99de6276a190f05", null ],
    [ "ac_btime", "structacct__v3.html#a8c5a46f0ba47e60e75aa997aefb4b5e7", null ],
    [ "ac_etime", "structacct__v3.html#ae70bca989faac1b35bc3be8596d0f7e5", null ],
    [ "ac_utime", "structacct__v3.html#a22ddc1e27b7a67ae7373f20b51b4e2b2", null ],
    [ "ac_stime", "structacct__v3.html#a9ce3966004958ee10803097a3d2f5045", null ],
    [ "ac_mem", "structacct__v3.html#ad6d2ef8e0c8cf9582e0baeb7423ef58b", null ],
    [ "ac_io", "structacct__v3.html#abc078e669807923540f1bbb2599d74f5", null ],
    [ "ac_rw", "structacct__v3.html#a24cc332df20e2ad092a27454bef8c9d9", null ],
    [ "ac_minflt", "structacct__v3.html#a82bb7d62b9d6d2ee2d170305f91d4f33", null ],
    [ "ac_majflt", "structacct__v3.html#aea80108aa37fa2184d7e206a834c5ef7", null ],
    [ "ac_swaps", "structacct__v3.html#a8d9a1a71da58af923e4d7e038158dfe5", null ],
    [ "ac_comm", "structacct__v3.html#a92393428a696ea480cb8c6a9e87f387a", null ]
];