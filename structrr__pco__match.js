var structrr__pco__match =
[
    [ "rpm_code", "structrr__pco__match.html#a43d1c4e02c197ded3fbb9870ed0a25d1", null ],
    [ "rpm_len", "structrr__pco__match.html#a9c241c4a99000e4c94d995e4b04cf77a", null ],
    [ "rpm_ordinal", "structrr__pco__match.html#a7222dff0a224d2ccd89dd18a4fd972eb", null ],
    [ "rpm_matchlen", "structrr__pco__match.html#a2b90c6bb8f541db08930cbcc7ecdf4c8", null ],
    [ "rpm_minlen", "structrr__pco__match.html#a716871f258063a07190980c44b8f622d", null ],
    [ "rpm_maxlen", "structrr__pco__match.html#a6c111ca6647059f61c0dad7ac5d63f5a", null ],
    [ "rpm_reserved", "structrr__pco__match.html#a9c7244e13629c1cccdac354eb7a9fe2e", null ],
    [ "rpm_prefix", "structrr__pco__match.html#a7e6329fc5031b52460098f3dab4f5bbf", null ]
];