var structstatfs =
[
    [ "f_type", "structstatfs.html#ab8661d90bda8d34754e458a555fd3d73", null ],
    [ "f_bsize", "structstatfs.html#a106e9702858e680ca051ec54fe6d4725", null ],
    [ "f_blocks", "structstatfs.html#aeb5a74d1795e2f9d095919b27cb30165", null ],
    [ "f_bfree", "structstatfs.html#a8b5e631aca603639231c103ecfa99ef9", null ],
    [ "f_bavail", "structstatfs.html#a34347ea77d000b9ecc6d634fb27d04f1", null ],
    [ "f_files", "structstatfs.html#a8b1179747c963107038a329e30f4e491", null ],
    [ "f_ffree", "structstatfs.html#ac9fcff67e2fc24803b460e4734212918", null ],
    [ "f_fsid", "structstatfs.html#a4cdcd262df8309f395a81805c5daafdb", null ],
    [ "f_namelen", "structstatfs.html#afc76917b1b930b1d03c642c9e7647498", null ],
    [ "f_frsize", "structstatfs.html#af81732fc7d80eeb791be5ad1bd68754d", null ],
    [ "f_flags", "structstatfs.html#ab7c2e51c33981783a7ed7d3243a91958", null ],
    [ "f_spare", "structstatfs.html#aad1159615f21fab710055bc77f91bb08", null ]
];