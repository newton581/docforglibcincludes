var structelf__prpsinfo =
[
    [ "pr_state", "structelf__prpsinfo.html#add77898e17cc10e511a5952cf9bf4d41", null ],
    [ "pr_sname", "structelf__prpsinfo.html#a6eac9cbba2b280012e96638d2f9d7e2a", null ],
    [ "pr_zomb", "structelf__prpsinfo.html#a5a2fbc642383a3fd3ba1c2f281753669", null ],
    [ "pr_nice", "structelf__prpsinfo.html#a9fb2ede9f09158806b3d8ad5c638c06c", null ],
    [ "pr_flag", "structelf__prpsinfo.html#aee9c18db6fa206cb93cb911cbade4867", null ],
    [ "pr_uid", "structelf__prpsinfo.html#a707e628fd57f96807cf8bfd13210ea39", null ],
    [ "pr_gid", "structelf__prpsinfo.html#ac8a16fb65f54dcba5300b9c52174b701", null ],
    [ "pr_pid", "structelf__prpsinfo.html#a65b0ca7bef3ad5e55b84369ae10f3838", null ],
    [ "pr_ppid", "structelf__prpsinfo.html#af6696b228cb81c21085c40613dcf6392", null ],
    [ "pr_pgrp", "structelf__prpsinfo.html#a373b7ded27b0e3c3b59b316b27a40f2c", null ],
    [ "pr_sid", "structelf__prpsinfo.html#aa3c6c366058b54252c3a48f0ab0093b9", null ],
    [ "pr_fname", "structelf__prpsinfo.html#a08be8cefa511939b41284f66315d520a", null ],
    [ "pr_psargs", "structelf__prpsinfo.html#a30aebe7f0324e8cc0235b59ae7ea63c6", null ]
];