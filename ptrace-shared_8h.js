var ptrace_shared_8h =
[
    [ "__ptrace_peeksiginfo_args", "struct____ptrace__peeksiginfo__args.html", "struct____ptrace__peeksiginfo__args" ],
    [ "__ptrace_seccomp_metadata", "struct____ptrace__seccomp__metadata.html", "struct____ptrace__seccomp__metadata" ],
    [ "__ptrace_syscall_info", "struct____ptrace__syscall__info.html", "struct____ptrace__syscall__info" ],
    [ "__ptrace_setoptions", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4ed", [
      [ "PTRACE_O_TRACESYSGOOD", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edabe4bb1388ad0f18bf9389feadb92e597", null ],
      [ "PTRACE_O_TRACEFORK", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edad51f76045ffa687773e95b8c06f12172", null ],
      [ "PTRACE_O_TRACEVFORK", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4eda5ce6e41c48011c2b99cf6d3e95905fe0", null ],
      [ "PTRACE_O_TRACECLONE", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4eda5cea2255b0d167c82f87a08b9328a479", null ],
      [ "PTRACE_O_TRACEEXEC", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edabf25b09fae0fdc1cbeeb920810df7858", null ],
      [ "PTRACE_O_TRACEVFORKDONE", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4eda24a5df41482a5d85b2527b9643a9c26a", null ],
      [ "PTRACE_O_TRACEEXIT", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edaca323fdefd7138b0343557fa96bef798", null ],
      [ "PTRACE_O_TRACESECCOMP", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4eda09ac86e8d8f16ce699266151032f511f", null ],
      [ "PTRACE_O_EXITKILL", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edaa1c42c25139e95e7d1deafb187d22bc5", null ],
      [ "PTRACE_O_SUSPEND_SECCOMP", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edaa6e68e2080edeae8de17d0f9cacc442c", null ],
      [ "PTRACE_O_MASK", "ptrace-shared_8h.html#a9d6cf5f83b2d79cbf48487295499b4edac19a8db30a06ac15f04296068f82ae95", null ]
    ] ],
    [ "__ptrace_eventcodes", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276e", [
      [ "PTRACE_EVENT_FORK", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276ea031fc1389fa8bab624e88c49518313f5", null ],
      [ "PTRACE_EVENT_VFORK", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276ea5f7829588a39ac791446a966088d501c", null ],
      [ "PTRACE_EVENT_CLONE", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276ea3f99ce1870d0bfe8a857c0c62ef3a622", null ],
      [ "PTRACE_EVENT_EXEC", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276eacc9e86963ad1e549c9d370e6568428bd", null ],
      [ "PTRACE_EVENT_VFORK_DONE", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276eae479850dd9e5b2f6dbc6a67c99cb56d4", null ],
      [ "PTRACE_EVENT_EXIT", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276ea8da6ddac68f18c8acebe32914872a4c6", null ],
      [ "PTRACE_EVENT_SECCOMP", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276eadd8df8bd973f034b9566a8e848d39279", null ],
      [ "PTRACE_EVENT_STOP", "ptrace-shared_8h.html#ae3762a1fbfa494fc1f1619ad5fe3276ea08d578c95511cb9d9a58a4251e7a9511", null ]
    ] ],
    [ "__ptrace_get_syscall_info_op", "ptrace-shared_8h.html#a0f0f95d059f6e898560f10e8114830a2", [
      [ "PTRACE_SYSCALL_INFO_NONE", "ptrace-shared_8h.html#a0f0f95d059f6e898560f10e8114830a2ab6abd186a1ffa5d642b9eac5c0c1128b", null ],
      [ "PTRACE_SYSCALL_INFO_ENTRY", "ptrace-shared_8h.html#a0f0f95d059f6e898560f10e8114830a2ace50b2df68dbad690cb4d5325310c8b7", null ],
      [ "PTRACE_SYSCALL_INFO_EXIT", "ptrace-shared_8h.html#a0f0f95d059f6e898560f10e8114830a2a272788879d765d8fb881afa5448edf5b", null ],
      [ "PTRACE_SYSCALL_INFO_SECCOMP", "ptrace-shared_8h.html#a0f0f95d059f6e898560f10e8114830a2a7cddb96c56332cdc1157f8ab6d9fdc05", null ]
    ] ],
    [ "__ptrace_peeksiginfo_flags", "ptrace-shared_8h.html#aa1d546cf916634a26bd8ab115844e3a4", [
      [ "PTRACE_PEEKSIGINFO_SHARED", "ptrace-shared_8h.html#aa1d546cf916634a26bd8ab115844e3a4abc04e0f7a059ca8767a625f45c8ec622", null ]
    ] ],
    [ "ptrace", "ptrace-shared_8h.html#ae12b1cd813b59098b4a000706009bf03", null ]
];