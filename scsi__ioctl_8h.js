var scsi__ioctl_8h =
[
    [ "SCSI_IOCTL_SEND_COMMAND", "scsi__ioctl_8h.html#af9dd15b01ad57c60cebf942d395bf2f0", null ],
    [ "SCSI_IOCTL_TEST_UNIT_READY", "scsi__ioctl_8h.html#ad681278e412d7f0fa3612bd7de315891", null ],
    [ "SCSI_IOCTL_BENCHMARK_COMMAND", "scsi__ioctl_8h.html#ac7ca58330b07ae1a83a7622a4d930d71", null ],
    [ "SCSI_IOCTL_SYNC", "scsi__ioctl_8h.html#a2f0bb8eb638eebc04b6d1ea4596736d1", null ],
    [ "SCSI_IOCTL_START_UNIT", "scsi__ioctl_8h.html#ab7c5a97e62462fd2e8e3fe074a84399b", null ],
    [ "SCSI_IOCTL_STOP_UNIT", "scsi__ioctl_8h.html#a0715d0b84ffed32523edf32fa08314ea", null ],
    [ "SCSI_IOCTL_DOORLOCK", "scsi__ioctl_8h.html#a056c3dd7eabe8423d7894f86d7ccfdc0", null ],
    [ "SCSI_IOCTL_DOORUNLOCK", "scsi__ioctl_8h.html#a13f4bbf454d48e677d484e347652d85c", null ]
];