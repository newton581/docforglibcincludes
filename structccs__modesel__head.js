var structccs__modesel__head =
[
    [ "_r1", "structccs__modesel__head.html#ab0a5afb8a1aad09a8336aca55eeec147", null ],
    [ "medium", "structccs__modesel__head.html#a124456ad184c50c279755f5915c03436", null ],
    [ "_r2", "structccs__modesel__head.html#a548422783084ae444be90f7b1efac7d0", null ],
    [ "block_desc_length", "structccs__modesel__head.html#a811c892f863414d710ec9299da6f3d10", null ],
    [ "density", "structccs__modesel__head.html#ab7aaa829d48bb446770ca5e60b115104", null ],
    [ "number_blocks_hi", "structccs__modesel__head.html#a9e887cd02d886f0ac2a8d3e5a5f60508", null ],
    [ "number_blocks_med", "structccs__modesel__head.html#a08432ef7e339a7cc6fd12224a40ab219", null ],
    [ "number_blocks_lo", "structccs__modesel__head.html#a90f606f0bd2ab4ba458b2dd86a86e5ad", null ],
    [ "_r3", "structccs__modesel__head.html#a9a37eb5245ba0f4cce8c1578a54e39af", null ],
    [ "block_length_hi", "structccs__modesel__head.html#ad5810c2d5265b6c346104331d1655322", null ],
    [ "block_length_med", "structccs__modesel__head.html#a36ce748328fcf825e73f959abfbb842f", null ],
    [ "block_length_lo", "structccs__modesel__head.html#aa8b2cf7f2228e818d73e1553780a1963", null ]
];