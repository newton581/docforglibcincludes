var fpu__control_8h =
[
    [ "_FPU_MASK_IM", "fpu__control_8h.html#a2bd512529409eb2fab6e429c8e360947", null ],
    [ "_FPU_MASK_DM", "fpu__control_8h.html#ad3eea1cf16c2781de32a2638a6b8baa1", null ],
    [ "_FPU_MASK_ZM", "fpu__control_8h.html#a307ad8e13360fd78ffe669f8b3aff02f", null ],
    [ "_FPU_MASK_OM", "fpu__control_8h.html#aec23ca2a4c702432eed4e32178424d81", null ],
    [ "_FPU_MASK_UM", "fpu__control_8h.html#a3ccb2d2d2eab31d396a40712c1193280", null ],
    [ "_FPU_MASK_PM", "fpu__control_8h.html#a89f037d5d1290f81df654815aa6a6ef8", null ],
    [ "_FPU_EXTENDED", "fpu__control_8h.html#a794f39e9512b954a887e95466dd88ebe", null ],
    [ "_FPU_DOUBLE", "fpu__control_8h.html#a6eac90096cc12acbea4f73e4c66faf24", null ],
    [ "_FPU_SINGLE", "fpu__control_8h.html#a943af67d7e5b6c2d51db2980b65f9943", null ],
    [ "_FPU_RC_NEAREST", "fpu__control_8h.html#a71ff5d059d8178e29e2e1e2ec7770e33", null ],
    [ "_FPU_RC_DOWN", "fpu__control_8h.html#a643b8fa05eee41d4d046fb98f4dfc73b", null ],
    [ "_FPU_RC_UP", "fpu__control_8h.html#a9d870f255c146209a92c974e24470b17", null ],
    [ "_FPU_RC_ZERO", "fpu__control_8h.html#a964b20abe22a84235da31494e92eaa87", null ],
    [ "_FPU_RESERVED", "fpu__control_8h.html#af0737a650f396982f25c7ea70568af05", null ],
    [ "_FPU_DEFAULT", "fpu__control_8h.html#a18efebabd40da2c99860a91c796a7c97", null ],
    [ "_FPU_IEEE", "fpu__control_8h.html#a4c546049611d96d1687b7d670e4dd7fa", null ],
    [ "_FPU_GETCW", "fpu__control_8h.html#ab8ddbdd01791d2af1bb3b735eee089ad", null ],
    [ "_FPU_SETCW", "fpu__control_8h.html#a73d3374234d8d20fc6a11009fd6b6927", null ],
    [ "__attribute__", "fpu__control_8h.html#a3a99000350ca1ad225beda92dccf7d93", null ],
    [ "__fpu_control", "fpu__control_8h.html#a157555f1ae3af1991e10900c30d391e8", null ]
];