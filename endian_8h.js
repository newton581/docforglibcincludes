var endian_8h =
[
    [ "LITTLE_ENDIAN", "endian_8h.html#a8782a401fbf55261460863fc2f8df1ce", null ],
    [ "BIG_ENDIAN", "endian_8h.html#a23eb5e058a210efdde3d64e69679fafa", null ],
    [ "PDP_ENDIAN", "endian_8h.html#a2026615c7441e0b093ff7389f46f7654", null ],
    [ "BYTE_ORDER", "endian_8h.html#a1771b7fb65ee640524d0052f229768c3", null ],
    [ "htobe16", "endian_8h.html#a4b9167199621b390f82c3e5361ba6df6", null ],
    [ "htole16", "endian_8h.html#a3ea73a6089f61223b225c46e2ba58a47", null ],
    [ "be16toh", "endian_8h.html#abb9b3120f6457dc04471c134fee1d221", null ],
    [ "le16toh", "endian_8h.html#a684a5d26d1989cbd925e97292cc81c72", null ],
    [ "htobe32", "endian_8h.html#aa65407014913309932b9a5ea3c16a86d", null ],
    [ "htole32", "endian_8h.html#a9bea1e76e277f13ae39ac86095510bfa", null ],
    [ "be32toh", "endian_8h.html#a6acac399720b3a57d9050420aa0aac41", null ],
    [ "le32toh", "endian_8h.html#ad2dfbafcefb3add65ea44e581398e90a", null ],
    [ "htobe64", "endian_8h.html#a5c81b75e6da4507e0a90b6f6169c4787", null ],
    [ "htole64", "endian_8h.html#a3070a5f8867dbc37bdafcb1a7cd491bf", null ],
    [ "be64toh", "endian_8h.html#af94eb043f5be14b4f260fb9b4c89ca2e", null ],
    [ "le64toh", "endian_8h.html#afeefd2e03c936cc08821e7fa67946c4a", null ]
];