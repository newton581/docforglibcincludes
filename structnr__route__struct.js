var structnr__route__struct =
[
    [ "type", "structnr__route__struct.html#aafdb3becca57f1e6014afe6312b4c3c0", null ],
    [ "callsign", "structnr__route__struct.html#a5d2e0f80294571e86037c18c88b9a480", null ],
    [ "device", "structnr__route__struct.html#ad1eeda44da29e201b4bc29700053b05f", null ],
    [ "quality", "structnr__route__struct.html#a2ccbad7b4c1dbfb86b053e54c6904338", null ],
    [ "mnemonic", "structnr__route__struct.html#ac4b24ab39a3c4934b3386117777cea2d", null ],
    [ "neighbour", "structnr__route__struct.html#a9480327d14661ee47b3dba8d80ba67a4", null ],
    [ "obs_count", "structnr__route__struct.html#a003e22ffd4f2324f41b3736c35aa59fb", null ],
    [ "ndigis", "structnr__route__struct.html#ac1acbc521703805e1d2f6d25bfb3ccbd", null ],
    [ "digipeaters", "structnr__route__struct.html#a9b6a463ef03bac9b507967636bb40f77", null ]
];