var structsockaddr__ipx =
[
    [ "sipx_family", "structsockaddr__ipx.html#a9bd162d29eea86723887defdc7eecad9", null ],
    [ "sipx_port", "structsockaddr__ipx.html#a708273038c13d50645a000fd56fe98b1", null ],
    [ "sipx_network", "structsockaddr__ipx.html#ae2d75b1f834a1bb5d3ba45aa764dbab7", null ],
    [ "sipx_node", "structsockaddr__ipx.html#afafedbf8eac03609d80df08df6776a4b", null ],
    [ "sipx_type", "structsockaddr__ipx.html#a60a5d323e827d8529e08d489b24a0e52", null ],
    [ "sipx_zero", "structsockaddr__ipx.html#ac613014d075e940960b1881cc0bd3ff3", null ]
];