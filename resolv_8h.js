var resolv_8h =
[
    [ "res_sym", "structres__sym.html", "structres__sym" ],
    [ "LOCALDOMAINPARTS", "resolv_8h.html#a6cf1bb4d14e610d0a7321d3fd094d952", null ],
    [ "RES_TIMEOUT", "resolv_8h.html#a79e50bfd544e5538525a60cbdd23e2f2", null ],
    [ "RES_MAXNDOTS", "resolv_8h.html#aa42bbeb96547851d571adfbeca6766bd", null ],
    [ "RES_MAXRETRANS", "resolv_8h.html#a7f7c7d7881718df69984206a4f2d8cb3", null ],
    [ "RES_MAXRETRY", "resolv_8h.html#ae6a901c1105c0ed0eb93f4c8066d6cee", null ],
    [ "RES_DFLRETRY", "resolv_8h.html#a785314ada504308b3b9c0ab11093b818", null ],
    [ "RES_MAXTIME", "resolv_8h.html#ac3a3fbc76cdb2d9d7b6e23d5a34a1643", null ],
    [ "nsaddr", "resolv_8h.html#ab5be980698d236cf7d4fc80f049c48fe", null ],
    [ "__RES", "resolv_8h.html#a204156b96db76d5da946b87df665c55e", null ],
    [ "_PATH_RESCONF", "resolv_8h.html#ae20887848cf35f8fb40f94d2391d48ff", null ],
    [ "RES_INIT", "resolv_8h.html#ad24c937ea3a9341f33147ccfad0b8922", null ],
    [ "RES_DEBUG", "resolv_8h.html#ad0ab3ad1472c85af7ae8e0be244a83cd", null ],
    [ "RES_AAONLY", "resolv_8h.html#a7f386402f3da8400c91849af57310397", null ],
    [ "RES_USEVC", "resolv_8h.html#aeab26d900c2a79ea468b6e43448de7bb", null ],
    [ "RES_PRIMARY", "resolv_8h.html#af712d2774c5aaea394790b128ece794e", null ],
    [ "RES_IGNTC", "resolv_8h.html#a0c65698f0d9316947b47f2086e890753", null ],
    [ "RES_RECURSE", "resolv_8h.html#a6f9c3050d0df866dd53667dd348769d3", null ],
    [ "RES_DEFNAMES", "resolv_8h.html#a03ca9f827460142dfb59532f9763f8a3", null ],
    [ "RES_STAYOPEN", "resolv_8h.html#ad68db4960268da0c7e80432e8219c475", null ],
    [ "RES_DNSRCH", "resolv_8h.html#a8dc72efae356f484afe906135d039ea6", null ],
    [ "RES_NOALIASES", "resolv_8h.html#a43483e68993e0dee6f7d9b8d6ee80acc", null ],
    [ "RES_ROTATE", "resolv_8h.html#a8c0eb45e004711647c98f5068aae88c7", null ],
    [ "RES_NOCHECKNAME", "resolv_8h.html#aa8c3f9a9a79ee3ff92f1246008f17310", null ],
    [ "RES_KEEPTSIG", "resolv_8h.html#a2a9d512c911367dc1712f7e222021e96", null ],
    [ "RES_BLAST", "resolv_8h.html#a401225234f75176c360122765c05f4fd", null ],
    [ "RES_USE_EDNS0", "resolv_8h.html#a99ba27c2c9c796f3a6feb18be0cc5463", null ],
    [ "RES_SNGLKUP", "resolv_8h.html#ac9cea3c1a4e1ec9905217b3d5bacfb19", null ],
    [ "RES_SNGLKUPREOP", "resolv_8h.html#a13046dc619b83b60ed55f422a3287cc6", null ],
    [ "RES_USE_DNSSEC", "resolv_8h.html#af943055ea88053fcd96369d914db2b94", null ],
    [ "RES_NOTLDQUERY", "resolv_8h.html#a1ae967a7ec4cf5b14e7193dce21538fa", null ],
    [ "RES_NORELOAD", "resolv_8h.html#aeb01a0cc84bb9bbf90af16b7956fa77d", null ],
    [ "RES_TRUSTAD", "resolv_8h.html#a5f82afa98cb3d8587b05a441158e6604", null ],
    [ "RES_DEFAULT", "resolv_8h.html#ab82eb9fd35657868415955b3ca68e967", null ],
    [ "RES_PRF_STATS", "resolv_8h.html#ad795ef8d52ebc9b25e31e7d2e659e2e5", null ],
    [ "RES_PRF_UPDATE", "resolv_8h.html#a59f6b7a9de4366e213d50e8b9d10c7e0", null ],
    [ "RES_PRF_CLASS", "resolv_8h.html#ae034cc2e0e5dc39c8aaeb5d8f6d7ed51", null ],
    [ "RES_PRF_CMD", "resolv_8h.html#a7bfaa4d1135db98eeb323767ee94b09c", null ],
    [ "RES_PRF_QUES", "resolv_8h.html#a39b26cabde0ccbbcad4dec6fa836f615", null ],
    [ "RES_PRF_ANS", "resolv_8h.html#ac888edd0d4649f96e9469a6dd37910f2", null ],
    [ "RES_PRF_AUTH", "resolv_8h.html#a2291928444840ea2d34675c6a374dcc6", null ],
    [ "RES_PRF_ADD", "resolv_8h.html#ad324fb1ed48139cd837a28587b0d8dee", null ],
    [ "RES_PRF_HEAD1", "resolv_8h.html#abdc7d44c6a842817863ca3842d573ea1", null ],
    [ "RES_PRF_HEAD2", "resolv_8h.html#a7cd25e18b6c3dbaa7133acdc32ff1ccc", null ],
    [ "RES_PRF_TTLID", "resolv_8h.html#adc981ec2f919be68eac95618ba5c5ba5", null ],
    [ "RES_PRF_HEADX", "resolv_8h.html#afb276eaf0d1fa41862522745c14e30e0", null ],
    [ "RES_PRF_QUERY", "resolv_8h.html#ad7eccf16acbea9c7b8d68415875e1e7e", null ],
    [ "RES_PRF_REPLY", "resolv_8h.html#ab2ff98ebca10a742bf1f1e5d8ae0ab64", null ],
    [ "RES_PRF_INIT", "resolv_8h.html#ae27399e7f1e4318c702ced7e0c48c913", null ],
    [ "_res", "resolv_8h.html#ab18ca25ea5500baf22b6fc41db84ad44", null ],
    [ "fp_nquery", "resolv_8h.html#a25213c2244e0fd219d03a5936c20f81c", null ],
    [ "fp_query", "resolv_8h.html#abf1318446a2690ccdb0e06037c80f490", null ],
    [ "hostalias", "resolv_8h.html#a87192d7970ab95850c746b5c4e84842e", null ],
    [ "p_query", "resolv_8h.html#a937dba43b0ec6e372505434cf0196815", null ],
    [ "res_close", "resolv_8h.html#a0d74fc3711be55ef731447ac8c8d0c41", null ],
    [ "res_init", "resolv_8h.html#abdcbe3f125b57648ac1d8ad01f106386", null ],
    [ "res_isourserver", "resolv_8h.html#ae1681f8f36ead215cc4cc52a5a3e2380", null ],
    [ "res_mkquery", "resolv_8h.html#ad970bb883ca41b7878d82a491ce112b0", null ],
    [ "res_query", "resolv_8h.html#ae1558c78777c1e8e27ae60a1d90efe4f", null ],
    [ "res_querydomain", "resolv_8h.html#a479d1ba916285ce8c2ec6c7db112a623", null ],
    [ "res_search", "resolv_8h.html#acff578d41b5be29faf9650324374254d", null ],
    [ "res_send", "resolv_8h.html#a9cafcbda09e4fc7526d0dceb097da468", null ],
    [ "b64_ntop", "resolv_8h.html#a48a3730b3eb9e9d813f825e3ceebb7f0", null ],
    [ "b64_pton", "resolv_8h.html#adb1870f1091925ccd1fbe88c22c49978", null ],
    [ "dn_comp", "resolv_8h.html#af50f95d81f4a59ded2af101d0a9b7c40", null ],
    [ "dn_count_labels", "resolv_8h.html#a43b3b73be61194f26c775e6d1647aea2", null ],
    [ "dn_expand", "resolv_8h.html#a4092f043b935d69a2d3bcf555f909ee2", null ],
    [ "dn_skipname", "resolv_8h.html#a5d3e6fcad1d39958007c132d0ea7f9c9", null ],
    [ "fp_resstat", "resolv_8h.html#a9ab9251a35958219008aedbac60d7340", null ],
    [ "loc_aton", "resolv_8h.html#ad1735ff63ba65c6aa5f210dda947e1a0", null ],
    [ "loc_ntoa", "resolv_8h.html#a4e49cb07771b6cc6ce37209da3f3c3fc", null ],
    [ "p_cdname", "resolv_8h.html#a2ba127021bef9229e6b5998a629db8fe", null ],
    [ "p_cdnname", "resolv_8h.html#ac5d1526099675d81a42ec5b2c429fce7", null ],
    [ "p_class", "resolv_8h.html#a3b86f27c5f655645c4682f8b6d4798b0", null ],
    [ "p_fqname", "resolv_8h.html#ad5bc2fb5db75d238e6577e4cdf804ded", null ],
    [ "p_fqnname", "resolv_8h.html#ad70d88030f7d680f1c2dde78f2725b9e", null ],
    [ "p_option", "resolv_8h.html#af1bae1ab6adb986773dc707f0c635e29", null ],
    [ "p_time", "resolv_8h.html#a6da3140cec2aea6f275e1c2910f175ef", null ],
    [ "p_type", "resolv_8h.html#a693619300ca548547d97008184c448a4", null ],
    [ "p_rcode", "resolv_8h.html#a587e84492695a9c12c6a745d8d29a201", null ],
    [ "putlong", "resolv_8h.html#ad6355af940cabdceffad2ef554c7f7b2", null ],
    [ "putshort", "resolv_8h.html#a5313cc1efb3594be07d7461b04036061", null ],
    [ "res_dnok", "resolv_8h.html#ad1c90b3fb3aff6e6af1e169c5dfae8e8", null ],
    [ "res_hnok", "resolv_8h.html#a245860fc68f9f97c9cdfbba1f47a3201", null ],
    [ "res_hostalias", "resolv_8h.html#af9a51b378913617074dc314c5d70be9c", null ],
    [ "res_mailok", "resolv_8h.html#a750587be10d9af144a0bf561438be9ef", null ],
    [ "res_nameinquery", "resolv_8h.html#ade792cad46a639072ed7640df3119768", null ],
    [ "res_nclose", "resolv_8h.html#abee7350c040a390312643fe4d778cb05", null ],
    [ "res_ninit", "resolv_8h.html#a2193149d4a9afd80956882e87acbd0d3", null ],
    [ "res_nmkquery", "resolv_8h.html#a88bd95059ee917d6ca1cbf5fa76d834d", null ],
    [ "res_nquery", "resolv_8h.html#ad1e4813e34dcedcdc475013ce0b50a42", null ],
    [ "res_nquerydomain", "resolv_8h.html#ab409c3f6e65eeb360030f465ca6a1e5a", null ],
    [ "res_nsearch", "resolv_8h.html#adbf0d347fa517bd61ac12b29a995202f", null ],
    [ "res_nsend", "resolv_8h.html#a09834d4b7b2462b8555d04eba64e2571", null ],
    [ "res_ownok", "resolv_8h.html#a412436139cf44a10f531da981f2c002c", null ],
    [ "res_queriesmatch", "resolv_8h.html#ad72de4fa1b398164c0a29f192a34e329", null ],
    [ "res_randomid", "resolv_8h.html#a6a656ddd4876bdc449dff3a7d3fbf077", null ],
    [ "sym_ntop", "resolv_8h.html#ab57121b3f96da3714c9e4a5a62e89ab8", null ],
    [ "sym_ntos", "resolv_8h.html#a8521d6464dd760c3a1c1e35cdd42018e", null ],
    [ "sym_ston", "resolv_8h.html#a8c901627450257d10ceb8f0b7633b28a", null ],
    [ "__res_state", "resolv_8h.html#a483799611ea194b5c8275ee7863ee3f6", null ],
    [ "fp_nquery", "resolv_8h.html#ad0af54a21260508f4c7fc01689195fa2", null ],
    [ "fp_query", "resolv_8h.html#ab65fa8005561bacd9df5201a963a5217", null ],
    [ "hostalias", "resolv_8h.html#a394753af632efe8625521666ba5a93b8", null ],
    [ "p_query", "resolv_8h.html#a71de11c8bb4424f12c2adefcc9e72a03", null ],
    [ "res_isourserver", "resolv_8h.html#af1a8073f6636ad0f1fc0f71d727fa93c", null ],
    [ "res_mkquery", "resolv_8h.html#abbcd43e3d9be55c3c5cca506621270ec", null ],
    [ "res_query", "resolv_8h.html#ab25dce349bcd1780683c12a3f8fed93d", null ],
    [ "res_querydomain", "resolv_8h.html#a0f11f6281f4884c065517ef5871233eb", null ],
    [ "res_search", "resolv_8h.html#a9f8a563c8b88b17801fb0ef66da04e42", null ],
    [ "res_send", "resolv_8h.html#a095d68917168bd755290c9ee5afedcac", null ],
    [ "res_hnok", "resolv_8h.html#a245194511e46aa4ad9dc9889c49f4ec4", null ],
    [ "res_ownok", "resolv_8h.html#ab7fa14be705e9b87625b55704b236bf3", null ],
    [ "res_mailok", "resolv_8h.html#a1985c5fbf2137e4f9ca76a43fe370f48", null ],
    [ "res_dnok", "resolv_8h.html#a15f3aeb1ccc28d2ddc53c50edc0ed5c7", null ],
    [ "sym_ston", "resolv_8h.html#a66941547e4ee5e8dc2a1f9b2f864511d", null ],
    [ "sym_ntos", "resolv_8h.html#a98eabdafd752e843931553d8ea8c08e9", null ],
    [ "sym_ntop", "resolv_8h.html#a4a8a0f1dd0cc09875ee3f32fba0c50c6", null ],
    [ "b64_ntop", "resolv_8h.html#a7b8a2c5aceed4809692fbb5cc7d393f6", null ],
    [ "b64_pton", "resolv_8h.html#a65a70cbc847f05a14e695080fbbec64c", null ],
    [ "loc_aton", "resolv_8h.html#a95f8dda3e91567f375d3fcd8fd9a2359", null ],
    [ "loc_ntoa", "resolv_8h.html#acdb07cc3dc0c475da84ba8182a7d85e9", null ],
    [ "dn_skipname", "resolv_8h.html#a63a465bf01e0d4277ba266291abb49c8", null ],
    [ "putlong", "resolv_8h.html#a79e678cf1c0b433a6f40748938def6c9", null ],
    [ "putshort", "resolv_8h.html#adbbc445d7015707c8b0f63210667c53a", null ],
    [ "p_class", "resolv_8h.html#ade62d3a1c04fe724c9673dc21ac4cad2", null ],
    [ "p_time", "resolv_8h.html#a6c8f2b7a634c7f01ed8fcfb461d62592", null ],
    [ "p_type", "resolv_8h.html#aeb00ac046d047d3bbda3b1ff36c304e7", null ],
    [ "p_rcode", "resolv_8h.html#a53755fbea3a84f0068b74f0cb8e97c6f", null ],
    [ "p_cdnname", "resolv_8h.html#a5d1b2fc08b59cf811e482f98380df9ba", null ],
    [ "p_cdname", "resolv_8h.html#a7796cc255c4e94e835c4a3eb27bc6411", null ],
    [ "p_fqnname", "resolv_8h.html#a847979adc28aca268b22d34c1b83d18e", null ],
    [ "p_fqname", "resolv_8h.html#a41198bfd5f0405c45403817e532a29d0", null ],
    [ "p_option", "resolv_8h.html#a165f298c89db4de5602602a16f1def93", null ],
    [ "dn_count_labels", "resolv_8h.html#ae1ff7032422d8b7046c6b3a5a0ab0b5d", null ],
    [ "dn_comp", "resolv_8h.html#a013d6e3c74d1a86c3d5c62c63ab5cc09", null ],
    [ "dn_expand", "resolv_8h.html#afa940d98525f5b5438cb537bb7e4abc1", null ],
    [ "res_nameinquery", "resolv_8h.html#a12b1e8ed8003f207ea176f49ddf0e6c6", null ],
    [ "res_queriesmatch", "resolv_8h.html#a8a49f0763899bae68af500a819728fbe", null ],
    [ "res_ninit", "resolv_8h.html#ab5e43eed58f729892220cd8639127c31", null ],
    [ "fp_resstat", "resolv_8h.html#ad17d03bb319455897e74ad41f4a3104f", null ],
    [ "res_hostalias", "resolv_8h.html#a59995fcbd6c9d5fe2f37aea5aefaffd7", null ],
    [ "res_nquery", "resolv_8h.html#a6f3d47eab55fe0b454944b499e1734c3", null ],
    [ "res_nsearch", "resolv_8h.html#aa381dd3094ce7ceb3227969cbbdf2600", null ],
    [ "res_nquerydomain", "resolv_8h.html#abfd2cb54a803a75db0910f00bc052f31", null ],
    [ "res_nmkquery", "resolv_8h.html#a5fb4d302af63f93d08742544ad2dc9b7", null ],
    [ "res_nsend", "resolv_8h.html#a0fc4537ed2f3877d747cbe1827cb9066", null ],
    [ "res_nclose", "resolv_8h.html#a7adb39d8dccf87548a258bf68f2e4583", null ]
];