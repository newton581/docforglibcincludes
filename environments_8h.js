var environments_8h =
[
    [ "_POSIX_V7_ILP32_OFFBIG", "environments_8h.html#a5bd133671f26a4a00f65d2b3a7316e67", null ],
    [ "_POSIX_V6_ILP32_OFFBIG", "environments_8h.html#ade6a83bcbbb94927832a179644fce01a", null ],
    [ "_XBS5_ILP32_OFFBIG", "environments_8h.html#a35059007d1485e9ea5c1c814c1d922e9", null ],
    [ "_POSIX_V7_ILP32_OFF32", "environments_8h.html#a0ee805f721b5aa53f4a5fe2e8a4ed3f1", null ],
    [ "_POSIX_V6_ILP32_OFF32", "environments_8h.html#a7f0049819d7a90d740591016e746acf0", null ],
    [ "_XBS5_ILP32_OFF32", "environments_8h.html#aa7101d1629aa41bf3d53c83110cb35d9", null ],
    [ "__ILP32_OFF32_CFLAGS", "environments_8h.html#ae154e0704458fb0fedd3f2f898dc6ecc", null ],
    [ "__ILP32_OFF32_LDFLAGS", "environments_8h.html#a524e808d3b31bccbf1b7488f84d6d919", null ],
    [ "__ILP32_OFFBIG_CFLAGS", "environments_8h.html#a1a13328e51f7482a9c684935bb5372e3", null ],
    [ "__ILP32_OFFBIG_LDFLAGS", "environments_8h.html#a94824583d0d1709c9ad9a6ba2835d210", null ],
    [ "__LP64_OFF64_CFLAGS", "environments_8h.html#a12212b886e0b1e3bd72d10b363bf0622", null ],
    [ "__LP64_OFF64_LDFLAGS", "environments_8h.html#a968f8f22fd69a78a2822f5ad008ca19b", null ]
];