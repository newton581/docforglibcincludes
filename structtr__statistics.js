var structtr__statistics =
[
    [ "rx_packets", "structtr__statistics.html#a3e4eaf826c2555b842ff2c7c21e8e395", null ],
    [ "tx_packets", "structtr__statistics.html#ad580795d742a29239f50126f3e0475c8", null ],
    [ "rx_bytes", "structtr__statistics.html#a0f44a072b340286fcd522f81af12e786", null ],
    [ "tx_bytes", "structtr__statistics.html#a7ef9f77e16f22f273590a646a7ca048d", null ],
    [ "rx_errors", "structtr__statistics.html#ab6f80fb814f389c85131d59921e63f13", null ],
    [ "tx_errors", "structtr__statistics.html#a66772e78ad7eca243f2c188c4cdf7ee4", null ],
    [ "rx_dropped", "structtr__statistics.html#a03c166f78d8e7f992777360df5fdd53e", null ],
    [ "tx_dropped", "structtr__statistics.html#a8e3c0e0be374f1ee2121d19df1015090", null ],
    [ "multicast", "structtr__statistics.html#a90b68ba6b287166c20b185dfdf5dd593", null ],
    [ "transmit_collision", "structtr__statistics.html#a36b23a26b05c7a23d6be67dc1312534f", null ],
    [ "line_errors", "structtr__statistics.html#ae0c14da7cf39e5c8b01d12c3f5413279", null ],
    [ "internal_errors", "structtr__statistics.html#a148d648c7f2c1934937508e15bcb3b7c", null ],
    [ "burst_errors", "structtr__statistics.html#ac4f4fae745f70a83203ad0844376d7f5", null ],
    [ "A_C_errors", "structtr__statistics.html#af2a4ef3fc6ded745189976133a2508a1", null ],
    [ "abort_delimiters", "structtr__statistics.html#ad737765573a5848ddc984a4e4bf57b01", null ],
    [ "lost_frames", "structtr__statistics.html#a1635c05a4e45db588de60b008000ed80", null ],
    [ "recv_congest_count", "structtr__statistics.html#a903ff1c3daea3d77f641e344b9996c25", null ],
    [ "frame_copied_errors", "structtr__statistics.html#ad3baab77a82f7d67252f1a962d183dbd", null ],
    [ "frequency_errors", "structtr__statistics.html#af7751e3e95798f5cc97c2c340a722980", null ],
    [ "token_errors", "structtr__statistics.html#a5e9954f58836721a66524f696829a4d5", null ],
    [ "dummy1", "structtr__statistics.html#ae3492c4e9e66530f6434ca6c376e131e", null ]
];