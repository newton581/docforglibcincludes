var structuser =
[
    [ "regs", "structuser.html#a2786bc9b116efe3ebe5e96002739437c", null ],
    [ "u_fpvalid", "structuser.html#a7d5b413d5437521ebbfe699871539f26", null ],
    [ "i387", "structuser.html#a298c58e3fc951fb05b6fb6f697b8631a", null ],
    [ "u_tsize", "structuser.html#a809cd8edfe3e58e292ca5129988874e2", null ],
    [ "u_dsize", "structuser.html#ad5c475b8f62bef6c94b4e5476d7e5197", null ],
    [ "u_ssize", "structuser.html#acec3ff83bd8496df2e7ac16bd75ff33e", null ],
    [ "start_code", "structuser.html#a0d59fa3221d0eba4960dc2c35320701e", null ],
    [ "start_stack", "structuser.html#a9fd37c927acda3b7e34b832d8b16a3a0", null ],
    [ "signal", "structuser.html#a609e7da7928ecf8855a1918d071f8843", null ],
    [ "reserved", "structuser.html#a5df052951e5fb6e0ac39291f8965376f", null ],
    [ "u_ar0", "structuser.html#a7bdbcdaa7be4c6b58f7577d7f8622212", null ],
    [ "u_fpstate", "structuser.html#acb6000c688218fed968a1c81825f36e9", null ],
    [ "magic", "structuser.html#a9e312562a992090ee10b37b3a613f125", null ],
    [ "u_comm", "structuser.html#a99d8d93c06059c90aa68d2b59b22009e", null ],
    [ "u_debugreg", "structuser.html#a846cfb333e52875dd1716e46923e500a", null ]
];