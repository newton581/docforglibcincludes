var structsg__header =
[
    [ "pack_len", "structsg__header.html#a05140de7d6359416f365c730366f3549", null ],
    [ "reply_len", "structsg__header.html#a098120e287940096456063aa9b73e2c4", null ],
    [ "pack_id", "structsg__header.html#aed31d1ee18bf8960d9066aba43497142", null ],
    [ "result", "structsg__header.html#aecb04d1ea04446f6474cb55e186addbc", null ],
    [ "twelve_byte", "structsg__header.html#a6d4d04f34db88284217145606c54f73f", null ],
    [ "target_status", "structsg__header.html#a29e2980aff1cd9092bdaaef2e890bd40", null ],
    [ "host_status", "structsg__header.html#a5fd97924e9cf0edce95393eedf98b6a6", null ],
    [ "driver_status", "structsg__header.html#af725369971df82cfb0e01e6e3eb049f2", null ],
    [ "other_flags", "structsg__header.html#a0e9e02fff389518c3562c084f55536e5", null ],
    [ "sense_buffer", "structsg__header.html#a616a948dfedb50b917b894090a1e180c", null ]
];