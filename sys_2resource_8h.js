var sys_2resource_8h =
[
    [ "__id_t_defined", "sys_2resource_8h.html#ac070f7819b11e8440996e8ba33766b55", null ],
    [ "id_t", "sys_2resource_8h.html#a7f13a96f05e6a74e9b416154cbe5e2e2", null ],
    [ "__rusage_who_t", "sys_2resource_8h.html#a84c3d6fb1803bf40e69920543ee918e0", null ],
    [ "__priority_which_t", "sys_2resource_8h.html#a9ae4f19caa64bd259736f66ab5364984", null ],
    [ "getrlimit", "sys_2resource_8h.html#a012658e8edb4ad647a34fe5f49645f5c", null ],
    [ "setrlimit", "sys_2resource_8h.html#a0d7ff3d562330e012ebc5b0f0d04939f", null ],
    [ "getrusage", "sys_2resource_8h.html#a7ad553f7a0f26c065a42aaa39a30db7b", null ],
    [ "getpriority", "sys_2resource_8h.html#ade1dbade921e1b91bebfc4b9eb7f7f83", null ],
    [ "setpriority", "sys_2resource_8h.html#ae77bbfb6b61ff2f52e406c21ac1f30ae", null ],
    [ "__rlimit_resource_t", "sys_2resource_8h.html#a318a9e59553c7de77b6b50a713d2b1fa", null ]
];