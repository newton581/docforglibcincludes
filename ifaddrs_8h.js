var ifaddrs_8h =
[
    [ "ifaddrs", "structifaddrs.html", "structifaddrs" ],
    [ "ifa_broadaddr", "ifaddrs_8h.html#aae6c327a8fb5b5c77da3906b1b6386f6", null ],
    [ "ifa_dstaddr", "ifaddrs_8h.html#a0f11dd1ecdd08a6926c23817bac920f8", null ],
    [ "getifaddrs", "ifaddrs_8h.html#a89a08acf3c93ed67cf005a133034db6a", null ],
    [ "freeifaddrs", "ifaddrs_8h.html#a88718bc37c52ab04125201e86c41df2b", null ]
];