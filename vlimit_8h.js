var vlimit_8h =
[
    [ "INFINITY", "vlimit_8h.html#a956e2723d559858d08644ac99146e910", null ],
    [ "__vlimit_resource", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033a", [
      [ "LIM_NORAISE", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aa45378a1107b9cc41a1a0209dfcb630f7", null ],
      [ "LIM_CPU", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aa08ffe1513bed314684dff22485f829cb", null ],
      [ "LIM_FSIZE", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aaae97ecc9cb12407f0bd548b64df2e557", null ],
      [ "LIM_DATA", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aabbf893f7495386351135181bd3f8cc66", null ],
      [ "LIM_STACK", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aa619fd367d8ad279841835e2f55cba3a1", null ],
      [ "LIM_CORE", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aa41c7b661813a77449afa2e470aef3fea", null ],
      [ "LIM_MAXRSS", "vlimit_8h.html#acdcb478d74f16df0c8b1bc4c4562033aa8a9119d9532fc6aeb74afc889970c3e2", null ]
    ] ],
    [ "vlimit", "vlimit_8h.html#adce3645a71982c64f2c6a7a5a653c70f", null ]
];