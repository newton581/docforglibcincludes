var termios_8h =
[
    [ "__pid_t_defined", "termios_8h.html#a78397b687c0ddca84c27c22f8827a7a1", null ],
    [ "CCEQ", "termios_8h.html#ac426e9a8ac5f07f9dff7ed09cdca88e8", null ],
    [ "pid_t", "termios_8h.html#a63bef27b1fbd4a91faa273a387a38dc2", null ],
    [ "cfgetospeed", "termios_8h.html#a8f663b978716fa3543dce5a6de1a0e23", null ],
    [ "cfgetispeed", "termios_8h.html#aa803e9b7b20cca1aa310b3b640c64f73", null ],
    [ "cfsetospeed", "termios_8h.html#a9075cc5839c3dd1ce2ed9ffb53c8ebf9", null ],
    [ "cfsetispeed", "termios_8h.html#a4482088e914963539eed70bb6914f59b", null ],
    [ "cfsetspeed", "termios_8h.html#aa2f4bb6adbc37dc77ee9ffaa80e08ed9", null ],
    [ "tcgetattr", "termios_8h.html#a2c8128decfbb6a6ff722a3996697f55a", null ],
    [ "tcsetattr", "termios_8h.html#a7521aef804a763077fbe45741bec06d5", null ],
    [ "cfmakeraw", "termios_8h.html#aec80a2127ecdf7d6aa7e12c95e5e8048", null ],
    [ "tcsendbreak", "termios_8h.html#ad8362bd30cc44c65bccbbeaebbbb1f73", null ],
    [ "tcdrain", "termios_8h.html#a3600a55cba2a31a6e73aa77721331aa2", null ],
    [ "tcflush", "termios_8h.html#af5aa20c43de11f51012c3bf812b4c078", null ],
    [ "tcflow", "termios_8h.html#ab97867651bf4b89aa90863a010944258", null ],
    [ "tcgetsid", "termios_8h.html#a5af99ba9c7ba26dab6e557ba72994c64", null ]
];