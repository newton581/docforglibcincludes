var structspwd =
[
    [ "sp_namp", "structspwd.html#a9d2d6c0ad22d39e62f6807f3ea58a96c", null ],
    [ "sp_pwdp", "structspwd.html#a1ec6527342c4cb4756636e017df2266e", null ],
    [ "sp_lstchg", "structspwd.html#aa76ce8cd86329ee7c138493b54f5352c", null ],
    [ "sp_min", "structspwd.html#a0b37d4ae5ebea01884bdfe3ef05d541d", null ],
    [ "sp_max", "structspwd.html#afea5011d44f4e8429df91fefd19aa22d", null ],
    [ "sp_warn", "structspwd.html#a41e6af2dd337a9e81978ef84999d575d", null ],
    [ "sp_inact", "structspwd.html#ade4f8f6fe4a884a1fa8980a3e3a44abf", null ],
    [ "sp_expire", "structspwd.html#a37ba165cff3b403c6fe918d0a276604e", null ],
    [ "sp_flag", "structspwd.html#a886a2fd5cb6aadc4d6e3ab5d821ed777", null ]
];