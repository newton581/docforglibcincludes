var structvtimes =
[
    [ "vm_utime", "structvtimes.html#a5048f1c458d4004fb21b93bda3e7b30d", null ],
    [ "vm_stime", "structvtimes.html#acfa2e02ef4dfd2a150a30c41d9499400", null ],
    [ "vm_idsrss", "structvtimes.html#a516c4aaf944d3cfdeb3ebfd35bbeed94", null ],
    [ "vm_ixrss", "structvtimes.html#a74c73ed14e2fc1a30b1d7cb32b8588c7", null ],
    [ "vm_maxrss", "structvtimes.html#a3a2b43747099c286f3c7c146861839ab", null ],
    [ "vm_majflt", "structvtimes.html#ad3028509e4954f62c9bb2d2a80c80173", null ],
    [ "vm_minflt", "structvtimes.html#adfdc2cf15fa060cc66580cd05ba44f38", null ],
    [ "vm_nswap", "structvtimes.html#a3055788f82151bebf3b29e600599aeaa", null ],
    [ "vm_inblk", "structvtimes.html#a9713e231c60f0a761dbde50cc7b28017", null ],
    [ "vm_oublk", "structvtimes.html#a7f639d34d68612c7aec3c000dd84f4a4", null ]
];