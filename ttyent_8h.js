var ttyent_8h =
[
    [ "ttyent", "structttyent.html", "structttyent" ],
    [ "_PATH_TTYS", "ttyent_8h.html#a69683663dc1807a06f406d897b22c5c5", null ],
    [ "_TTYS_OFF", "ttyent_8h.html#aa4ab065f9d1b6a107cd48b562587f6d3", null ],
    [ "_TTYS_ON", "ttyent_8h.html#aab5821ae639ffdec1ba8c2a31a45132b", null ],
    [ "_TTYS_SECURE", "ttyent_8h.html#a23a997bb5a1bb668069fbc508219c354", null ],
    [ "_TTYS_WINDOW", "ttyent_8h.html#af8456c1b593b29f80627d908aa971d2e", null ],
    [ "TTY_ON", "ttyent_8h.html#a7cfa36c34a3f7121247e98bdadd2a90b", null ],
    [ "TTY_SECURE", "ttyent_8h.html#ab07014065aec957b403baceadc04174d", null ],
    [ "getttyent", "ttyent_8h.html#a3490fff1c95206eccfea8594ca582b89", null ],
    [ "getttynam", "ttyent_8h.html#afbdcc1e30198889e15309331fe82c399", null ],
    [ "setttyent", "ttyent_8h.html#ada94a8e66c127154e7c6d827af948c7a", null ],
    [ "endttyent", "ttyent_8h.html#a90ca1e3825fefbcf318ae765427db5d9", null ]
];