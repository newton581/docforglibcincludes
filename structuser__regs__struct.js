var structuser__regs__struct =
[
    [ "ebx", "structuser__regs__struct.html#a469cfb2eab448f908f115beb67daed8c", null ],
    [ "ecx", "structuser__regs__struct.html#afa5a6feca7f353413d697d5c0ba3ba00", null ],
    [ "edx", "structuser__regs__struct.html#a8846d5d34b02abf0552c661492d64e12", null ],
    [ "esi", "structuser__regs__struct.html#a1581ef400fe5d5024177e463cde20c0c", null ],
    [ "edi", "structuser__regs__struct.html#ab580b0d40b94366834b09d2de0665458", null ],
    [ "ebp", "structuser__regs__struct.html#aa89ed2a145e69fc96f123b1c000a4483", null ],
    [ "eax", "structuser__regs__struct.html#a785ab321ed250eea8b7ef554a4a345e8", null ],
    [ "xds", "structuser__regs__struct.html#a9f3fd9756e832ccd84b329d07d2ade5a", null ],
    [ "xes", "structuser__regs__struct.html#ab003433e7ae1e55237d50d267af2d0e6", null ],
    [ "xfs", "structuser__regs__struct.html#a68fd1ceac7433171132218e9dd1ff744", null ],
    [ "xgs", "structuser__regs__struct.html#a699a250fdc5eca5277191d1d617eda7c", null ],
    [ "orig_eax", "structuser__regs__struct.html#a475eff34457beab167285687c41b62c3", null ],
    [ "eip", "structuser__regs__struct.html#a8e04776632313b5f7bfe93e2b1227a3c", null ],
    [ "xcs", "structuser__regs__struct.html#a4cb2e7f547c6a6e4f8cd52dd21d9f3f3", null ],
    [ "eflags", "structuser__regs__struct.html#ab849d0ca8e348c83831062586fbeabc2", null ],
    [ "esp", "structuser__regs__struct.html#ae0c331ba5529a9decb472f6ec6b97c62", null ],
    [ "xss", "structuser__regs__struct.html#a99ef6b2b20cd6fb75c931f1d49992e3a", null ]
];