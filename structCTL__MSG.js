var structCTL__MSG =
[
    [ "vers", "structCTL__MSG.html#a04a907f5a06a853c57e46e27b788b59d", null ],
    [ "type", "structCTL__MSG.html#ae3ea8a871fb907414f3c8069802598ff", null ],
    [ "answer", "structCTL__MSG.html#ab9d59581d918528c136bfd95f4f4c107", null ],
    [ "pad", "structCTL__MSG.html#a2ba7edf64d2a305eb0c2eab660f9b1fc", null ],
    [ "id_num", "structCTL__MSG.html#a4f180651a41a1ad57f2f561353bb0dc1", null ],
    [ "addr", "structCTL__MSG.html#a2eda25c88aba67f8b7c4297f097aa105", null ],
    [ "ctl_addr", "structCTL__MSG.html#a64eba7177a70b2aa332c5cb08e4f61c5", null ],
    [ "pid", "structCTL__MSG.html#a1d83d8eee11ba1c56aa0bbef9b5af7a3", null ],
    [ "l_name", "structCTL__MSG.html#a6c7bd5886ad92122a1ef81ac122fadea", null ],
    [ "r_name", "structCTL__MSG.html#a9125c87b547d00e5081129b9d57769ea", null ],
    [ "r_tty", "structCTL__MSG.html#a216d3b0bfc16ad81e74f35aeee7ea9ef", null ]
];