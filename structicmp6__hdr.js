var structicmp6__hdr =
[
    [ "icmp6_type", "structicmp6__hdr.html#a61a949afe549d40f4ca7c12c049930c2", null ],
    [ "icmp6_code", "structicmp6__hdr.html#a2ee6790ae2931d39921346200bb6676d", null ],
    [ "icmp6_cksum", "structicmp6__hdr.html#a111d9a80ef510d1d865320c229539ffb", null ],
    [ "icmp6_un_data32", "structicmp6__hdr.html#a0df68af5787320467dc8f9874582b3ed", null ],
    [ "icmp6_un_data16", "structicmp6__hdr.html#a8576605aae69bef7f0aa8fa7c921c0fc", null ],
    [ "icmp6_un_data8", "structicmp6__hdr.html#a20e62a28b56de2f001a6bcfbf8b9e95d", null ],
    [ "icmp6_dataun", "structicmp6__hdr.html#afbaa1c1ceaa75bcf09294111ff50e227", null ]
];