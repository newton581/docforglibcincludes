var xopen__lim_8h =
[
    [ "_XOPEN_IOV_MAX", "xopen__lim_8h.html#a3617d2e6501be295f1c9c33d50da8df4", null ],
    [ "IOV_MAX", "xopen__lim_8h.html#a25080e819a36fcf9aede01a6e7298ea4", null ],
    [ "NL_ARGMAX", "xopen__lim_8h.html#a223358278e2720315ec60584c1355f55", null ],
    [ "NL_LANGMAX", "xopen__lim_8h.html#a82c429c39d68f08d56e4bfdb050fe54a", null ],
    [ "NL_MSGMAX", "xopen__lim_8h.html#a7e7a512ab4395a4bb0be4ff872764e45", null ],
    [ "NL_NMAX", "xopen__lim_8h.html#a35868d7ecc36eccfdeb1c26eb0e9f3e6", null ],
    [ "NL_SETMAX", "xopen__lim_8h.html#a5dd5a06bde8cda4edb4dfda129375a1a", null ],
    [ "NL_TEXTMAX", "xopen__lim_8h.html#a934658c7b37ed12672f76ec304a5b460", null ],
    [ "NZERO", "xopen__lim_8h.html#a3b04cc1d4ce6930b578eede1f1c6ebfc", null ],
    [ "WORD_BIT", "xopen__lim_8h.html#af95e2cdeed2bb68c59e2c3d07b6c3d04", null ],
    [ "LONG_BIT", "xopen__lim_8h.html#a88c78a5170af546a3417d28875fd3710", null ]
];