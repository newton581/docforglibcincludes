var signum_generic_8h =
[
    [ "SIG_ERR", "signum-generic_8h.html#a3c330fbddd84bf34e65af370b11998d6", null ],
    [ "SIG_DFL", "signum-generic_8h.html#a27d5dc561093d6243542e6a2465bc0f8", null ],
    [ "SIG_IGN", "signum-generic_8h.html#acf0e499b0ac946b366b4f47a3b3e8b9e", null ],
    [ "SIGINT", "signum-generic_8h.html#a487309e3e9e0527535644115204a639a", null ],
    [ "SIGILL", "signum-generic_8h.html#a4c9c5284f8c8d146bd7a93d25017fc62", null ],
    [ "SIGABRT", "signum-generic_8h.html#aa86c630133e5b5174ac970227b273446", null ],
    [ "SIGFPE", "signum-generic_8h.html#a7fbba29aec3e4a8daae12935299df92d", null ],
    [ "SIGSEGV", "signum-generic_8h.html#ae20b4f7171a09516ea73c9d2745bd596", null ],
    [ "SIGTERM", "signum-generic_8h.html#a682182a5e5f38fd61f4311501e9dac5d", null ],
    [ "SIGHUP", "signum-generic_8h.html#a136c64ec2d1306de745e39d6aee362ca", null ],
    [ "SIGQUIT", "signum-generic_8h.html#a62045b465be11891160d53c10861b16c", null ],
    [ "SIGTRAP", "signum-generic_8h.html#aa2beb906ab1b46676e63823f4e773c38", null ],
    [ "SIGKILL", "signum-generic_8h.html#addd8dcd406ce514ab3b4f576a5343d42", null ],
    [ "SIGPIPE", "signum-generic_8h.html#a57e9c8c5fa13bf86bc779a9f6f408b7c", null ],
    [ "SIGALRM", "signum-generic_8h.html#aa6946723c6b7a86ec3c33caaf832840b", null ],
    [ "SIGIO", "signum-generic_8h.html#a929c5eace94ce2e099c6fa732450ce86", null ],
    [ "SIGIOT", "signum-generic_8h.html#ab12271d5e5911f8a85dc7c3f0e2364c7", null ],
    [ "SIGCLD", "signum-generic_8h.html#a3d4973bb9e3b358d11a7592c127b7ec3", null ],
    [ "_NSIG", "signum-generic_8h.html#a275020780cb70a8b0cba8db5accc5d19", null ]
];