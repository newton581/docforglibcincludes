var if__shaper_8h =
[
    [ "shaperconf", "structshaperconf.html", "structshaperconf" ],
    [ "SHAPER_QLEN", "if__shaper_8h.html#ad1a837ca843bb75505802bfe8cd8778e", null ],
    [ "SHAPER_LATENCY", "if__shaper_8h.html#a578bb19fd813d21c4ba262d65b6738d3", null ],
    [ "SHAPER_MAXSLIP", "if__shaper_8h.html#aeaa86cff20ff1749c912dad48b966ac0", null ],
    [ "SHAPER_BURST", "if__shaper_8h.html#a11f6e6b611950f51d4fac0e36c541539", null ],
    [ "SHAPER_SET_DEV", "if__shaper_8h.html#a07927c58ba4c186e45d5d02d08ac9414", null ],
    [ "SHAPER_SET_SPEED", "if__shaper_8h.html#ad4a4766a299568ee94885a3c51085235", null ],
    [ "SHAPER_GET_DEV", "if__shaper_8h.html#a1f8c84b634b18b8a3c8c75ccfb030716", null ],
    [ "SHAPER_GET_SPEED", "if__shaper_8h.html#a9ecee6346c34cf1c008ec126b7283c31", null ],
    [ "ss_speed", "if__shaper_8h.html#a8646bea25e1908d13f156b7a48675c9c", null ],
    [ "ss_name", "if__shaper_8h.html#ad02fb69e6c1de50b34144ed14d5b78d0", null ]
];