var reboot_8h =
[
    [ "RB_AUTOBOOT", "reboot_8h.html#a1959ea6ff411c3473e1849a7ca56df8f", null ],
    [ "RB_HALT_SYSTEM", "reboot_8h.html#acb43d11e61f71ee2e296019ec82537eb", null ],
    [ "RB_ENABLE_CAD", "reboot_8h.html#a78e9b1a88a3c13554cda964e81b5ba01", null ],
    [ "RB_DISABLE_CAD", "reboot_8h.html#ac1acaa71d70eb04d5e4372b8374e7caa", null ],
    [ "RB_POWER_OFF", "reboot_8h.html#ac0875ed5abfb0f3b3df3a2c80da5c425", null ],
    [ "RB_SW_SUSPEND", "reboot_8h.html#a62494b188e2bb021b9981070a0f5049a", null ],
    [ "RB_KEXEC", "reboot_8h.html#a2a954c65a942758c24ffb66998710302", null ],
    [ "reboot", "reboot_8h.html#a0c9c4d1de58e642335960d5447024162", null ]
];