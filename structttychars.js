var structttychars =
[
    [ "tc_erase", "structttychars.html#a201f1ff8ca25abe5edc57b348dc80fb7", null ],
    [ "tc_kill", "structttychars.html#a69943e60556aa9d590dd4e2a5c5cd4f8", null ],
    [ "tc_intrc", "structttychars.html#afd83da2b9ad09dec3ffa67570898213a", null ],
    [ "tc_quitc", "structttychars.html#a9586b6f3a202bf2470879a8fd5ddc5f4", null ],
    [ "tc_startc", "structttychars.html#a9c37c21b8342b129328bd4d60a838e59", null ],
    [ "tc_stopc", "structttychars.html#a39998777aa0eef1ab19382b4df31542a", null ],
    [ "tc_eofc", "structttychars.html#a5460d8448bc0099485e559fffa40e450", null ],
    [ "tc_brkc", "structttychars.html#aa51765aaa3e7932f62cbe51f25f8698a", null ],
    [ "tc_suspc", "structttychars.html#a2a7ff8075673d423558a7765cab9ff63", null ],
    [ "tc_dsuspc", "structttychars.html#a33bcc508c0172dd04d45c6d6d70e1260", null ],
    [ "tc_rprntc", "structttychars.html#a429b6d470ef8fe1b3303a8ea71eeb342", null ],
    [ "tc_flushc", "structttychars.html#a2556efe25f41d7ff93256337b638809e", null ],
    [ "tc_werasc", "structttychars.html#a35abe53fc5d9a3d06ed7af1657b70dbd", null ],
    [ "tc_lnextc", "structttychars.html#a551cb1ff9b17c8e76111b67b9d274404", null ]
];