var sys_2epoll_8h =
[
    [ "epoll_data", "unionepoll__data.html", "unionepoll__data" ],
    [ "epoll_event", "structepoll__event.html", "structepoll__event" ],
    [ "EPOLLIN", "sys_2epoll_8h.html#a9473bb25876648bb51926e6723802982", null ],
    [ "EPOLLPRI", "sys_2epoll_8h.html#af76a4ccb6d42cfdf3ff7d5ac5a971834", null ],
    [ "EPOLLOUT", "sys_2epoll_8h.html#a737460cee0c76ac8d04e9726ec200d6f", null ],
    [ "EPOLLRDNORM", "sys_2epoll_8h.html#ace47c8a3d068e427a825773a3043bf16", null ],
    [ "EPOLLRDBAND", "sys_2epoll_8h.html#af3ca771c01458a76902b34f98fa4ea78", null ],
    [ "EPOLLWRNORM", "sys_2epoll_8h.html#ae7b18b9b50e9130909fde80c6c832ff9", null ],
    [ "EPOLLWRBAND", "sys_2epoll_8h.html#a2d145da45c73675533044344a42a93e8", null ],
    [ "EPOLLMSG", "sys_2epoll_8h.html#abd58cfbc4c4aeff6dff5c8a096afd287", null ],
    [ "EPOLLERR", "sys_2epoll_8h.html#a2b69cc87f3556dc5d41b9b1122a4413e", null ],
    [ "EPOLLHUP", "sys_2epoll_8h.html#ae6832a9da6c1bc16f40c4a5b15dde53a", null ],
    [ "EPOLLRDHUP", "sys_2epoll_8h.html#afb6a93d0058c1e2bef8cff5ba1c28560", null ],
    [ "EPOLLEXCLUSIVE", "sys_2epoll_8h.html#a01d0e5ffa077246fcc15159ac2247dfa", null ],
    [ "EPOLLWAKEUP", "sys_2epoll_8h.html#af84b6a6f3ca2fecb9608dcfb17961df1", null ],
    [ "EPOLLONESHOT", "sys_2epoll_8h.html#a12f33e7f9d329aeaf78678b56fa38d1c", null ],
    [ "EPOLLET", "sys_2epoll_8h.html#ada664c5f9d581e10b4f72d1bb444dbb4", null ],
    [ "EPOLL_CTL_ADD", "sys_2epoll_8h.html#a2d36c1c8457d15bf764d4a94b4077b38", null ],
    [ "EPOLL_CTL_DEL", "sys_2epoll_8h.html#af8de2279a945391e86525bcb904bc092", null ],
    [ "EPOLL_CTL_MOD", "sys_2epoll_8h.html#a8818bbe0441220b091976d944a2b1620", null ],
    [ "epoll_data_t", "sys_2epoll_8h.html#ae448b6f1679acd3309458397357ffa2d", null ],
    [ "EPOLL_EVENTS", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361d", [
      [ "EPOLLIN", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da1421ae0d96e8d2b00661a578826a7465", null ],
      [ "EPOLLIN", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da1421ae0d96e8d2b00661a578826a7465", null ],
      [ "EPOLLPRI", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da8f5e4d0d4812a17efe45d99bd342f0ed", null ],
      [ "EPOLLPRI", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da8f5e4d0d4812a17efe45d99bd342f0ed", null ],
      [ "EPOLLOUT", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361dacb3b2222069c7c77e808fc6eddff6965", null ],
      [ "EPOLLOUT", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361dacb3b2222069c7c77e808fc6eddff6965", null ],
      [ "EPOLLRDNORM", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da50f87afb089debdb7ac9e22617e3d7ce", null ],
      [ "EPOLLRDNORM", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da50f87afb089debdb7ac9e22617e3d7ce", null ],
      [ "EPOLLRDBAND", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361dad3c50c7c115b59977cf015955e9d7847", null ],
      [ "EPOLLRDBAND", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361dad3c50c7c115b59977cf015955e9d7847", null ],
      [ "EPOLLWRNORM", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da6673e2173cc476e8aa033157bb0d56d8", null ],
      [ "EPOLLWRNORM", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da6673e2173cc476e8aa033157bb0d56d8", null ],
      [ "EPOLLWRBAND", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361daf27488f55e1c367da5a7d177dcd558c0", null ],
      [ "EPOLLWRBAND", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361daf27488f55e1c367da5a7d177dcd558c0", null ],
      [ "EPOLLMSG", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da98e47276bf18980e2621e3baca9ce390", null ],
      [ "EPOLLMSG", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da98e47276bf18980e2621e3baca9ce390", null ],
      [ "EPOLLERR", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da6219d6dbc169aab7e994d655e0124794", null ],
      [ "EPOLLERR", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da6219d6dbc169aab7e994d655e0124794", null ],
      [ "EPOLLHUP", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da14a7c75ca276cbc9eda1d9ce32189ad0", null ],
      [ "EPOLLHUP", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da14a7c75ca276cbc9eda1d9ce32189ad0", null ],
      [ "EPOLLRDHUP", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da5c7b062dc007d310ba3947aa4fa3f317", null ],
      [ "EPOLLRDHUP", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da5c7b062dc007d310ba3947aa4fa3f317", null ],
      [ "EPOLLEXCLUSIVE", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361daa95b53926eb93fe8a1a4bdd7e917b9be", null ],
      [ "EPOLLEXCLUSIVE", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361daa95b53926eb93fe8a1a4bdd7e917b9be", null ],
      [ "EPOLLWAKEUP", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da7a9d775c6ba189b032b4e9cc546d2697", null ],
      [ "EPOLLWAKEUP", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361da7a9d775c6ba189b032b4e9cc546d2697", null ],
      [ "EPOLLONESHOT", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361dafb6ce49e037c0083d946b30f4b70d9e5", null ],
      [ "EPOLLONESHOT", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361dafb6ce49e037c0083d946b30f4b70d9e5", null ],
      [ "EPOLLET", "sys_2epoll_8h.html#ab97a65b5c201255d4fba9ca80a43361daab3a4009e63ec486262573cd303a2802", null ]
    ] ],
    [ "epoll_create", "sys_2epoll_8h.html#a165878f48eb5637613ed687ccb959344", null ],
    [ "epoll_create1", "sys_2epoll_8h.html#a726c5da84e8351d83d530ffc7d1a5a57", null ],
    [ "epoll_ctl", "sys_2epoll_8h.html#a1105d53a4e1ca4a3eb398a44464a5bc5", null ],
    [ "epoll_wait", "sys_2epoll_8h.html#ab5d2c31bacc6ea7b0e8c0e8487539c6b", null ],
    [ "epoll_pwait", "sys_2epoll_8h.html#aaf6e430ad5c6b1352b4292fa46bbb7e8", null ],
    [ "__EPOLL_PACKED", "sys_2epoll_8h.html#ac26cf4c68fc005a09238e4450ee82612", null ]
];