var structglob__t =
[
    [ "gl_pathc", "structglob__t.html#afbc7b413215373c7793966aff4a61fc7", null ],
    [ "gl_pathv", "structglob__t.html#abd9ba3e5bd7a4767af2cd3dd98a2a64f", null ],
    [ "gl_offs", "structglob__t.html#a681adaf229ca3feed5730ba05ff99cce", null ],
    [ "gl_flags", "structglob__t.html#a1e09b682379e4687de3b58b14ffc2a38", null ],
    [ "gl_closedir", "structglob__t.html#a66bfa365c8ea4f1d811ff8b099777bcb", null ],
    [ "gl_readdir", "structglob__t.html#abf0de73a2cdeb1d95cd472197917de74", null ],
    [ "gl_opendir", "structglob__t.html#a66b19e18d3821fb83834aa8a1a11e141", null ],
    [ "gl_lstat", "structglob__t.html#a8fb27d0eac2c3037d2a60783c677047c", null ],
    [ "gl_stat", "structglob__t.html#a8705e95c86608f5863f46de6ddfb40db", null ]
];