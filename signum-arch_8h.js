var signum_arch_8h =
[
    [ "SIGSTKFLT", "signum-arch_8h.html#a03612fcda4ab9671dfa7051e7b666fb6", null ],
    [ "SIGPWR", "signum-arch_8h.html#a633dbd7dca7d839e6af0299bd4713f0a", null ],
    [ "SIGBUS", "signum-arch_8h.html#aead3d488474201acf18c4b63d91edc3c", null ],
    [ "SIGSYS", "signum-arch_8h.html#a8bacf9eb18fd539099c1bb4666c45d60", null ],
    [ "SIGURG", "signum-arch_8h.html#ad9ff13149e36144a4ea28788948c34dd", null ],
    [ "SIGSTOP", "signum-arch_8h.html#a069e358bc9a864b7dc4fed2440eda14c", null ],
    [ "SIGTSTP", "signum-arch_8h.html#abe65c086e01f0d286b7a785a7e3cdd1a", null ],
    [ "SIGCONT", "signum-arch_8h.html#a024f43063003e753afc6cca1acd6e104", null ],
    [ "SIGCHLD", "signum-arch_8h.html#a0e63521a64cc8bc2b91d36a87d32c9f8", null ],
    [ "SIGTTIN", "signum-arch_8h.html#a2c146e34a6b5a78dfba41cded3331181", null ],
    [ "SIGTTOU", "signum-arch_8h.html#a782864287613e2c5c030411fa9c5e9b1", null ],
    [ "SIGPOLL", "signum-arch_8h.html#a1614264a836d6a5642554775ef0134d0", null ],
    [ "SIGXFSZ", "signum-arch_8h.html#a75440a7aa885a1052dfd3b4393fd9baa", null ],
    [ "SIGXCPU", "signum-arch_8h.html#a7265cbba4972503c1c30a2e52a929874", null ],
    [ "SIGVTALRM", "signum-arch_8h.html#a71403d2f5240e409e213060ea3301851", null ],
    [ "SIGPROF", "signum-arch_8h.html#ab6bd2a2ff7e58d45965ef257f96dfe65", null ],
    [ "SIGUSR1", "signum-arch_8h.html#a944a8250e34bfd7991123abd3436d8c0", null ],
    [ "SIGUSR2", "signum-arch_8h.html#abaaa61abe503c43481e35e21b0b5a031", null ],
    [ "SIGWINCH", "signum-arch_8h.html#a13ba0e7c4365813312eb4566907bce4f", null ],
    [ "SIGIO", "signum-arch_8h.html#a929c5eace94ce2e099c6fa732450ce86", null ],
    [ "SIGIOT", "signum-arch_8h.html#ab12271d5e5911f8a85dc7c3f0e2364c7", null ],
    [ "SIGCLD", "signum-arch_8h.html#a3d4973bb9e3b358d11a7592c127b7ec3", null ],
    [ "__SIGRTMIN", "signum-arch_8h.html#a644e2ca21f05e0b78f34f5a8bf160c0a", null ],
    [ "__SIGRTMAX", "signum-arch_8h.html#a7ee238e41767952da0a3139265c79fd6", null ]
];