var structnd__opt__prefix__info =
[
    [ "nd_opt_pi_type", "structnd__opt__prefix__info.html#ac6130a55465cabe96794e87b63f22325", null ],
    [ "nd_opt_pi_len", "structnd__opt__prefix__info.html#af46d9c9c3c0e41d2068ce18eda375749", null ],
    [ "nd_opt_pi_prefix_len", "structnd__opt__prefix__info.html#a2c012e067f27788890fd35c360799cd3", null ],
    [ "nd_opt_pi_flags_reserved", "structnd__opt__prefix__info.html#a98fa568461e0abb2fd7e758a4e173ed0", null ],
    [ "nd_opt_pi_valid_time", "structnd__opt__prefix__info.html#a343bdba2b8f13f346eda1bb4d7f0a87c", null ],
    [ "nd_opt_pi_preferred_time", "structnd__opt__prefix__info.html#a436b6753cba5927c5124d21ddcf17564", null ],
    [ "nd_opt_pi_reserved2", "structnd__opt__prefix__info.html#a4e41a2855dfc07c464cb53e68506b0de", null ],
    [ "nd_opt_pi_prefix", "structnd__opt__prefix__info.html#ad198dd5414633a38b4d56ceceb13a4a7", null ]
];