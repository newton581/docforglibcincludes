var local__lim_8h =
[
    [ "__undef_NR_OPEN", "local__lim_8h.html#ad4d5491948e2b559ff95b58c63fc01af", null ],
    [ "__undef_LINK_MAX", "local__lim_8h.html#a3ffbf490b46074a5f6d07ebc3740819a", null ],
    [ "__undef_OPEN_MAX", "local__lim_8h.html#a63af1d8815250393de334fc105cbc642", null ],
    [ "__undef_ARG_MAX", "local__lim_8h.html#a6774964df1b050187c6806dabb15b8fc", null ],
    [ "_POSIX_THREAD_KEYS_MAX", "local__lim_8h.html#acfe1124183f47eb0a395e9115446ad51", null ],
    [ "PTHREAD_KEYS_MAX", "local__lim_8h.html#a0a23e66e087bcf5c253b9b2746f19a64", null ],
    [ "_POSIX_THREAD_DESTRUCTOR_ITERATIONS", "local__lim_8h.html#ab1dcb8bed4f0f3540e278a4932223ade", null ],
    [ "PTHREAD_DESTRUCTOR_ITERATIONS", "local__lim_8h.html#a1d96b13a4ba5975d724c5fe13788a957", null ],
    [ "_POSIX_THREAD_THREADS_MAX", "local__lim_8h.html#a4b941a84a6ba25b666a6127b88598fec", null ],
    [ "AIO_PRIO_DELTA_MAX", "local__lim_8h.html#aa9d8b79923c4fc549c074fc387ef65c9", null ],
    [ "PTHREAD_STACK_MIN", "local__lim_8h.html#a8b3dc1c5c1a165d6143b1dce950e8266", null ],
    [ "DELAYTIMER_MAX", "local__lim_8h.html#a86025bd0a07f29a6ae97f310ff9c701c", null ],
    [ "TTY_NAME_MAX", "local__lim_8h.html#a8e87ff1f5de326c7161ef933ff2fb0f1", null ],
    [ "LOGIN_NAME_MAX", "local__lim_8h.html#af3b7f7833ae69cac7adf84f5973715fe", null ],
    [ "HOST_NAME_MAX", "local__lim_8h.html#ac956117a90023ec0971b8f9fce9dec75", null ],
    [ "MQ_PRIO_MAX", "local__lim_8h.html#ad1516b4f64b6dc890b1fa3bf576bfef9", null ],
    [ "SEM_VALUE_MAX", "local__lim_8h.html#a2961bb23950351c6b10976674c27ddaf", null ]
];