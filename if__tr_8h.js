var if__tr_8h =
[
    [ "trh_hdr", "structtrh__hdr.html", "structtrh__hdr" ],
    [ "trllc", "structtrllc.html", "structtrllc" ],
    [ "tr_statistics", "structtr__statistics.html", "structtr__statistics" ],
    [ "TR_ALEN", "if__tr_8h.html#a0e2fde341b03ec511aa78f69bbadce31", null ],
    [ "TR_HLEN", "if__tr_8h.html#ae147bb9b8d6126f757bd77489a7b9123", null ],
    [ "AC", "if__tr_8h.html#a67d1a34c515736c571484ec2a3543da5", null ],
    [ "LLC_FRAME", "if__tr_8h.html#ae4c81b44ad0ff6f70e3f80f1a27538ea", null ],
    [ "EXTENDED_SAP", "if__tr_8h.html#a2a7293861e132d51cd2e84c62f9f1c42", null ],
    [ "UI_CMD", "if__tr_8h.html#a50b02d99eb8a996c3ffaa33e01a33adf", null ],
    [ "TR_RII", "if__tr_8h.html#af5d39532f7d75d9d26ea1c54fec552c3", null ],
    [ "TR_RCF_DIR_BIT", "if__tr_8h.html#a72203b1740cb4e8cfb5aeb1cea80d528", null ],
    [ "TR_RCF_LEN_MASK", "if__tr_8h.html#a893f5054a0d4fbdfd1d010ac985d0d77", null ],
    [ "TR_RCF_BROADCAST", "if__tr_8h.html#a15085018504a5edfa360527bc38ea0f5", null ],
    [ "TR_RCF_LIMITED_BROADCAST", "if__tr_8h.html#ae77b7a8803ba2c37e51497f427dca6ed", null ],
    [ "TR_RCF_FRAME2K", "if__tr_8h.html#a519b7351265b87252e375fe010cd3653", null ],
    [ "TR_RCF_BROADCAST_MASK", "if__tr_8h.html#a08eb5879465eee5ba69756dc2021e010", null ],
    [ "TR_MAXRIFLEN", "if__tr_8h.html#abb5bb6166b5e0651553e5bf0be3af335", null ]
];