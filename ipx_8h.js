var ipx_8h =
[
    [ "sockaddr_ipx", "structsockaddr__ipx.html", "structsockaddr__ipx" ],
    [ "ipx_route_definition", "structipx__route__definition.html", "structipx__route__definition" ],
    [ "ipx_interface_definition", "structipx__interface__definition.html", "structipx__interface__definition" ],
    [ "ipx_config_data", "structipx__config__data.html", "structipx__config__data" ],
    [ "ipx_route_def", "structipx__route__def.html", "structipx__route__def" ],
    [ "SOL_IPX", "ipx_8h.html#a791647b26fd0310796b98d4f11a0f825", null ],
    [ "IPX_TYPE", "ipx_8h.html#a83a85983686a14810e2624b30bad0b78", null ],
    [ "IPX_NODE_LEN", "ipx_8h.html#a079cfce49a9bc12288a72d359338b5ba", null ],
    [ "IPX_MTU", "ipx_8h.html#ab372ca61944fcfe8b2181f451d1aabb0", null ],
    [ "sipx_special", "ipx_8h.html#a51b60f6d4a08cd52690e48cec829a378", null ],
    [ "sipx_action", "ipx_8h.html#a9abe3259c4506a4ddbdee5cffeaa90b1", null ],
    [ "IPX_DLTITF", "ipx_8h.html#a380179e64e1ce375325f6ddf49e81d03", null ],
    [ "IPX_CRTITF", "ipx_8h.html#af95430ae371bb69abe06bc82d3a6a769", null ],
    [ "IPX_FRAME_NONE", "ipx_8h.html#af9f3ec1a3479a120853d503a6c26ff6b", null ],
    [ "IPX_FRAME_SNAP", "ipx_8h.html#ae212ae6dadee4eb1704350406c4f5d45", null ],
    [ "IPX_FRAME_8022", "ipx_8h.html#a51ca28ca0f70596c40a2686b38d2ca99", null ],
    [ "IPX_FRAME_ETHERII", "ipx_8h.html#acf63ad1d53505e40f4836a6a748372ee", null ],
    [ "IPX_FRAME_8023", "ipx_8h.html#af68448b11b35be95b23c1aafb459b46e", null ],
    [ "IPX_FRAME_TR_8022", "ipx_8h.html#abc30796a59679eefd35ab6c46cf0a647", null ],
    [ "IPX_SPECIAL_NONE", "ipx_8h.html#a1ee33d534b8ae7bb1bb034295ae391bc", null ],
    [ "IPX_PRIMARY", "ipx_8h.html#a4ecda2a350a630b1b060fd98fa42f7cb", null ],
    [ "IPX_INTERNAL", "ipx_8h.html#af8f58e8025bd48bc2c0d4fccd3825478", null ],
    [ "IPX_ROUTE_NO_ROUTER", "ipx_8h.html#aec8f8d4af8c4ce7e3af3ca8680fe5fcc", null ],
    [ "IPX_RT_SNAP", "ipx_8h.html#a7d8f9ffcbde0d197a853aaf6ad429749", null ],
    [ "IPX_RT_8022", "ipx_8h.html#a256d678942f4ed3c89e7a7f7310aa58f", null ],
    [ "IPX_RT_BLUEBOOK", "ipx_8h.html#a2728d1837d90d8996e5f391f4a0f5310", null ],
    [ "IPX_RT_ROUTED", "ipx_8h.html#a560df6196ed604bbe0abacfea9bdde23", null ],
    [ "SIOCAIPXITFCRT", "ipx_8h.html#a15aa800c1c8801510001db7cc09e18bb", null ],
    [ "SIOCAIPXPRISLT", "ipx_8h.html#a2846d91bc34e64855f6d75844df4e6c2", null ],
    [ "SIOCIPXCFGDATA", "ipx_8h.html#aa85b66f70ac2248e4c8935f59c6426cf", null ],
    [ "SIOCIPXNCPCONN", "ipx_8h.html#a1e1468281faadd38ad06b73d4d06ecb7", null ],
    [ "ipx_route_definition", "ipx_8h.html#afdf9908f99f989e2c460a823f1ba09b7", null ],
    [ "ipx_interface_definition", "ipx_8h.html#a40d3a4eaacb43ca390d63f1b75a3b024", null ],
    [ "ipx_config_data", "ipx_8h.html#af2dc5302c05b57f3e144a4ab35e90a8b", null ]
];