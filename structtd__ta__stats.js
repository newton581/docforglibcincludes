var structtd__ta__stats =
[
    [ "nthreads", "structtd__ta__stats.html#a650110a95888a2b028fbe0d44f5039de", null ],
    [ "r_concurrency", "structtd__ta__stats.html#a7e6eadc84129e4e2d0a06f77a0f41a54", null ],
    [ "nrunnable_num", "structtd__ta__stats.html#a4ece2fc16d66766adf86639c77227b72", null ],
    [ "nrunnable_den", "structtd__ta__stats.html#a82600cc343124e6d2ad8cfa5f9476dde", null ],
    [ "a_concurrency_num", "structtd__ta__stats.html#a558bfb378039b0a1b92c2749f5a25bf3", null ],
    [ "a_concurrency_den", "structtd__ta__stats.html#aac04982830c8c83cb19b1142d9d2de3c", null ],
    [ "nlwps_num", "structtd__ta__stats.html#ad82a8e9426dfdb70507c05cd354581a3", null ],
    [ "nlwps_den", "structtd__ta__stats.html#ac84d837e257d93aadbbc151ad9a6a397", null ],
    [ "nidle_num", "structtd__ta__stats.html#a9b061a5f1ac78dbcaa3c926af3b46762", null ],
    [ "nidle_den", "structtd__ta__stats.html#ac4fbeb7b14f8d9134b1622f7fbd2981f", null ]
];