var sys_2statvfs_8h =
[
    [ "__fsblkcnt_t_defined", "sys_2statvfs_8h.html#a11c8d124172e996a679ebb7f618fd4e8", null ],
    [ "__fsfilcnt_t_defined", "sys_2statvfs_8h.html#a6f85fe0ee6cbdb6bc75090150e6c83cd", null ],
    [ "fsblkcnt_t", "sys_2statvfs_8h.html#a259e02dbd79df22bec11ea0ecad2e28b", null ],
    [ "fsfilcnt_t", "sys_2statvfs_8h.html#a31e0eb7cd530717cdd080ed6323f224e", null ],
    [ "statvfs", "sys_2statvfs_8h.html#ac10d4bbf744d8e2c2219b7dec2b7d1a9", null ],
    [ "fstatvfs", "sys_2statvfs_8h.html#ace1e1de05e44b4cb7ba8d5cf18dae768", null ]
];