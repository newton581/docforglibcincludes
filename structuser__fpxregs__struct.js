var structuser__fpxregs__struct =
[
    [ "cwd", "structuser__fpxregs__struct.html#a1d2e01e5525ce7bbdcc495c828b58c6c", null ],
    [ "swd", "structuser__fpxregs__struct.html#a5a7032d809a537cc75470d3c19805f55", null ],
    [ "twd", "structuser__fpxregs__struct.html#adfc494cbdd3bb71fa3866dd6f4e2308d", null ],
    [ "fop", "structuser__fpxregs__struct.html#aaa682d6643f8c8649f58e2055c43e3ee", null ],
    [ "fip", "structuser__fpxregs__struct.html#a18baf14cf8fb5da53ad62c379d79b266", null ],
    [ "fcs", "structuser__fpxregs__struct.html#a61af8f5fd154051ad0857046841111d6", null ],
    [ "foo", "structuser__fpxregs__struct.html#a8db9e25e73ee2a9a257b3ca87a5aba4e", null ],
    [ "fos", "structuser__fpxregs__struct.html#ab68d3357a208e1ce69ae6973b5acc5d8", null ],
    [ "mxcsr", "structuser__fpxregs__struct.html#a8b47e1d8ea1aa6118455c72ef36ec643", null ],
    [ "reserved", "structuser__fpxregs__struct.html#a97a861cdf5666c28234c8186d40fe7d2", null ],
    [ "st_space", "structuser__fpxregs__struct.html#a99022d0d8e8c5fe324478c1be9278e62", null ],
    [ "xmm_space", "structuser__fpxregs__struct.html#afe20a7f4e1486b92054ba47bc469cabd", null ],
    [ "padding", "structuser__fpxregs__struct.html#a7b9574da0d2eef7cd9cd328692e37b86", null ]
];