var bits_2dirent_8h =
[
    [ "dirent", "structdirent.html", "structdirent" ],
    [ "d_fileno", "bits_2dirent_8h.html#a28add2fe1e622bd008b1abf449f25035", null ],
    [ "_DIRENT_HAVE_D_RECLEN", "bits_2dirent_8h.html#ae1887b2a3ef03056c13ed701a60f49b9", null ],
    [ "_DIRENT_HAVE_D_OFF", "bits_2dirent_8h.html#a73529c12ca5c52856efedad4b8a42bfe", null ],
    [ "_DIRENT_HAVE_D_TYPE", "bits_2dirent_8h.html#a1e6aefd9b3e29578655bcdf140e43fda", null ],
    [ "_DIRENT_MATCHES_DIRENT64", "bits_2dirent_8h.html#a3218d88b8ddc5e813e6ba9afcb70acac", null ]
];