var struct____pthread__rwlock__arch__t =
[
    [ "__readers", "struct____pthread__rwlock__arch__t.html#aa84c20d49ab9ebeb83dff7ca083eea68", null ],
    [ "__writers", "struct____pthread__rwlock__arch__t.html#a80c4b1ca63906afb0a6277faa12a53f4", null ],
    [ "__wrphase_futex", "struct____pthread__rwlock__arch__t.html#ad863da237f9d4e1843cc44480c29c4be", null ],
    [ "__writers_futex", "struct____pthread__rwlock__arch__t.html#ae0109243611adf0d0a0efc8a9e35bf2f", null ],
    [ "__pad3", "struct____pthread__rwlock__arch__t.html#a4517155989cf32e4d7b027d6c960272e", null ],
    [ "__pad4", "struct____pthread__rwlock__arch__t.html#a8b969d01c60944387707b6d8203f0faa", null ],
    [ "__flags", "struct____pthread__rwlock__arch__t.html#a539163a5a2744aaf15fda40c0578d0ae", null ],
    [ "__shared", "struct____pthread__rwlock__arch__t.html#a679239142fe4579c9127c16d9cfef051", null ],
    [ "__rwelision", "struct____pthread__rwlock__arch__t.html#af296e704bc2f310b9dff6bc5b1f183c8", null ],
    [ "__pad2", "struct____pthread__rwlock__arch__t.html#a015c8dc8a27d1d61ec8f51d28272042b", null ],
    [ "__cur_writer", "struct____pthread__rwlock__arch__t.html#a57a4a0f23e2f2f990eac6d50c0994178", null ]
];