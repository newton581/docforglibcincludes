var structElf64__Verdef =
[
    [ "vd_version", "structElf64__Verdef.html#afe15c8d72df394443deb40da61df3459", null ],
    [ "vd_flags", "structElf64__Verdef.html#a1d69114c03f683d4b6b6f3a83c7021f1", null ],
    [ "vd_ndx", "structElf64__Verdef.html#a069835dc20ac41af0ac611eefbe5169a", null ],
    [ "vd_cnt", "structElf64__Verdef.html#ac41468980738fcc6b1b15ccda97a19a8", null ],
    [ "vd_hash", "structElf64__Verdef.html#afb01f82af6211a4a0dfc314c3b3a43b2", null ],
    [ "vd_aux", "structElf64__Verdef.html#ae59893f742edf4e58e2acd78a1168aa3", null ],
    [ "vd_next", "structElf64__Verdef.html#a9cebd5131a542990d3130489a3b4acec", null ]
];