var bits_2ipc_8h =
[
    [ "IPC_CREAT", "bits_2ipc_8h.html#ace43f23fcb66ddaad964bb8ea8de6e9c", null ],
    [ "IPC_EXCL", "bits_2ipc_8h.html#acd312ab97691605718a3ee9a1c7c63e9", null ],
    [ "IPC_NOWAIT", "bits_2ipc_8h.html#a5afdf5fc48bb22fa27fbd85627b189b9", null ],
    [ "IPC_RMID", "bits_2ipc_8h.html#a752c83032a7bec60c904d97508ea4599", null ],
    [ "IPC_SET", "bits_2ipc_8h.html#a1f1cdce55426e50878b1c71a4fc67a41", null ],
    [ "IPC_STAT", "bits_2ipc_8h.html#a16a91ee69c3cb6bfec425e1bfd5edd18", null ],
    [ "IPC_PRIVATE", "bits_2ipc_8h.html#ae2b9b856a4a657c250b0b2e1cc0835d9", null ]
];