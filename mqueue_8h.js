var mqueue_8h =
[
    [ "mq_open", "mqueue_8h.html#aac6deed341073ece41079c6ea147db39", null ],
    [ "mq_close", "mqueue_8h.html#a3254e137e1fb44f9cecc76a3fc2318d2", null ],
    [ "mq_getattr", "mqueue_8h.html#a2fbd711aed61acabe631decbdf0b8890", null ],
    [ "mq_setattr", "mqueue_8h.html#a2020168e9aa5b591a6bbd63d0c4e9e96", null ],
    [ "mq_unlink", "mqueue_8h.html#a98fadd4a0865592170cb61975c9b6614", null ],
    [ "mq_notify", "mqueue_8h.html#aefbbf7a6bb6311c55f66b0ff9b5957ee", null ],
    [ "mq_receive", "mqueue_8h.html#a458057c68b4fcb7226ec6d61b7afbc54", null ],
    [ "mq_send", "mqueue_8h.html#a0b561e2d631a748f5e28bca9cba1781f", null ],
    [ "mq_timedreceive", "mqueue_8h.html#ae9b123a71476d5991d27e1ac971c27ab", null ],
    [ "mq_timedsend", "mqueue_8h.html#ac566c5e668bad5087abfa0d59be2307d", null ]
];