var dir_396aa916c97a570218b0fe8c621cc232 =
[
    [ "__FILE.h", "____FILE_8h.html", "____FILE_8h" ],
    [ "__fpos64_t.h", "____fpos64__t_8h.html", "____fpos64__t_8h" ],
    [ "__fpos_t.h", "____fpos__t_8h.html", "____fpos__t_8h" ],
    [ "__locale_t.h", "____locale__t_8h.html", "____locale__t_8h" ],
    [ "__mbstate_t.h", "____mbstate__t_8h.html", "____mbstate__t_8h" ],
    [ "__sigset_t.h", "____sigset__t_8h.html", "____sigset__t_8h" ],
    [ "__sigval_t.h", "____sigval__t_8h.html", "____sigval__t_8h" ],
    [ "clock_t.h", "clock__t_8h.html", "clock__t_8h" ],
    [ "clockid_t.h", "clockid__t_8h.html", "clockid__t_8h" ],
    [ "cookie_io_functions_t.h", "cookie__io__functions__t_8h.html", "cookie__io__functions__t_8h" ],
    [ "error_t.h", "error__t_8h.html", "error__t_8h" ],
    [ "FILE.h", "FILE_8h.html", "FILE_8h" ],
    [ "locale_t.h", "locale__t_8h.html", "locale__t_8h" ],
    [ "mbstate_t.h", "mbstate__t_8h.html", "mbstate__t_8h" ],
    [ "res_state.h", "res__state_8h.html", "res__state_8h" ],
    [ "sig_atomic_t.h", "sig__atomic__t_8h.html", "sig__atomic__t_8h" ],
    [ "sigevent_t.h", "sigevent__t_8h.html", "sigevent__t_8h" ],
    [ "siginfo_t.h", "siginfo__t_8h.html", "siginfo__t_8h" ],
    [ "sigset_t.h", "sigset__t_8h.html", "sigset__t_8h" ],
    [ "sigval_t.h", "sigval__t_8h.html", "sigval__t_8h" ],
    [ "stack_t.h", "stack__t_8h.html", "stack__t_8h" ],
    [ "struct_FILE.h", "struct__FILE_8h.html", "struct__FILE_8h" ],
    [ "struct_iovec.h", "struct__iovec_8h.html", "struct__iovec_8h" ],
    [ "struct_itimerspec.h", "struct__itimerspec_8h.html", [
      [ "itimerspec", "structitimerspec.html", "structitimerspec" ]
    ] ],
    [ "struct_msqid_ds.h", "struct__msqid__ds_8h.html", [
      [ "msqid_ds", "structmsqid__ds.html", "structmsqid__ds" ]
    ] ],
    [ "struct_osockaddr.h", "struct__osockaddr_8h.html", [
      [ "osockaddr", "structosockaddr.html", "structosockaddr" ]
    ] ],
    [ "struct_rusage.h", "struct__rusage_8h.html", [
      [ "rusage", "structrusage.html", "structrusage" ]
    ] ],
    [ "struct_sched_param.h", "struct__sched__param_8h.html", [
      [ "sched_param", "structsched__param.html", "structsched__param" ]
    ] ],
    [ "struct_semid_ds.h", "struct__semid__ds_8h.html", [
      [ "semid_ds", "structsemid__ds.html", "structsemid__ds" ]
    ] ],
    [ "struct_shmid_ds.h", "struct__shmid__ds_8h.html", [
      [ "shmid_ds", "structshmid__ds.html", "structshmid__ds" ]
    ] ],
    [ "struct_sigstack.h", "struct__sigstack_8h.html", [
      [ "sigstack", "structsigstack.html", "structsigstack" ]
    ] ],
    [ "struct_statx.h", "struct__statx_8h.html", [
      [ "statx", "structstatx.html", "structstatx" ]
    ] ],
    [ "struct_statx_timestamp.h", "struct__statx__timestamp_8h.html", [
      [ "statx_timestamp", "structstatx__timestamp.html", "structstatx__timestamp" ]
    ] ],
    [ "struct_timespec.h", "struct__timespec_8h.html", [
      [ "timespec", "structtimespec.html", "structtimespec" ]
    ] ],
    [ "struct_timeval.h", "struct__timeval_8h.html", [
      [ "timeval", "structtimeval.html", "structtimeval" ]
    ] ],
    [ "struct_tm.h", "struct__tm_8h.html", [
      [ "tm", "structtm.html", "structtm" ]
    ] ],
    [ "time_t.h", "time__t_8h.html", "time__t_8h" ],
    [ "timer_t.h", "timer__t_8h.html", "timer__t_8h" ],
    [ "wint_t.h", "wint__t_8h.html", "wint__t_8h" ]
];