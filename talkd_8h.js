var talkd_8h =
[
    [ "CTL_MSG", "structCTL__MSG.html", "structCTL__MSG" ],
    [ "CTL_RESPONSE", "structCTL__RESPONSE.html", "structCTL__RESPONSE" ],
    [ "NAME_SIZE", "talkd_8h.html#a834e9a379307f869a10f4da078be5786", null ],
    [ "TTY_SIZE", "talkd_8h.html#a7f582b1520a0339362bd4556e06ecb4a", null ],
    [ "TALK_VERSION", "talkd_8h.html#a6b6b21d49f7c47c1bd9fc18a27f2f56f", null ],
    [ "LEAVE_INVITE", "talkd_8h.html#ae406dac44c8c5c92e57bd0892165aeaa", null ],
    [ "LOOK_UP", "talkd_8h.html#a9659759f0772c8863989689085bc384f", null ],
    [ "DELETE", "talkd_8h.html#abbbe5949f3c1f72e439924f8cf503509", null ],
    [ "ANNOUNCE", "talkd_8h.html#a2387038a7388f10200c86bc137dff526", null ],
    [ "SUCCESS", "talkd_8h.html#aa90cac659d18e8ef6294c7ae337f6b58", null ],
    [ "NOT_HERE", "talkd_8h.html#af1323652e6edc5ceae6a08c77f68ccb5", null ],
    [ "FAILED", "talkd_8h.html#a681680feae4df4182d532564c42fa1fc", null ],
    [ "MACHINE_UNKNOWN", "talkd_8h.html#a7b849def0e1382f614091d45538ab6dc", null ],
    [ "PERMISSION_DENIED", "talkd_8h.html#a8e799e6dafc317175cfd1f30edc1a2e7", null ],
    [ "UNKNOWN_REQUEST", "talkd_8h.html#aa1fd412ce175e4c22f52c1522009b786", null ],
    [ "BADVERSION", "talkd_8h.html#a61ea2f9f86e71bfe0f406fd170fc6357", null ],
    [ "BADADDR", "talkd_8h.html#a63a7c1cde5fb0cf0d0023d55c742dd4f", null ],
    [ "BADCTLADDR", "talkd_8h.html#a910cf8b9f9bcea74e34eff5c6de3a270", null ],
    [ "MAX_LIFE", "talkd_8h.html#a9c363d06a988867421cf992508a715f9", null ],
    [ "RING_WAIT", "talkd_8h.html#a83aa6030131dade06fab7891f292f600", null ]
];