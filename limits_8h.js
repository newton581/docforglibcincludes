var limits_8h =
[
    [ "__GLIBC_INTERNAL_STARTING_HEADER_IMPLEMENTATION", "limits_8h.html#ae10442b7b38b7d32e00266d40e68c7f2", null ],
    [ "MB_LEN_MAX", "limits_8h.html#a88a1b2d6ac46174ac7b571dca5b1f274", null ],
    [ "_LIMITS_H", "limits_8h.html#a326adad4eb665d97f1a65eec3f1e44c7", null ],
    [ "CHAR_BIT", "limits_8h.html#a308d9dd2c0028ddb184b455bbd7865de", null ],
    [ "SCHAR_MIN", "limits_8h.html#aa05d197000ad5c143ada0fcd9379b236", null ],
    [ "SCHAR_MAX", "limits_8h.html#a8c13fdd8c2840edf0cb04a65297037bb", null ],
    [ "UCHAR_MAX", "limits_8h.html#a4066e640ee269d5d8f83ff6643b7af5f", null ],
    [ "CHAR_MIN", "limits_8h.html#a5d707bd32338557ced18c6ac76ca1b3a", null ],
    [ "CHAR_MAX", "limits_8h.html#a778eefd6535a9d4b752fca5dd0af58db", null ],
    [ "SHRT_MIN", "limits_8h.html#ae59de266aceffa1c258ac13f45fe0d18", null ],
    [ "SHRT_MAX", "limits_8h.html#a1f758438cb1c7bcf55da2431f5e319e6", null ],
    [ "USHRT_MAX", "limits_8h.html#a689b119da994dece91d44b5aeac643ed", null ],
    [ "INT_MIN", "limits_8h.html#a21658776274b3d146c674318b635a334", null ],
    [ "INT_MAX", "limits_8h.html#a9ec306f36d50c7375e74f0d1c55a3a67", null ],
    [ "UINT_MAX", "limits_8h.html#ac998ea02fbd821fc123d60445ce76f38", null ],
    [ "LONG_MAX", "limits_8h.html#a50fece4db74f09568b2938db583c5655", null ],
    [ "LONG_MIN", "limits_8h.html#ae8a44c5a7436466221e0f3859d02420f", null ],
    [ "ULONG_MAX", "limits_8h.html#a41c51926a1997aab3503f9083935e06c", null ]
];