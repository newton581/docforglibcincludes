var sigcontext_8h =
[
    [ "_fpx_sw_bytes", "struct__fpx__sw__bytes.html", "struct__fpx__sw__bytes" ],
    [ "_fpreg", "struct__fpreg.html", "struct__fpreg" ],
    [ "_fpxreg", "struct__fpxreg.html", "struct__fpxreg" ],
    [ "_xmmreg", "struct__xmmreg.html", "struct__xmmreg" ],
    [ "_fpstate", "struct__fpstate.html", "struct__fpstate" ],
    [ "sigcontext", "structsigcontext.html", "structsigcontext" ],
    [ "_xsave_hdr", "struct__xsave__hdr.html", "struct__xsave__hdr" ],
    [ "_ymmh_state", "struct__ymmh__state.html", "struct__ymmh__state" ],
    [ "_xstate", "struct__xstate.html", "struct__xstate" ],
    [ "FP_XSTATE_MAGIC1", "sigcontext_8h.html#a9bd00fa6d1cefede39c09cfeb4f40054", null ],
    [ "FP_XSTATE_MAGIC2", "sigcontext_8h.html#ac5993672078b0ab0f51a586ecdef955d", null ],
    [ "FP_XSTATE_MAGIC2_SIZE", "sigcontext_8h.html#ac50600910e3461d528b8eb2162029e32", null ],
    [ "sigcontext_struct", "sigcontext_8h.html#a8a9a15a90f49e7a1c64ea703d0e1c12d", null ],
    [ "X86_FXSR_MAGIC", "sigcontext_8h.html#ad1fa7ee44e3e2835a385b4b01eed72a7", null ]
];