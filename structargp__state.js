var structargp__state =
[
    [ "root_argp", "structargp__state.html#aeb76ae301a95bfd28302d6fcb5939018", null ],
    [ "argc", "structargp__state.html#ae689e0c571be1923c10ecaeff98c3327", null ],
    [ "argv", "structargp__state.html#a5ec3ad1d03907eb6861cb70d9b5d5fd5", null ],
    [ "next", "structargp__state.html#a08956e65ea8a844a793dbfad24752eb0", null ],
    [ "flags", "structargp__state.html#ae87e3e58f670382c8d577398dd95a58a", null ],
    [ "arg_num", "structargp__state.html#a99c0c620622b43850563cf18dfc87c48", null ],
    [ "quoted", "structargp__state.html#a21bf4aa18eaa853202fd336111699aa1", null ],
    [ "input", "structargp__state.html#a67a81c353a4529576ada52e3894ab9c1", null ],
    [ "child_inputs", "structargp__state.html#a98708432d5703047872d0e4a9d789730", null ],
    [ "hook", "structargp__state.html#a111072803a02dc9e13fc5659c4c04fc2", null ],
    [ "name", "structargp__state.html#a7f26462efb02001489ec29e1a08cd9a7", null ],
    [ "err_stream", "structargp__state.html#a415f2c013ce604857b38e6af65525b98", null ],
    [ "out_stream", "structargp__state.html#af25838e7a96764c6e6202f802db823fd", null ],
    [ "pstate", "structargp__state.html#a377e7ea9e565a06ddf5757bdd873daa0", null ]
];