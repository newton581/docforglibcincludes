var unionieee854__long__double =
[
    [ "d", "unionieee854__long__double.html#aa175483552db77c58c0467117fc370fd", null ],
    [ "mantissa1", "unionieee854__long__double.html#ae578239f7096185e75a4c6bc305cee0c", null ],
    [ "mantissa0", "unionieee854__long__double.html#ae6025783e81e1cb865f808d892955592", null ],
    [ "exponent", "unionieee854__long__double.html#afec0a7033ebba3ff2a47124475f236b9", null ],
    [ "negative", "unionieee854__long__double.html#aab16afd98a9d28c6bf7ba60d19b413ec", null ],
    [ "empty", "unionieee854__long__double.html#ac6798497091eff68b057467a17e3111c", null ],
    [ "ieee", "unionieee854__long__double.html#ab7707b32d8e9fa365906ab79ebf79397", null ],
    [ "quiet_nan", "unionieee854__long__double.html#a203f0a3dc46f3a918b716730da4eacbe", null ],
    [ "one", "unionieee854__long__double.html#a9347657828614dcd5a80a34bda7d558e", null ],
    [ "ieee_nan", "unionieee854__long__double.html#aa28a8b50470c23d4394c478440b1af06", null ]
];