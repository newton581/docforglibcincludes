var socket_constants_8h =
[
    [ "SOL_SOCKET", "socket-constants_8h.html#a92d045f6ee2f343d6b28830a9fec082e", null ],
    [ "SO_ACCEPTCONN", "socket-constants_8h.html#a4a86a7abccf8140410bf8a64c571bd6d", null ],
    [ "SO_BROADCAST", "socket-constants_8h.html#ad05e5d66b4608d73747c4a10b802a737", null ],
    [ "SO_DONTROUTE", "socket-constants_8h.html#a4a6d9f7ea4bf046c50102c17ba1faf37", null ],
    [ "SO_ERROR", "socket-constants_8h.html#a040d4fd00495232970a03425bc00e77a", null ],
    [ "SO_KEEPALIVE", "socket-constants_8h.html#a0691781c519eed3f9a634f8eb55cd258", null ],
    [ "SO_LINGER", "socket-constants_8h.html#a552d2cd8ffc1157c016299c5eba95b72", null ],
    [ "SO_OOBINLINE", "socket-constants_8h.html#a1ab39f351679dd0e32436f0e6c9890d4", null ],
    [ "SO_RCVBUF", "socket-constants_8h.html#a0db12e960ac303030400d9fd95391b52", null ],
    [ "SO_RCVLOWAT", "socket-constants_8h.html#ac750f0f8266f391654627fe3068f79c8", null ],
    [ "SO_RCVTIMEO", "socket-constants_8h.html#af2d1ed6a34336a6f3df80fb518325846", null ],
    [ "SO_REUSEADDR", "socket-constants_8h.html#a5589f74fada0d0cd47bd6ea8741a58ee", null ],
    [ "SO_SNDBUF", "socket-constants_8h.html#af618cbb85161ff3196d3bcdf7565ba64", null ],
    [ "SO_SNDLOWAT", "socket-constants_8h.html#a5b4707f0d55cfacf9cd25e5554975c8f", null ],
    [ "SO_SNDTIMEO", "socket-constants_8h.html#ab9d2f7ca5c94bd51cdab3e1913b66e2d", null ],
    [ "SO_TYPE", "socket-constants_8h.html#a8ab1e00e94a92737d3a4b407f7fa90f1", null ]
];