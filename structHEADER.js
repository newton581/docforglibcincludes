var structHEADER =
[
    [ "id", "structHEADER.html#aef1d48d86fb1f3ee012ab3b2458ae9e7", null ],
    [ "rd", "structHEADER.html#a0812f8e9f51fc40e5f43428058a36db1", null ],
    [ "tc", "structHEADER.html#abf0988f44d25a7405c83311458ea7569", null ],
    [ "aa", "structHEADER.html#add27e280d7e94d8f2a2d1452a51ff28a", null ],
    [ "opcode", "structHEADER.html#a3e98a47bbded029ae48c92afe2701bfc", null ],
    [ "qr", "structHEADER.html#ae3054cc3d1355b118a109678e03c5d7e", null ],
    [ "rcode", "structHEADER.html#ab054045810f5f43044073e1d79433722", null ],
    [ "cd", "structHEADER.html#af6ad961d51056d745152ce2f75201b12", null ],
    [ "ad", "structHEADER.html#a1b8edbc3e195ff0580beba30c18bbffc", null ],
    [ "unused", "structHEADER.html#a323e5fe0824dcad119d6235123e4c64b", null ],
    [ "ra", "structHEADER.html#acef34639f03f194787885c971d6e0616", null ],
    [ "qdcount", "structHEADER.html#a0e51191676ae937ba5776ee8c014f97d", null ],
    [ "ancount", "structHEADER.html#ab87d4b06b8ef1853284739cbafa5bb60", null ],
    [ "nscount", "structHEADER.html#a0a5a2d35731f35af169d808f697c80cd", null ],
    [ "arcount", "structHEADER.html#ab4a7f4363fdd0cd7934eec9a9072d1c0", null ]
];