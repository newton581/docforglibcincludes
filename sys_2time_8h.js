var sys_2time_8h =
[
    [ "itimerval", "structitimerval.html", "structitimerval" ],
    [ "__suseconds_t_defined", "sys_2time_8h.html#ac90dbcf0ad67481848a52e8553d4df6f", null ],
    [ "ITIMER_REAL", "sys_2time_8h.html#ace5b149f36c4133045c32d756e2b9a82", null ],
    [ "ITIMER_VIRTUAL", "sys_2time_8h.html#a330e4df6b49a32d370804104ee3eecce", null ],
    [ "ITIMER_PROF", "sys_2time_8h.html#a7d71f3bf9d4bc0941c00d3135f808f06", null ],
    [ "suseconds_t", "sys_2time_8h.html#a0d96f5f233948708f74f5405c787fe3c", null ],
    [ "__itimer_which_t", "sys_2time_8h.html#aabcead714e1bf464a2478fab652c937e", null ],
    [ "__itimer_which", "sys_2time_8h.html#ace61e2889347bbd2bb3c990e89dc6928", [
      [ "ITIMER_REAL", "sys_2time_8h.html#ace61e2889347bbd2bb3c990e89dc6928a9aa6bb697758ee72273c417a706ae1cb", null ],
      [ "ITIMER_REAL", "sys_2time_8h.html#ace61e2889347bbd2bb3c990e89dc6928a9aa6bb697758ee72273c417a706ae1cb", null ],
      [ "ITIMER_VIRTUAL", "sys_2time_8h.html#ace61e2889347bbd2bb3c990e89dc6928abce1bee49ae6d6940a3bdcc42ff7bf95", null ],
      [ "ITIMER_VIRTUAL", "sys_2time_8h.html#ace61e2889347bbd2bb3c990e89dc6928abce1bee49ae6d6940a3bdcc42ff7bf95", null ],
      [ "ITIMER_PROF", "sys_2time_8h.html#ace61e2889347bbd2bb3c990e89dc6928a56982edbfec2cfdf3d17da24c1c4e80c", null ]
    ] ],
    [ "gettimeofday", "sys_2time_8h.html#a07ad10fe323d0ea7872971ffb0f00a89", null ],
    [ "getitimer", "sys_2time_8h.html#a87dc6dd7cb3dee26ebf591ceca0efb9e", null ],
    [ "setitimer", "sys_2time_8h.html#a3d06f7574674a400532bf68a3ddc1e68", null ],
    [ "utimes", "sys_2time_8h.html#a40ce6eae11c4bf3b7f27ac36b1e27e0a", null ]
];