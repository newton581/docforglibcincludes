var proc__service_8h =
[
    [ "ps_err_e", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777", [
      [ "PS_OK", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777a4d9e1f94202d0062728898d11d1105b1", null ],
      [ "PS_ERR", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777addab70dfd82808e0e99f118e3e63e577", null ],
      [ "PS_BADPID", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777a7c5acae2ddd9511e369ab05244f2eb27", null ],
      [ "PS_BADLID", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777af6e5530891a6d238582a476581f0dabe", null ],
      [ "PS_BADADDR", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777afa7788183e6e4f1c60f81197d27db49d", null ],
      [ "PS_NOSYM", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777a48bd84aa0aa4d92bd56e26a49bf739d8", null ],
      [ "PS_NOFREGS", "proc__service_8h.html#a6e7ca191f6aa7c32a4ada70ec9da4777a86e302dc9884d193f700ddd2e5792972", null ]
    ] ],
    [ "ps_pdread", "proc__service_8h.html#aee18ebe8fdd0c2dc3fdb13ae34031848", null ],
    [ "ps_pdwrite", "proc__service_8h.html#ab6d6bbc71844eb93b9b28d845708e22d", null ],
    [ "ps_ptread", "proc__service_8h.html#a11712fb499450fa0971403ee02c77283", null ],
    [ "ps_ptwrite", "proc__service_8h.html#af22abe62601862c77169a7ed408c892f", null ],
    [ "ps_lgetregs", "proc__service_8h.html#af4b5b6988b1ece7cf4c4ae6ad8c03424", null ],
    [ "ps_lsetregs", "proc__service_8h.html#aa935bf59fa0a96cb48a66613ee1d1029", null ],
    [ "ps_lgetfpregs", "proc__service_8h.html#ab421691dbc49f54c4f8ebc335e2b8951", null ],
    [ "ps_lsetfpregs", "proc__service_8h.html#addcb8661a9e8c10f94cb4d942fceb467", null ],
    [ "ps_getpid", "proc__service_8h.html#a37e30e127f61a93669733d8dbfe7fd5e", null ],
    [ "ps_get_thread_area", "proc__service_8h.html#ad216d299d8b9a4557f1c343c26a720ad", null ],
    [ "ps_pglobal_lookup", "proc__service_8h.html#a725b2074d03f82f1ddad408037e65cec", null ],
    [ "ps_pstop", "proc__service_8h.html#a4b2385575384f2a543e6a845b6bd08af", null ],
    [ "ps_pcontinue", "proc__service_8h.html#a8c85987adb816a3be5507aa3ea8d3e5c", null ],
    [ "ps_lstop", "proc__service_8h.html#a9f749389bcd77d2c282083972a80d832", null ],
    [ "ps_lcontinue", "proc__service_8h.html#a0cb9b27d9e4ad42f6e1ef6878233e72d", null ]
];