var reg_8h =
[
    [ "EBX", "reg_8h.html#ab115c151477ddd6da5358ddc00071052", null ],
    [ "ECX", "reg_8h.html#ae52163bab922ab29302d224786b8526d", null ],
    [ "EDX", "reg_8h.html#a8104635452d33b8536b33b551d98998f", null ],
    [ "ESI", "reg_8h.html#ad4c46ad303098e5253b9216faff99d48", null ],
    [ "EDI", "reg_8h.html#aebb930ae9eecaa5a8fa94d2ef4f159e0", null ],
    [ "EBP", "reg_8h.html#a220a1f7741c506d2c73c9d5f0c3ad036", null ],
    [ "EAX", "reg_8h.html#a2fa1397b8142d609008de2e71c83e854", null ],
    [ "DS", "reg_8h.html#a343a817759efae73463660b09a2a898f", null ],
    [ "ES", "reg_8h.html#a403811204922acc8d200abb94fa62fe4", null ],
    [ "FS", "reg_8h.html#a30588c5eca7c9cb6ebba02a0236f0119", null ],
    [ "GS", "reg_8h.html#a9d7be33a951c7d74d3d6b45de8d44729", null ],
    [ "ORIG_EAX", "reg_8h.html#a04089417205329594d8e4ca5f6e482c6", null ],
    [ "EIP", "reg_8h.html#aa3aae9858ac5a0baffdfbbe501f4abfa", null ],
    [ "CS", "reg_8h.html#a3780e2fe762dc532df7d0f030b55caa0", null ],
    [ "EFL", "reg_8h.html#a58fd0a150970f39c6ce5877a2c6e453e", null ],
    [ "UESP", "reg_8h.html#a793081248ba04b3204d8605faaf2fa53", null ],
    [ "SS", "reg_8h.html#a88f7782e210e61586baf33b93240d905", null ]
];