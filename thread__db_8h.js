var thread__db_8h =
[
    [ "td_thrhandle", "structtd__thrhandle.html", "structtd__thrhandle" ],
    [ "td_thr_events", "structtd__thr__events.html", "structtd__thr__events" ],
    [ "td_notify", "structtd__notify.html", "structtd__notify" ],
    [ "td_event_msg", "structtd__event__msg.html", "structtd__event__msg" ],
    [ "td_eventbuf_t", "structtd__eventbuf__t.html", "structtd__eventbuf__t" ],
    [ "td_ta_stats", "structtd__ta__stats.html", "structtd__ta__stats" ],
    [ "td_thrinfo", "structtd__thrinfo.html", "structtd__thrinfo" ],
    [ "TD_THR_ANY_USER_FLAGS", "thread__db_8h.html#ac004d54a1e1a2221185a34a5076673d4", null ],
    [ "TD_THR_LOWEST_PRIORITY", "thread__db_8h.html#a39686658953e72ecc96340a450187dd3", null ],
    [ "TD_SIGNO_MASK", "thread__db_8h.html#a0a42742d59778244b7f5e7ab2c97527d", null ],
    [ "TD_EVENTSIZE", "thread__db_8h.html#a566c8e19ac6cf0deea02c97813cd08f8", null ],
    [ "BT_UISHIFT", "thread__db_8h.html#a05b3c1850eb6cc223e567f43e821d1c6", null ],
    [ "BT_NBIPUI", "thread__db_8h.html#adcc9c39f556e7d692448f6683911103a", null ],
    [ "BT_UIMASK", "thread__db_8h.html#a7fffa346bd3aace7d19f39f64bad1b3a", null ],
    [ "__td_eventmask", "thread__db_8h.html#ac2dafa001894d4e4648075f9f2c82603", null ],
    [ "__td_eventword", "thread__db_8h.html#aedb2cc7061993e2ac7b9767a15bef103", null ],
    [ "td_event_emptyset", "thread__db_8h.html#ae7be534e38e98b9d0797806cd0716ac2", null ],
    [ "td_event_fillset", "thread__db_8h.html#a3f76aaf28db608aabea9049363c5beca", null ],
    [ "td_event_addset", "thread__db_8h.html#ae5962c9b1a62b04090df920a7d3cc6c5", null ],
    [ "td_event_delset", "thread__db_8h.html#a486572a73e88fbfd3356be37cd917540", null ],
    [ "td_eventismember", "thread__db_8h.html#a21213fcc9caccc481694555511b89b49", null ],
    [ "td_eventisempty", "thread__db_8h.html#ad40d1452d416476c42a1e96f8ab49959", null ],
    [ "td_thragent_t", "thread__db_8h.html#a851f228594aa5e5537c2e91657a15a8f", null ],
    [ "td_thrhandle_t", "thread__db_8h.html#ac09ff273ecfd7b19cbe57c60dc7414bb", null ],
    [ "td_thr_events_t", "thread__db_8h.html#acfe270077c18e2b0bba7ef312163b725", null ],
    [ "td_notify_t", "thread__db_8h.html#acc4fd579300da66409a4c23f958935ca", null ],
    [ "td_event_msg_t", "thread__db_8h.html#af2c9673f334208312380bda5a7696abe", null ],
    [ "td_ta_stats_t", "thread__db_8h.html#a88924c9b9b9015cd15eb71efaa230e03", null ],
    [ "thread_t", "thread__db_8h.html#aa406fd87cfd69d7c0a93b6a241978712", null ],
    [ "thread_key_t", "thread__db_8h.html#a0753bf61cc9ac279c16a75f27fb88cae", null ],
    [ "td_thr_iter_f", "thread__db_8h.html#a3a733eebfdfaf16591cdf4986b13f530", null ],
    [ "td_key_iter_f", "thread__db_8h.html#a1a9c28f8b9a2636385a4c1e7bae5ec03", null ],
    [ "td_thrinfo_t", "thread__db_8h.html#ae18231df4fc8c6826052fe55ce2c70fa", null ],
    [ "td_err_e", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434", [
      [ "TD_OK", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a93e810059742feb944de782cee183f3f", null ],
      [ "TD_ERR", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a59f033ea09df04aef2c83400b5d245cd", null ],
      [ "TD_NOTHR", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a923e209a5e9fd286ddf7f691db028bd8", null ],
      [ "TD_NOSV", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434ac3b9729dedbfd41ec77e61935717ef06", null ],
      [ "TD_NOLWP", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434aebb10b6538c75c90951c6ef972673508", null ],
      [ "TD_BADPH", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a09f100a56667a7cc3eaba21dca2ef5bb", null ],
      [ "TD_BADTH", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a9766e98fa78f694c1bc1149049a7ffa1", null ],
      [ "TD_BADSH", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434aedd104e68a48e0219dd8df261d8a05ba", null ],
      [ "TD_BADTA", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a0304c83af37ac9bbd8dd3ec2cf46d735", null ],
      [ "TD_BADKEY", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a35c9620b6daaade5adbd313777ba50f6", null ],
      [ "TD_NOMSG", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a7d15a6cd0a614e2ad07a716b384baaf3", null ],
      [ "TD_NOFPREGS", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a176156ac8bcd0cd35cc2763567a47d81", null ],
      [ "TD_NOLIBTHREAD", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434adc7eb8cb36425a689e6770156390a74a", null ],
      [ "TD_NOEVENT", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434adede655355684a3520222447afd2f010", null ],
      [ "TD_NOCAPAB", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a39c9423cfde70c6cbdaee285dfd89add", null ],
      [ "TD_DBERR", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a15dc23b1af650848117b5052cf3b7603", null ],
      [ "TD_NOAPLIC", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a00ad61c57929da5e28525ac68e2535ea", null ],
      [ "TD_NOTSD", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a5d74afeba53bbaff261358d282c977b4", null ],
      [ "TD_MALLOC", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a1664fb7160fb1dbbb982039e1e594e2c", null ],
      [ "TD_PARTIALREG", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434ade80c9872ca4dbb6ca3fb401ae710bba", null ],
      [ "TD_NOXREGS", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434ac05eb64179e5733c2b7636006511afaf", null ],
      [ "TD_TLSDEFER", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434ab65dea1536d21e36d6b14d15dcfeb814", null ],
      [ "TD_NOTALLOC", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a1da6860f55086bf113aa77237d5c34fa", null ],
      [ "TD_VERSION", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a1a1e2a6c024b0286c54cc5fc2bffbca7", null ],
      [ "TD_NOTLS", "thread__db_8h.html#a03bba0f82045c1a5bb35aa55e693f434a69d2c51a658b737a0bc003df4ca11480", null ]
    ] ],
    [ "td_thr_state_e", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8e", [
      [ "TD_THR_ANY_STATE", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8eaa7598975576c3ad9f6900645f216ff74", null ],
      [ "TD_THR_UNKNOWN", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8eae5a95caf13bb3f3138f8c2f7c42a8473", null ],
      [ "TD_THR_STOPPED", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8ea8b36ffff700a29862946d8bf7a86e236", null ],
      [ "TD_THR_RUN", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8ea34ea41cb09d07fcf5089666087431c1e", null ],
      [ "TD_THR_ACTIVE", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8ead2531288df0901605a906357be33768e", null ],
      [ "TD_THR_ZOMBIE", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8eade49dca447c1cd9167eb487102f580b3", null ],
      [ "TD_THR_SLEEP", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8ea3045ddd4f7fd42aef776b6ece1191903", null ],
      [ "TD_THR_STOPPED_ASLEEP", "thread__db_8h.html#a7e015b84265c7d3667103553d8230d8eacceb59b58d49515364ad81281cac55c9", null ]
    ] ],
    [ "td_thr_type_e", "thread__db_8h.html#aabda5874a9e345d0cb603ac251cd4860", [
      [ "TD_THR_ANY_TYPE", "thread__db_8h.html#aabda5874a9e345d0cb603ac251cd4860a61be8ba49137889b29a4c05ffa897b1e", null ],
      [ "TD_THR_USER", "thread__db_8h.html#aabda5874a9e345d0cb603ac251cd4860ab396431a5dda6157356c01be75898e8e", null ],
      [ "TD_THR_SYSTEM", "thread__db_8h.html#aabda5874a9e345d0cb603ac251cd4860ac1abf1f8a110ef92acbfdee695e8a95a", null ]
    ] ],
    [ "td_event_e", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0", [
      [ "TD_ALL_EVENTS", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a48208ed9033aebe2dbdf76d2faa3b894", null ],
      [ "TD_EVENT_NONE", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a31b0ff99c014820d52c73c6ccae4306d", null ],
      [ "TD_READY", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a6c4dacbb235cbcb59aec0c49418f06a3", null ],
      [ "TD_SLEEP", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a0bab4a91b175f1d400ebf96dd103514c", null ],
      [ "TD_SWITCHTO", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a1a3e6e5206475ff3488455eb3c5e81b3", null ],
      [ "TD_SWITCHFROM", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0ab39bd74eebc38034e9e34ac9ce9821b5", null ],
      [ "TD_LOCK_TRY", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a93aac2643d81ef8727961cfba33cab19", null ],
      [ "TD_CATCHSIG", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a73d1ac373bb2d3bd00ec904ff0607a4e", null ],
      [ "TD_IDLE", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a3990a5cb3b5845307585dbda56421f6e", null ],
      [ "TD_CREATE", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0aecc98b124ac8e6a5ce8ad882158bf863", null ],
      [ "TD_DEATH", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a40da0b34365d11c6c5c56c8fce78dd69", null ],
      [ "TD_PREEMPT", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0ab2bbe083c1c053d28d3004586c80c238", null ],
      [ "TD_PRI_INHERIT", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0af76ee9fa53d2ed3ef83be475f59d8e83", null ],
      [ "TD_REAP", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a0e2eeb1ba4b682954457677381134822", null ],
      [ "TD_CONCURRENCY", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0ab4da93d2f1e61b0a9f34db4570bc2524", null ],
      [ "TD_TIMEOUT", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a4e192b7e87597e3a35bf400dc399f80d", null ],
      [ "TD_MIN_EVENT_NUM", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0ab2c783ef0835fb6add483b604bec07d1", null ],
      [ "TD_MAX_EVENT_NUM", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a6f606b62e2f421639802b797433e452d", null ],
      [ "TD_EVENTS_ENABLE", "thread__db_8h.html#ae1a250281b93679f2616df2bbcba40e0a0f0c4483e83f1fb1be9dcd9478942eba", null ]
    ] ],
    [ "td_notify_e", "thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4f", [
      [ "NOTIFY_BPT", "thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4fa1c3948b71ca8133c24b64c719fecb05f", null ],
      [ "NOTIFY_AUTOBPT", "thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4fa1e811d28b111355bf4125b05af7f0292", null ],
      [ "NOTIFY_SYSCALL", "thread__db_8h.html#a4a5544e755cdd6cc8c0cf4dd9904ed4fa4ffa647540fafbc2590f12fd4e2eafe8", null ]
    ] ],
    [ "td_init", "thread__db_8h.html#abc8d849640a0e0d2b78f2d4d2c82adca", null ],
    [ "td_log", "thread__db_8h.html#a9220d9325223b104a601436d07d90bef", null ],
    [ "td_symbol_list", "thread__db_8h.html#a5fe8e070bafafaec7170dfbf6863d6ef", null ],
    [ "td_ta_new", "thread__db_8h.html#a560ade2d2a89e429856072c075cec38b", null ],
    [ "td_ta_delete", "thread__db_8h.html#af1ffd364ea00f860c076b5b8d08a43f0", null ],
    [ "td_ta_get_nthreads", "thread__db_8h.html#a797dfc9f2c7d443a3225a6e994df7191", null ],
    [ "td_ta_get_ph", "thread__db_8h.html#a9e26a1eb8b2801d3dc6ecc79e6f8f0a1", null ],
    [ "td_ta_map_id2thr", "thread__db_8h.html#a349494525c33af0675543657520c6617", null ],
    [ "td_ta_map_lwp2thr", "thread__db_8h.html#aa7dfd6cc674cc4f3232928dd422c9966", null ],
    [ "td_ta_thr_iter", "thread__db_8h.html#a88fcd4452d947de238d2e5882825f7a2", null ],
    [ "td_ta_tsd_iter", "thread__db_8h.html#a0919ebcbdb79107743bff5e161755b25", null ],
    [ "td_ta_event_addr", "thread__db_8h.html#a16f08639d069f63a4b5711064057b81e", null ],
    [ "td_ta_set_event", "thread__db_8h.html#a3ceb639eba7e6de8f73ffd581d0d2d19", null ],
    [ "td_ta_clear_event", "thread__db_8h.html#a001058be056aa5376129bf42e8e1ef38", null ],
    [ "td_ta_event_getmsg", "thread__db_8h.html#a8fb0fe65e1c65c3d34d6486944b190a3", null ],
    [ "td_ta_setconcurrency", "thread__db_8h.html#a46ee0fd2eda381b1001ccec9e4ae51e1", null ],
    [ "td_ta_enable_stats", "thread__db_8h.html#a38e7bdd8203fbd6ae1751a8fe99cecc8", null ],
    [ "td_ta_reset_stats", "thread__db_8h.html#a016015741b09e8bf19bad88e2d609744", null ],
    [ "td_ta_get_stats", "thread__db_8h.html#ab88c50443f2ddaa8cee05610a8137b33", null ],
    [ "td_thr_validate", "thread__db_8h.html#acbd836ad9df35a3ecc639dc5c527d62e", null ],
    [ "td_thr_get_info", "thread__db_8h.html#afd523bd275051655a6d6b997e9486282", null ],
    [ "td_thr_getfpregs", "thread__db_8h.html#a31eb128e904c8ff67672f3593b901a45", null ],
    [ "td_thr_getgregs", "thread__db_8h.html#ae163da0eb1f56cfe5db0a71bf723ca3c", null ],
    [ "td_thr_getxregs", "thread__db_8h.html#a0d28439d16bbe9f26cee85844dc7d7d9", null ],
    [ "td_thr_getxregsize", "thread__db_8h.html#a8bdf233dda995ae3b407de1a636620d7", null ],
    [ "td_thr_setfpregs", "thread__db_8h.html#a416c3326bfd83ff7793f72a696c5670b", null ],
    [ "td_thr_setgregs", "thread__db_8h.html#a114d7269990e08ed770837f851bc73ff", null ],
    [ "td_thr_setxregs", "thread__db_8h.html#a5e6d5d0b3f9a9e6457d87e43d7eefb18", null ],
    [ "td_thr_tlsbase", "thread__db_8h.html#ad946181da86deab7941c791bba940d9a", null ],
    [ "td_thr_tls_get_addr", "thread__db_8h.html#acbdba550039412dd7707f5d2219e6a55", null ],
    [ "td_thr_event_enable", "thread__db_8h.html#a2824139f7cd3bb58410f0992c9e87dfb", null ],
    [ "td_thr_set_event", "thread__db_8h.html#a986c0e630e53f7c97f4b1c6ecf7e98c2", null ],
    [ "td_thr_clear_event", "thread__db_8h.html#ac9e33dded8e236821cfb45700285c33c", null ],
    [ "td_thr_event_getmsg", "thread__db_8h.html#a1b20be84d780114cf18cd602db6f53f2", null ],
    [ "td_thr_setprio", "thread__db_8h.html#a070a74765e4fe6516a7e2242cde38203", null ],
    [ "td_thr_setsigpending", "thread__db_8h.html#aab715140cb2fd940b945366486c4c41c", null ],
    [ "td_thr_sigsetmask", "thread__db_8h.html#a39c4edfeba64336a5e0aa14bb8ef9789", null ],
    [ "td_thr_tsd", "thread__db_8h.html#a6e1728b292b68d0ede71c50d48416cd5", null ],
    [ "td_thr_dbsuspend", "thread__db_8h.html#a972dfe8cedc503e4c0b575584ba11f17", null ],
    [ "td_thr_dbresume", "thread__db_8h.html#a49866210d12039bc39e964d8be334a5a", null ]
];