var structifaddrs =
[
    [ "ifa_next", "structifaddrs.html#a56ad9ba85e2c8f8634b8f15fc53b6447", null ],
    [ "ifa_name", "structifaddrs.html#a15b0944beb947aaef5e8532635889f6f", null ],
    [ "ifa_flags", "structifaddrs.html#a156cc3214646519277ac6be2d66fcc2b", null ],
    [ "ifa_addr", "structifaddrs.html#a12d148f51adf48cee024fc6204f42c94", null ],
    [ "ifa_netmask", "structifaddrs.html#acac3a8d7d40841c497bb55b9e42c41d5", null ],
    [ "ifu_broadaddr", "structifaddrs.html#add59b160fa5ec481f1f650158cc746ab", null ],
    [ "ifu_dstaddr", "structifaddrs.html#a67a6b7d90d3648e32752c8251cfce98a", null ],
    [ "ifa_ifu", "structifaddrs.html#aa1044c3929ae2a433a44ecf9b7d690b9", null ],
    [ "ifa_data", "structifaddrs.html#a7298836283614a0a469c8da1eabdabb9", null ]
];