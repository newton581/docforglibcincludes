var structin6__rtmsg =
[
    [ "rtmsg_dst", "structin6__rtmsg.html#a368e4c91c896f62a2facb99bbe9fe78e", null ],
    [ "rtmsg_src", "structin6__rtmsg.html#ad3f7b4b6146e4696b805922ee9afb4d7", null ],
    [ "rtmsg_gateway", "structin6__rtmsg.html#abad12d3a267fff9392d368b9e749ef67", null ],
    [ "rtmsg_type", "structin6__rtmsg.html#a12ae3095bc45b3b02982631a756b9a64", null ],
    [ "rtmsg_dst_len", "structin6__rtmsg.html#a87c7fc6f5bd264a0258be74bfbfc94a4", null ],
    [ "rtmsg_src_len", "structin6__rtmsg.html#a7255eba8f09832103e92182de4b13395", null ],
    [ "rtmsg_metric", "structin6__rtmsg.html#a9aabccf6955cbe5b011834c422a4975b", null ],
    [ "rtmsg_info", "structin6__rtmsg.html#aa805cc9b0eb2f0f26ef0509b2f0dd985", null ],
    [ "rtmsg_flags", "structin6__rtmsg.html#a3a105a12eff33c322a2ddd3b1c5d2675", null ],
    [ "rtmsg_ifindex", "structin6__rtmsg.html#a0180c58b6da363fc28f732f59c4e4e4c", null ]
];