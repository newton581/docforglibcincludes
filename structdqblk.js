var structdqblk =
[
    [ "dqb_bhardlimit", "structdqblk.html#aeb20af8c1780749a1180e928b47391d8", null ],
    [ "dqb_bsoftlimit", "structdqblk.html#a58e37828553208c57e678a7eda0ab11c", null ],
    [ "dqb_curspace", "structdqblk.html#a0faf798c8d21f335ac6546c247f4b5c7", null ],
    [ "dqb_ihardlimit", "structdqblk.html#abef2ae499aa74e37362f407841ba6ad3", null ],
    [ "dqb_isoftlimit", "structdqblk.html#a11e980eb7f9dfdc9aa8097208a46e927", null ],
    [ "dqb_curinodes", "structdqblk.html#a3d3645cdfc3864ec08bede15ae815ff6", null ],
    [ "dqb_btime", "structdqblk.html#a25805333790a7c6d15bc63b72fb61f1b", null ],
    [ "dqb_itime", "structdqblk.html#acc2cb13c39d3cac9e9fce53da90e1669", null ],
    [ "dqb_valid", "structdqblk.html#abed86d61c3e6804a5b07b04fe58ceba3", null ]
];