var structobstack =
[
    [ "chunk_size", "structobstack.html#aae4ebede691123c42ddccb53b4260480", null ],
    [ "chunk", "structobstack.html#ad11260c09c506e85a86d3f2e6f730d3c", null ],
    [ "object_base", "structobstack.html#a39dc40bc7eccec4f2810d025a480f260", null ],
    [ "next_free", "structobstack.html#a1134d5cfda84e15f946dba23a36a31b0", null ],
    [ "chunk_limit", "structobstack.html#adda2258fb1ef558a3cc061e478247528", null ],
    [ "tempint", "structobstack.html#a2f74442c78b953dc44d3f3ff12dde341", null ],
    [ "tempptr", "structobstack.html#a5a26a2770455281a47210a9564826c9c", null ],
    [ "temp", "structobstack.html#ae2c669dbbaa34d026ec6e74e76bbd4c3", null ],
    [ "alignment_mask", "structobstack.html#aede35e3feeb2e2a6bb7d477080da0505", null ],
    [ "chunkfun", "structobstack.html#a918c4796807ab238c071ef0f6e40e4c6", null ],
    [ "freefun", "structobstack.html#af45243f3fa387250feb88fb66ef15f3e", null ],
    [ "extra_arg", "structobstack.html#a704fa8dd8d9289c8b4e10abf42ea4451", null ],
    [ "use_extra_arg", "structobstack.html#a55cda74d7c4902e288a16a5ffabf9d90", null ],
    [ "maybe_empty_object", "structobstack.html#ad95e854a072cbf8dee2ad33b3815bb52", null ],
    [ "alloc_failed", "structobstack.html#a5594d29faa2e5fe4a099eeef480060f5", null ]
];