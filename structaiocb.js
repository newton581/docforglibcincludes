var structaiocb =
[
    [ "aio_fildes", "structaiocb.html#ad6b6e95e6e4a79487f7e6edaae26003f", null ],
    [ "aio_lio_opcode", "structaiocb.html#a8a23597de7bfc422b58ec9816ced7d47", null ],
    [ "aio_reqprio", "structaiocb.html#a7a8f63c080c8602ebcacbb86e9f5547e", null ],
    [ "aio_buf", "structaiocb.html#a60a966202034e1abde4ca3ea0965fa30", null ],
    [ "aio_nbytes", "structaiocb.html#a6b4f8a1d05ed5444784389734d71cda7", null ],
    [ "aio_sigevent", "structaiocb.html#a7065f3086beb9cdffc525c1d09949268", null ],
    [ "__next_prio", "structaiocb.html#a705a62229b5034359d09ec95fc6d0c09", null ],
    [ "__abs_prio", "structaiocb.html#aa73c7812ecbe15e0f58c484d46318937", null ],
    [ "__policy", "structaiocb.html#ab7afb99aa3222db2250d9962a72399b8", null ],
    [ "__error_code", "structaiocb.html#a34393d48fd4169a77b828079e32f2f5e", null ],
    [ "__return_value", "structaiocb.html#a5b10e3b61e635476055c511df4181f18", null ],
    [ "aio_offset", "structaiocb.html#ae11b136f0a64b8d6a2210aa18188a6a7", null ],
    [ "__pad", "structaiocb.html#aecb8eeef12bcd280e6b8aab57ddd89a9", null ],
    [ "__glibc_reserved", "structaiocb.html#adafb531b9bad89e957d538e994e44754", null ]
];