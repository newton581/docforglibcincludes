var structstatx =
[
    [ "stx_mask", "structstatx.html#a291f04ee4cff9cb2ab06fc1f8b38d2ed", null ],
    [ "stx_blksize", "structstatx.html#a412afbe61aafaa0b2b4bbbff3f78fc5c", null ],
    [ "stx_attributes", "structstatx.html#a8784827fc5a85ebe003c924ff00048ba", null ],
    [ "stx_nlink", "structstatx.html#aa833e048ac67f7d6a786ae986c82a10b", null ],
    [ "stx_uid", "structstatx.html#a3fa69f155258518fdc6ff272543f8c0b", null ],
    [ "stx_gid", "structstatx.html#a35518d3c4463c2964aadd0a71454dc73", null ],
    [ "stx_mode", "structstatx.html#a708f401bed16a95ef360ed7ae102cb95", null ],
    [ "__statx_pad1", "structstatx.html#a3fe4252b161ab4f25aa241ea502e05cb", null ],
    [ "stx_ino", "structstatx.html#a1f11b954f91476e2db8795042d5aa2f3", null ],
    [ "stx_size", "structstatx.html#a6238df92d354191e65bce44c399d19ba", null ],
    [ "stx_blocks", "structstatx.html#a972876db9f6d4362307c30f726af55a9", null ],
    [ "stx_attributes_mask", "structstatx.html#a4b1a1d56c1ec9d8d5d1529adb88ef40e", null ],
    [ "stx_atime", "structstatx.html#adb2b54f235dd77b0288c233107100919", null ],
    [ "stx_btime", "structstatx.html#a59252b595e69eb20d38938dc22de279e", null ],
    [ "stx_ctime", "structstatx.html#afb5556958d755ca1ea27b209ff6c7bd5", null ],
    [ "stx_mtime", "structstatx.html#a27f2721d912b3d5627a22926bb744d60", null ],
    [ "stx_rdev_major", "structstatx.html#a1ff1790762770eda0a942c6f1cedb924", null ],
    [ "stx_rdev_minor", "structstatx.html#a9446edc749686b43b496408a7211d0c8", null ],
    [ "stx_dev_major", "structstatx.html#ab7980b50de52435d49bb62f980deeba7", null ],
    [ "stx_dev_minor", "structstatx.html#a256027d5866baf50a3ef0d3058a61abe", null ],
    [ "__statx_pad2", "structstatx.html#a3f4949dc8091ab25ad43a4d19d8b351a", null ]
];