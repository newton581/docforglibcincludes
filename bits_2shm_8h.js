var bits_2shm_8h =
[
    [ "SHM_R", "bits_2shm_8h.html#a45da2dbfb146e926c8fd842379c0362c", null ],
    [ "SHM_W", "bits_2shm_8h.html#afd00993215ed6030ec817bf3615044d1", null ],
    [ "SHM_RDONLY", "bits_2shm_8h.html#a899e8ef0c4c33e2a5cc708c05c75429a", null ],
    [ "SHM_RND", "bits_2shm_8h.html#a4ae5f621aa1333d9d5962c3e9d674a90", null ],
    [ "SHM_REMAP", "bits_2shm_8h.html#ab652a1a4737f9118a64a8fb74084ef7d", null ],
    [ "SHM_EXEC", "bits_2shm_8h.html#a0bb6454e0dd48a66376bfaa05170ce82", null ],
    [ "SHM_LOCK", "bits_2shm_8h.html#a66735ad43f79860ccdd21888c3ead8cc", null ],
    [ "SHM_UNLOCK", "bits_2shm_8h.html#a7497459fc5ebe82bbbdfa3809c938312", null ],
    [ "shmatt_t", "bits_2shm_8h.html#af2f35c73b9015bea978f27b3e37d650e", null ]
];