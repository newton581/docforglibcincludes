var user_8h =
[
    [ "user_fpregs_struct", "structuser__fpregs__struct.html", "structuser__fpregs__struct" ],
    [ "user_fpxregs_struct", "structuser__fpxregs__struct.html", "structuser__fpxregs__struct" ],
    [ "user_regs_struct", "structuser__regs__struct.html", "structuser__regs__struct" ],
    [ "user", "structuser.html", "structuser" ],
    [ "PAGE_SHIFT", "user_8h.html#a850d80ca2291d26b40dc6b25c419f81a", null ],
    [ "PAGE_SIZE", "user_8h.html#a7d467c1d283fdfa1f2081ba1e0d01b6e", null ],
    [ "PAGE_MASK", "user_8h.html#ae4aa620ce57c7c3171b916de2c5f09f2", null ],
    [ "NBPG", "user_8h.html#afd3c1dc59507fe105cd3e62111c38542", null ],
    [ "UPAGES", "user_8h.html#ae439c12b4d0c80ad6b00bc18d1792bc4", null ],
    [ "HOST_TEXT_START_ADDR", "user_8h.html#aa200509fb4a19749831733050ac55daa", null ],
    [ "HOST_STACK_END_ADDR", "user_8h.html#a8bdb76bf60a690586e5749043fa7dda8", null ]
];