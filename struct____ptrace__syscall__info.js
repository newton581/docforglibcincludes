var struct____ptrace__syscall__info =
[
    [ "__attribute__", "struct____ptrace__syscall__info.html#a5200c992edc603daf3c50e42188cd14a", null ],
    [ "op", "struct____ptrace__syscall__info.html#ab3d1d8b6331f8db7416f781ed9c73709", null ],
    [ "instruction_pointer", "struct____ptrace__syscall__info.html#a7376e60f85a46c3a3c40ba8fe3edbc62", null ],
    [ "stack_pointer", "struct____ptrace__syscall__info.html#ab587a0650af627c53ec395d48194db7f", null ],
    [ "nr", "struct____ptrace__syscall__info.html#a08c9c1257d8b2534d718172b0a2f1399", null ],
    [ "args", "struct____ptrace__syscall__info.html#a643ec3702fca3f19082520efc85ebd17", null ],
    [ "entry", "struct____ptrace__syscall__info.html#a6048ab91fef7d761fcd1a4b3d43f1a08", null ],
    [ "rval", "struct____ptrace__syscall__info.html#a25ffed43f65750c018ed70f8d3038844", null ],
    [ "is_error", "struct____ptrace__syscall__info.html#a5a6969fbcb32924c2eb63eb2bdd2560d", null ],
    [ "exit", "struct____ptrace__syscall__info.html#ac8588aaa1ae08da74d3734414d8cc96c", null ],
    [ "ret_data", "struct____ptrace__syscall__info.html#ac51dda3e0d180af05612c2eee35375e2", null ],
    [ "seccomp", "struct____ptrace__syscall__info.html#a9ee87e5c55a5a7899eeb1afdbc49b9ea", null ]
];