var structElf32__Verdef =
[
    [ "vd_version", "structElf32__Verdef.html#ad2f222a352ff9e2d92199811ff094822", null ],
    [ "vd_flags", "structElf32__Verdef.html#a782382383f96808eeddb86e3db2737c3", null ],
    [ "vd_ndx", "structElf32__Verdef.html#afcaa14f5175bb38ecb6ef832c2aa2232", null ],
    [ "vd_cnt", "structElf32__Verdef.html#a38c7ed683760f798e42ff3c411ea23ba", null ],
    [ "vd_hash", "structElf32__Verdef.html#aed64e7dede5f8150f10457c420b48416", null ],
    [ "vd_aux", "structElf32__Verdef.html#a3693473efde66cb13cecda2053c91f9d", null ],
    [ "vd_next", "structElf32__Verdef.html#ac919bdca49dff2f3a04b1328903edb71", null ]
];