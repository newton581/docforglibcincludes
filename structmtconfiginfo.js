var structmtconfiginfo =
[
    [ "mt_type", "structmtconfiginfo.html#a0dde4b4f094922254da24b7e6743ab5e", null ],
    [ "ifc_type", "structmtconfiginfo.html#ac3d3876a2e8f219fe22a58b8d7522b9f", null ],
    [ "irqnr", "structmtconfiginfo.html#a2b9f145b9cae5a9b906761a7ff701bc1", null ],
    [ "dmanr", "structmtconfiginfo.html#a6c4f1510018eda4ba8e4b2ecfee48359", null ],
    [ "port", "structmtconfiginfo.html#a4064a78a2f77ef1657ae2340ed223030", null ],
    [ "debug", "structmtconfiginfo.html#a8aac2bd56088a05c0251aeb052818e1b", null ],
    [ "have_dens", "structmtconfiginfo.html#afb9dc956b0c3ff96a1dd308130f8570f", null ],
    [ "have_bsf", "structmtconfiginfo.html#a4cc08ddbc4649c70027271322cd50ed1", null ],
    [ "have_fsr", "structmtconfiginfo.html#aaf5238953a026e326451175a8b5b0168", null ],
    [ "have_bsr", "structmtconfiginfo.html#aa9a091b0aca0574fdfa7e370c52dde46", null ],
    [ "have_eod", "structmtconfiginfo.html#a9ee98a19d281ed37427fbcfdddac16c3", null ],
    [ "have_seek", "structmtconfiginfo.html#a62baf76f4e913103adcc10ed55d5a078", null ],
    [ "have_tell", "structmtconfiginfo.html#ae7ddbe385ffa6191abb8556dabe59244", null ],
    [ "have_ras1", "structmtconfiginfo.html#a199115582dfad05f3e9ff2dc7f287592", null ],
    [ "have_ras2", "structmtconfiginfo.html#a0a89fac268f7de0bb47881ba9457d0e5", null ],
    [ "have_ras3", "structmtconfiginfo.html#a5b0ce41fe5e43d4ac2074fdef9730361", null ],
    [ "have_qfa", "structmtconfiginfo.html#a2cdd712e49beaa24a90af867f7d6e927", null ],
    [ "pad1", "structmtconfiginfo.html#a3a61f82d6323381b72a656c1c642535c", null ],
    [ "reserved", "structmtconfiginfo.html#a61eabf62f50bf60b51dfdb72cd13f2e3", null ]
];