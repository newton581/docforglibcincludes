var structsigevent =
[
    [ "sigev_value", "structsigevent.html#ab70849a792ac0fa390c8c096819272d5", null ],
    [ "sigev_signo", "structsigevent.html#a5c645ec1d12bb46efc3f4097c52b665d", null ],
    [ "sigev_notify", "structsigevent.html#aae9a19d879c38e0c4e8a9bf738c5081e", null ],
    [ "_pad", "structsigevent.html#a7e2cc15f02fe3b3affe20f45c7e4ffa7", null ],
    [ "_tid", "structsigevent.html#aae283fb8799fd184e639061a42416a78", null ],
    [ "_function", "structsigevent.html#ae63837da568b6a3251486202be4a6a10", null ],
    [ "_attribute", "structsigevent.html#ac6e0e48fa645d39224b6daa4aba36d2e", null ],
    [ "_sigev_thread", "structsigevent.html#aaed179c7484031a1a774b9cc7a6675da", null ],
    [ "_sigev_un", "structsigevent.html#a6756b5c7a614f62ffb228133bfa0c0a1", null ]
];