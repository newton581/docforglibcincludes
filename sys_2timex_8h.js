var sys_2timex_8h =
[
    [ "ntptimeval", "structntptimeval.html", "structntptimeval" ],
    [ "NTP_API", "sys_2timex_8h.html#a11aa8446285f441f65f65ba33bef8d03", null ],
    [ "TIME_OK", "sys_2timex_8h.html#a1a8f377fc0eaaafb652217bfe8a5d58a", null ],
    [ "TIME_INS", "sys_2timex_8h.html#a8edb86e9fac809f6728f4bd5b050b267", null ],
    [ "TIME_DEL", "sys_2timex_8h.html#a0ec3b102bfd084b7020cccfad729b541", null ],
    [ "TIME_OOP", "sys_2timex_8h.html#ac86b0c6fc7fbe9da5c1660fadb43de56", null ],
    [ "TIME_WAIT", "sys_2timex_8h.html#a24fa2248797a8a88020d8cda0ea2189c", null ],
    [ "TIME_ERROR", "sys_2timex_8h.html#a1adc2d647c3b6fa0d24595dbbdce9b7a", null ],
    [ "TIME_BAD", "sys_2timex_8h.html#a379c0ffeacb0859f4685e71872d911fc", null ],
    [ "MAXTC", "sys_2timex_8h.html#a283ccaaf3b2809b02392eb58923e22b9", null ],
    [ "ntp_gettime", "sys_2timex_8h.html#addf648b3dbd9c66efdaa4521575df7f3", null ],
    [ "__adjtimex", "sys_2timex_8h.html#a6c8895b7b169e72578c18f68bd480de2", null ],
    [ "adjtimex", "sys_2timex_8h.html#a30dc1c2417a5ba076dd41b7ecaf03f11", null ],
    [ "ntp_gettimex", "sys_2timex_8h.html#aee834927a74f5285273367efc015cac1", null ],
    [ "ntp_adjtime", "sys_2timex_8h.html#ac66a03f0fb6c972c5617bf038fb26ecf", null ]
];